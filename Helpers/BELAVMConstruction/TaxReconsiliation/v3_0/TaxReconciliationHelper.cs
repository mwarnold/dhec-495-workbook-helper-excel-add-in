﻿using DheccExcelAddIn2013.Models.BELAVMConstruction.TaxReconciliation.v3_0;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using DheccExcelAddIn2013.Models;

namespace DheccExcelAddIn2013.Helpers.BELAVMConstruction.TaxReconsiliation.v3_0
{
    public static class TaxReconciliationHelper
    {
        public static TaxReconciliationModel Get()
        {
            TaxReconciliationModel trm = new TaxReconciliationModel();
            Worksheet ws = Globals.ThisAddIn.Application.Worksheets["A5) Tax Reconciliation"];
            ws.Activate();
            Range rng = ws.get_Range("B8", "H100");
            int i = 6;
            foreach (Range r in rng.Rows)
            {
                GenericList gl = new GenericList();
                string val = r.Cells[1, 1].Value2;
                if (string.IsNullOrEmpty(val))
                    continue;
                if (val.Contains("Net income"))
                    break;
                gl.Items.Add(r.Cells[1, 1].Value2.ToString());
                gl.Items.Add(r.Cells[1, 2].Value2.ToString());
                gl.Items.Add(r.Cells[1, 3].Value2.ToString());
                gl.Items.Add(r.Cells[1, 4].Value2.ToString());
                gl.Items.Add(r.Cells[1, 5].Value2.ToString());
                gl.Items.Add(r.Cells[1, 6].Value2.ToString());
                gl.Items.Add(r.Cells[1, 7].Value2.ToString());
                trm.AdjustmentsForReconciliation.Add(gl);
            }
            trm.TaxVarianceComments.Add(ExcelPaster.GetCellValue(ws, "C19"));
            trm.TaxVarianceComments.Add(ExcelPaster.GetCellValue(ws, "D19"));
            trm.TaxVarianceComments.Add(ExcelPaster.GetCellValue(ws, "E19"));
            trm.TaxVarianceComments.Add(ExcelPaster.GetCellValue(ws, "F19"));
            trm.TaxVarianceComments.Add(ExcelPaster.GetCellValue(ws, "G9"));
            trm.TaxVarianceComments.Add(ExcelPaster.GetCellValue(ws, "H19"));

            trm.TaxReconComments.Add(ExcelPaster.GetCellValue(ws, "C20"));
            trm.TaxReconComments.Add(ExcelPaster.GetCellValue(ws, "D20"));
            trm.TaxReconComments.Add(ExcelPaster.GetCellValue(ws, "E20"));
            trm.TaxReconComments.Add(ExcelPaster.GetCellValue(ws, "F20"));
            trm.TaxReconComments.Add(ExcelPaster.GetCellValue(ws, "G20"));
            trm.TaxReconComments.Add(ExcelPaster.GetCellValue(ws, "H20"));

            return trm;
        }

        public static void Set(TaxReconciliationModel trm)
        {
            Worksheet ws = Globals.ThisAddIn.Application.Worksheets["A5) Tax Reconciliation"];
            ws.Activate();
            Worksheet wsDownloadBEL = Globals.ThisAddIn.GetDownloadSheet();
            int rc = trm.AdjustmentsForReconciliation.Count;
            Range iRange = wsDownloadBEL.Range["B500:H" + (300 + (rc - 1))]; //the -1 accounts  for an extra index row that needs to be trimmed
            iRange.Value = ExcelPaster.GetRangeValuesToInsert(trm.AdjustmentsForReconciliation);
            wsDownloadBEL.Activate();
            iRange.Activate();
            iRange.Cut();
            ws.Activate();
            Range sectionRange = ws.get_Range("B8", "H8");
            sectionRange.Activate();
            sectionRange.Insert(XlInsertShiftDirection.xlShiftDown, Type.Missing);

            int rngLen = rc + 12;
            ExcelPaster.SetCellValue(ws, "C" + rc, trm.TaxVarianceComments[0]);
            ExcelPaster.SetCellValue(ws, "D" + rc, trm.TaxVarianceComments[1]);
            ExcelPaster.SetCellValue(ws, "E" + rc, trm.TaxVarianceComments[2]);
            ExcelPaster.SetCellValue(ws, "F" + rc, trm.TaxVarianceComments[3]);
            ExcelPaster.SetCellValue(ws, "G" + rc, trm.TaxVarianceComments[4]);
            ExcelPaster.SetCellValue(ws, "H" + rc, trm.TaxVarianceComments[5]);
            rc++;
            ExcelPaster.SetCellValue(ws, "C" + rc, trm.TaxReconComments[0]);
            ExcelPaster.SetCellValue(ws, "D" + rc, trm.TaxReconComments[1]);
            ExcelPaster.SetCellValue(ws, "E" + rc, trm.TaxReconComments[2]);
            ExcelPaster.SetCellValue(ws, "F" + rc, trm.TaxReconComments[3]);
            ExcelPaster.SetCellValue(ws, "G" + rc, trm.TaxReconComments[4]);
            ExcelPaster.SetCellValue(ws, "H" + rc, trm.TaxReconComments[5]);
        }
    }
}
