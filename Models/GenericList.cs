﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DheccExcelAddIn2013.Models
{
    public class GenericList
    {
        public List<string> Items { get; set; }
        public GenericList()
        {
            Items = new List<string>();
        }
    }
}
