﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DheccExcelAddIn2013.Helpers;
using DheccExcelAddIn2013.Models;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.IO;
using DheccExcelAddIn2013.Interfaces;

namespace DheccExcelAddIn2013.UserControls
{
    public partial class ucRemoteCopies : UserControl
    {
        private static string _saveHistId;
        private IDheccContract iContract;
        private static int _currentPage = 1;
        private static DateTime _endDate = DateTime.Now.AddDays(1);
        private static DateTime _startDate = DateTime.Now.AddDays(-7);
        private static int? _claimantId = null;
        private static int? _claimId = null;
        private static com.browngreer.www3.clsInterimInfoList _remoteCopies = new com.browngreer.www3.clsInterimInfoList();
        private static com.browngreer.www3.clsInterimInfo _stagedItem = new com.browngreer.www3.clsInterimInfo();

        public ucRemoteCopies()
        {
            InitializeComponent();
            this.iContract = new DheccContract();
            }

        public void RefreshControl()
        {
            //if this explodes, user hasn't set up programmatic access to the vba module and cannot continue. 
            try
            {
                var i = Globals.ThisAddIn.Application.ActiveWorkbook.VBProject.VBComponents.Count;
            }
            catch
            {
                MessageBox.Show("The DWH Workbook Helper Add In required programmatic access to the VBA Module. You will be redirected to the Add In Settings. In the 'Notifications' tab, you will find the instructions for setting this up in Excel. It should take less than two minutes.");
                Globals.ThisAddIn.SetCustomTaskPane("My Account");
                return;
            }
            btnReset.Enabled = false;
            dtpStartDate.Value = DateTime.Now.AddDays(-7);
            dtpEndDate.Value = DateTime.Now.AddDays(1);
            HideDetails();
            _remoteCopies = new com.browngreer.www3.clsInterimInfoList();
            if (AppVariables.IsLoggedIn)
                LoadRemote();
        }

        private void ucRemoteCopies_Load(object sender, EventArgs e)
        {

        }

        private void LoadRemote()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                if (!AppVariables.IsLoggedIn)
                    return;
                var client = AppVariables.BGWebService();
                _remoteCopies = client.GetInterimSavedInfo(AppVariables.CurrentUserId, AppVariables.AuthToken, _claimantId,
                  _claimId, _startDate, _endDate, cbMyClaims.Checked);
                if (!WebServiceResponseHelper.ValidateResponse(_remoteCopies.Result.ResultID, _remoteCopies.Result.ResultMessage))
                    return;
                WorkbookTemplate.UpdateActiveWorkbookList(_remoteCopies.WorkbookInfoList);
                dtpStartDate.Value = _startDate;
                dtpEndDate.Value = _endDate;
                LoadRemoteCopies(_remoteCopies);
                Cursor.Current = Cursors.Arrow;
            }
            catch (Exception ex)
            {
                WebServiceResponseHelper.ValidateResponse(0, "An unhandled exception has occured. Our dev team has been notifiied. We apologize for the inconvenience");
                EmailHelper.SendExceptionEmail(ex.ToString());
            }
        }

        protected class GridItem
        {
            public string SaveHistId { get; set; }
            public string Claimant { get; set; }
            public string Claim { get; set; }
            public bool AllowCheckIn { get; set; }
        }

        private void LoadRemoteCopies(com.browngreer.www3.clsInterimInfoList rCopies)
        {
            try
            {
                var goodResults = _remoteCopies.InterimInfoList.Where(x => x.ClaimData != null);
                if (goodResults == null || goodResults.ToList().Count == 0)
                {
                    dgRemoteCopies.DataSource = null;
                    return;
                }
                var results = (from rc in goodResults.OrderByDescending(x => x.Date)
                               select new
                               {
                                   SaveHistId = rc.SaveHistID,
                                   ClaimData = rc.ClaimData
                               });
                List<GridItem> remoteList = new List<GridItem>();
                foreach (var r in results)
                {
                    var ww = JsonConvert.DeserializeObject<WorkbookWrapper>(r.ClaimData);
                    remoteList.Add(
                        new GridItem()
                        {
                             Claim = ww.ClaimInfo.ClaimId.ToString(),
                             Claimant = string.Format("{0}-#{1}", ww.ClaimInfo.ClaimantName, ww.ClaimInfo.ClaimantId),
                             SaveHistId = r.SaveHistId.ToString(),
                        });
                }
                dgRemoteCopies.DataSource = null;
                dgRemoteCopies.DataSource = remoteList;

                int width = ((dgRemoteCopies.Width / 2) - 15);

                if (results.Count() > 0)
                {
                    dgRemoteCopies.Columns[0].Visible = false;
                    dgRemoteCopies.Columns[1].Width = width;
                    dgRemoteCopies.Columns[2].Width = width;
                    dgRemoteCopies.CellClick += new DataGridViewCellEventHandler(dgRemoteCopies_CellContentClick);
                }

            }
            catch (Exception ex)
            {
                WebServiceResponseHelper.ValidateResponse(0, "An unhandled exception has occured. Our dev team has been notifiied. We apologize for the inconvenience");
                EmailHelper.SendExceptionEmail(ex.ToString());
            }
        }
        private static int _clickedRowIndex = 0;
        private void dgRemoteCopies_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex < 0)
                    return;
                _clickedRowIndex = e.RowIndex;
                ShowDetails();
                DataGridViewRow dr = dgRemoteCopies.Rows[e.RowIndex];
                _saveHistId = dr.Cells[0].Value.ToString();
                _stagedItem = _remoteCopies.InterimInfoList.SingleOrDefault(x => x.SaveHistID.ToString() == _saveHistId);
                tbCommitMessage.Text = _stagedItem.CommitMessage;
                lblDateValue.Text = _stagedItem.Date.ToShortDateString() + " " + _stagedItem.Date.ToShortTimeString();
                btnReset.Enabled = true;
            }
            catch (Exception ex)
            {
                WebServiceResponseHelper.ValidateResponse(0, "An unhandled exception has occured. Our dev team has been notifiied. We apologize for the inconvenience");
                EmailHelper.SendExceptionEmail(ex.ToString());
            }
        }

        private void ShowDetails()
        {
            pnlUploadInfo.Visible = true;
            dgRemoteCopies.Height = 415;
            pnlGrid.Height = 458;
        }

        private void HideDetails()
        {
            pnlUploadInfo.Visible = false;
            dgRemoteCopies.Height = 818;
            pnlGrid.Height = 860;
        }

        private void btnLoadClaim_Click(object sender, EventArgs e)
        {

        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            cbMyClaims.Checked = true;
            _claimantId = null;
            _claimId = null;
            HideDetails();
            LoadRemote();
            btnReset.Enabled = false;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            HideDetails();
            try
            {
                _claimId = int.Parse(tbClaim.Text);
                _claimantId = int.Parse(tbClaimant.Text);
            }
            catch { }
            _startDate = dtpStartDate.Value;
            _endDate = dtpEndDate.Value;
            LoadRemote();
            btnReset.Enabled = true;
        }

        private void btnLoadClaim_Click_1(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                WorkbookWrapper ww = new WorkbookWrapper();
                ww = JsonConvert.DeserializeObject<WorkbookWrapper>(_stagedItem.ClaimData);
                var descItems = _remoteCopies.InterimInfoList.OrderByDescending(x => x.Date).ToList();
                var mostRecentCommit = descItems.FirstOrDefault(x => x.ClaimantID == ww.ClaimInfo.ClaimantId
                    && x.ClaimID == ww.ClaimInfo.ClaimId);
                if (mostRecentCommit != null)
                {
                    int liIndex = descItems.IndexOf(mostRecentCommit);
                    if (liIndex < _clickedRowIndex) //higher on the list which is ordered by date.
                    {
                        DialogResult dialogRes = MessageBox.Show("Newer checked-in claims are available. Would you like to continue?", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk);
                        if (dialogRes == DialogResult.No)
                            return;
                    }
                }
                ww.IsRemoteCopy = true;
                ww.CanEdit = iContract.UsersClaims().Any(x => x == ww.ClaimInfo.ClaimId);
                WorkbookTemplate.OpenRemoteWorkbook(ww, ww.SaveHistId);                
                Cursor.Current = Cursors.Arrow;
            }
            catch (Exception ex)
            {
                WebServiceResponseHelper.ValidateResponse(0, "An unhandled exception has occured. Our dev team has been notifiied. We apologize for the inconvenience");
                EmailHelper.SendExceptionEmail(ex.ToString());
            }
        }

        private void dgRemoteCopies_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            foreach (DataGridViewRow dr in dgRemoteCopies.Rows)
            {
                try
                {
                    if ((bool)dr.Cells[3].Value)
                    {
                        dr.DefaultCellStyle =
                            new DataGridViewCellStyle()
                            {
                                BackColor = System.Drawing.Color.LightYellow
                            };
                    }
                }
                catch { }
            }
        }





    }
}
