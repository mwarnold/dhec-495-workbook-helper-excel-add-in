﻿namespace DheccExcelAddIn2013.UserControls
{
    partial class ucSaveRemote
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblUploadText = new System.Windows.Forms.Label();
            this.btnUpload = new System.Windows.Forms.Button();
            this.pbUpload = new System.Windows.Forms.ProgressBar();
            this.tbMessage = new System.Windows.Forms.TextBox();
            this.lblCmMes = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblUploadText
            // 
            this.lblUploadText.AutoSize = true;
            this.lblUploadText.Location = new System.Drawing.Point(20, 20);
            this.lblUploadText.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUploadText.Name = "lblUploadText";
            this.lblUploadText.Size = new System.Drawing.Size(264, 17);
            this.lblUploadText.TabIndex = 0;
            this.lblUploadText.Text = "Save the current state of the workbook   ";
            // 
            // btnUpload
            // 
            this.btnUpload.Location = new System.Drawing.Point(19, 155);
            this.btnUpload.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnUpload.Name = "btnUpload";
            this.btnUpload.Size = new System.Drawing.Size(126, 28);
            this.btnUpload.TabIndex = 1;
            this.btnUpload.Text = "Upload Now";
            this.btnUpload.UseVisualStyleBackColor = true;
            this.btnUpload.Click += new System.EventHandler(this.btnUpload_Click);
            // 
            // pbUpload
            // 
            this.pbUpload.Location = new System.Drawing.Point(152, 155);
            this.pbUpload.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pbUpload.Name = "pbUpload";
            this.pbUpload.Size = new System.Drawing.Size(275, 28);
            this.pbUpload.TabIndex = 2;
            // 
            // tbMessage
            // 
            this.tbMessage.Location = new System.Drawing.Point(19, 76);
            this.tbMessage.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbMessage.Multiline = true;
            this.tbMessage.Name = "tbMessage";
            this.tbMessage.Size = new System.Drawing.Size(410, 54);
            this.tbMessage.TabIndex = 5;
            // 
            // lblCmMes
            // 
            this.lblCmMes.AutoSize = true;
            this.lblCmMes.Location = new System.Drawing.Point(20, 50);
            this.lblCmMes.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCmMes.Name = "lblCmMes";
            this.lblCmMes.Size = new System.Drawing.Size(115, 17);
            this.lblCmMes.TabIndex = 6;
            this.lblCmMes.Text = "Commit Message";
            // 
            // ucSaveRemote
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Controls.Add(this.lblCmMes);
            this.Controls.Add(this.tbMessage);
            this.Controls.Add(this.pbUpload);
            this.Controls.Add(this.btnUpload);
            this.Controls.Add(this.lblUploadText);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "ucSaveRemote";
            this.Size = new System.Drawing.Size(450, 606);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblUploadText;
        private System.Windows.Forms.Button btnUpload;
        private System.Windows.Forms.ProgressBar pbUpload;
        private System.Windows.Forms.TextBox tbMessage;
        private System.Windows.Forms.Label lblCmMes;
    }
}
