﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DheccExcelAddIn2013.Models
{
    public class ClaimInfoModel
    {
        public int ClaimantId { get; set; }
        public string ClaimantName { get; set; }
        public int ClaimId { get; set; }
        public List<string> DownloadBel { get; set; }
        public string TempVal { get; set; }
        public ClaimInfoModel()
        {
            DownloadBel = new List<string>();
        }
    }
}
