﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using DheccExcelAddIn2013.Models.BELAVMConstruction.TaxVariance.v3_0;
using DheccExcelAddIn2013.Models;

namespace DheccExcelAddIn2013.Helpers.BELAVMConstruction.TaxVariance.v3_0
{
    public static class TaxVarianceA31Helper
    {
        public static TaxVarianceA31Model Get()
        {
            Worksheet ws = Globals.ThisAddIn.Application.Worksheets["A3.1) Tax Variance"];
            ws.Activate();
            TaxVarianceA31Model tvm = new TaxVarianceA31Model();
            Range rng = ws.get_Range("F12", "J12");
            foreach (var rv in rng.Value2)
                tvm.GrossReceiptsOrSalesPerTaxReturn.Add(ExcelPaster.FormatRangeValueToString(rv));

            rng = ws.get_Range("F17", "J17");
            foreach (var rv in rng.Value2)
                tvm.NetIncomeLossPerTaxReturn.Add(ExcelPaster.FormatRangeValueToString(rv));
            
            rng = ws.get_Range("F103", "K103");
            foreach (var rv in rng.Value2)
                tvm.DetailedAccounts.Add(ExcelPaster.FormatRangeValueToString(rv));

            rng = ws.get_Range("B107", "K180");
            int i = 107;
            foreach (Range r in rng.Rows)
            {
                GenericList gl = new GenericList();
                string val = r.Cells[1, 1].Value2;
                if (string.IsNullOrEmpty(val) || val.Contains("P&L General Account") || val.Contains("Tax Return General") || val.Contains("Variance of"))
                    break;
                foreach (var rv in r.Value2)
                    gl.Items.Add(ExcelPaster.FormatRangeValueToString(rv));
                tvm.PAndLGeneralAcctDetails.Add(gl);
            }

            int taxRetStartRow = 0;
            int varianceOfStartRow = 0;
            rng = ws.get_Range("B107", "B300");
            i = 106;
            foreach (var rv in rng.Value2)
            {
                i++;
                string cVal = ExcelPaster.FormatRangeValueToString(rv);
                if (string.IsNullOrEmpty(cVal))
                    continue;
                if (cVal.Contains("General Account"))
                    taxRetStartRow = i;
                else if (cVal.Contains("Variance"))
                {
                    varianceOfStartRow = i;
                    break;
                }
            }
            i = 107;
            rng = ws.get_Range("B107", "K" + (taxRetStartRow - 2));
            foreach (Range r in rng.Rows)
            {
                GenericList gl = new GenericList();
                string val = r.Cells[i, 1].Value2;
                if (string.IsNullOrEmpty(val))
                    break;
                foreach (var rv in r.Value2)
                    gl.Items.Add(ExcelPaster.FormatRangeValueToString(rv));
                tvm.PAndLGeneralAcctDetails.Add(gl);
                i++;
            }

            i = taxRetStartRow;
            rng = ws.get_Range("B" + taxRetStartRow, "K" + (varianceOfStartRow - 2));
            foreach (Range r in rng.Rows)
            {
                GenericList gl = new GenericList();
                string val = r.Cells[i, 1].Value2;
                if (string.IsNullOrEmpty(val))
                    break;
                foreach (var rv in r.Value2)
                    gl.Items.Add(ExcelPaster.FormatRangeValueToString(rv));
                tvm.PAndLGeneralAcctDetails.Add(gl);
                i++;
            }

            i = varianceOfStartRow;
            rng = ws.get_Range("B" + varianceOfStartRow, "K500");
            foreach (Range r in rng.Rows)
            {
                GenericList gl = new GenericList();
                string val = r.Cells[i, 1].Value2;
                if (string.IsNullOrEmpty(val))
                    break;
                foreach (var rv in r.Value2)
                    gl.Items.Add(ExcelPaster.FormatRangeValueToString(rv));
                tvm.PAndLGeneralAcctDetails.Add(gl);
                i++;
            }

            return tvm;
        }

        public static void Set(TaxVarianceA31Model tvm)
        {
            Worksheet ws = Globals.ThisAddIn.Application.Worksheets["A3.1) Tax Variance"];
            ws.Activate();
            Range rng = ws.get_Range("F12", "J12");
            ExcelPaster.PasteHorizontalSingleDimension(tvm.GrossReceiptsOrSalesPerTaxReturn, rng);

            rng = ws.get_Range("F17", "J17");
            ExcelPaster.PasteHorizontalSingleDimension(tvm.NetIncomeLossPerTaxReturn, rng);
            
            rng = ws.get_Range("F103", "K103");
            ExcelPaster.PasteHorizontalSingleDimension(tvm.DetailedAccounts, rng);
           
            Worksheet wsDownloadBEL = Globals.ThisAddIn.GetDownloadSheet();
            int rc = tvm.PAndLGeneralAcctDetails.Count;
            Range iRange = wsDownloadBEL.Range["B500:H" + (300 + (rc - 1))]; //the -1 accounts  for an extra index row that needs to be trimmed
            iRange.Value = ExcelPaster.GetRangeValuesToInsert(tvm.PAndLGeneralAcctDetails);
            wsDownloadBEL.Activate();
            iRange.Activate();
            iRange.Cut();
            ws.Activate();
            Range sectionRange = ws.get_Range("E107", "K107");
            sectionRange.Activate();
            sectionRange.Insert(XlInsertShiftDirection.xlShiftDown, Type.Missing);
           
            int taxRetStartRow = 0;
            int varianceOfStartRow = 0;
            rng = ws.get_Range("B107", "B500");
            int i = 107;
            foreach (Range r in rng.Rows)
            {
                i++;
                string cVal = r.Cells[i, 1].Value2;
                if (string.IsNullOrEmpty(cVal))
                    continue;
                if (cVal == "Tax Return General Account Details")
                    taxRetStartRow = i;
                else if (cVal == "Variance of")
                {
                    varianceOfStartRow = i;
                    break;
                }
            }
            rc = tvm.TaxReturnGeneralAcctDetails.Count;
            iRange = wsDownloadBEL.Range["B500:H" + (300 + (rc - 1))]; //the -1 accounts  for an extra index row that needs to be trimmed
            iRange.Value = ExcelPaster.GetRangeValuesToInsert(tvm.TaxReturnGeneralAcctDetails);
            wsDownloadBEL.Activate();
            iRange.Activate();
            iRange.Cut();
            ws.Activate();
            sectionRange = ws.get_Range("E" + taxRetStartRow, "K" + taxRetStartRow);
            sectionRange.Activate();
            sectionRange.Insert(XlInsertShiftDirection.xlShiftDown, Type.Missing);


             taxRetStartRow = 0;
             varianceOfStartRow = 0;
            rng = ws.get_Range("B107", "B500");
             i = 107;
            foreach (Range r in rng.Rows)
            {
                i++;
                string cVal = r.Cells[i, 1].Value2;
                if (string.IsNullOrEmpty(cVal))
                    continue;
                if (cVal == "Tax Return General Account Details")
                    taxRetStartRow = i;
                else if (cVal == "Variance of")
                {
                    varianceOfStartRow = i;
                    break;
                }
            }

            rc = tvm.VarianceOf.Count;
            iRange = wsDownloadBEL.Range["B500:H" + (300 + (rc - 1))]; //the -1 accounts  for an extra index row that needs to be trimmed
            iRange.Value = ExcelPaster.GetRangeValuesToInsert(tvm.VarianceOf);
            wsDownloadBEL.Activate();
            iRange.Activate();
            iRange.Cut();
            ws.Activate();
            sectionRange = ws.get_Range("E" + varianceOfStartRow, "K" + varianceOfStartRow);
            sectionRange.Activate();
            sectionRange.Insert(XlInsertShiftDirection.xlShiftDown, Type.Missing);
        }
    }
}
