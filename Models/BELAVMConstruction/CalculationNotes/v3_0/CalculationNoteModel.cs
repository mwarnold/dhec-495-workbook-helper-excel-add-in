﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DheccExcelAddIn2013.Models.BELAVMConstruction.CalculationNotes.v3_0
{
    public class CalculationNoteModel
    {
        public string Id { get; set; }
        public string AppliesTo { get; set; }
        public string Description { get; set; }
    }
}
