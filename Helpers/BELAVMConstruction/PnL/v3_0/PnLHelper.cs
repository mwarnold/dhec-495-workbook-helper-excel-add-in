﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using DheccExcelAddIn2013.Interfaces;
using DheccExcelAddIn2013.Models;
using System.Windows.Forms;
using DheccExcelAddIn2013.Models.BELAVMConstruction.PnL.v3_0;

namespace DheccExcelAddIn2013.Helpers.BELAVMConstruction.PnL.v3_0
{
    public static class PnLHelper
    {
        private static string plType = "";
        private static bool startDateEntry = false;
        private static bool sectionFound = false;
        private static int rowId = 0;

        private static int sectionRowStartId = 0;
        private static int rc = 0;

        private static Range sectionRange;
        private static Array tempRange;
        private static DateTime startDate;
        private static DateTime endDate;
        private static Worksheet wsDownloadBEL;

        private static IDheccContract iContract = new DheccContract();
        public static PandLModel Get()
        {
            PandLModel pnlModel = new PandLModel();
            Worksheet worksheet = Globals.ThisAddIn.Application.Worksheets.get_Item("A2) Monthly P&L (Source)");
            worksheet.Activate();
            Range range = worksheet.Range["A1", "A500"];
            var rangeValues = (Array)range.Value2;
            wsDownloadBEL = Globals.ThisAddIn.GetDownloadSheet();
            plType = "";
            startDateEntry = false;
            rowId = 0;
            sectionRowStartId = 0;
            sectionFound = false;
            foreach (var arrayValue in rangeValues)
            {
                rowId++;
                string val;
                try
                {
                    val = arrayValue.ToString();
                }
                catch //this is a null in the range, continue to next row value, but could be end of section. See note below.
                {
                    if (sectionFound)
                    {
                        startDateEntry = true;
                        val = "";
                    }
                    else
                        continue;
                }
                if (val == "Revenues" || val == "Cost of Goods Sold" || val == "Expenses:" || val == "Other Income (Loss):" || val == "Other Expenses:" || val == "Source:" || val == "Notes:")
                {
                    //excel range array handles empty cells as nulls so the val == "" never hits, so set the sectionFound bool on enter and exit. 
                    //Then check to see if the section found is true and then the null exception hits, this equals end of section. 
                    sectionFound = true;
                    plType = val.ToString();
                    sectionRowStartId = rowId + 1;
                    continue;
                } //the below key words mark an unentered section value and ends the row
                else if (val == "Revenue" || val == "COGS" || val == "Expense" || val == "Other Income (Loss)" || val == "Other Expense")
                {
                    startDateEntry = true;
                    val = "";
                } // these are reserve keywork marks for next sections
                else if (val == "Total Revenues" || val == "Total Adjustments" || val == "Total Cost of Goods Sold" ||
                    val == "Total Operating Expenses" || val == "Total Other Income (Loss)" || val == "Total Other Expense")
                {
                    startDateEntry = true;
                    val = "";
                }
                if (startDateEntry)
                {
                    switch (plType)
                    {
                        case "Revenues":
                            sectionRange = worksheet.Range["A" + sectionRowStartId + ":CQ" + (rowId - 1)];
                            tempRange = (Array)sectionRange.Value2;
                            foreach (var rv in tempRange)
                            {
                                if (rv == null)
                                    pnlModel.Revenues.Add("");
                                else
                                    pnlModel.Revenues.Add(ExcelPaster.FormatRangeValueToString(rv));
                            }
                            plType = "";
                            continue;
                        case "Cost of Goods Sold":
                            sectionRange = worksheet.Range["A" + sectionRowStartId + ":CQ" + (rowId - 1)];
                            tempRange = (Array)sectionRange.Value2;
                            foreach (var rv in tempRange)
                            {
                                if (rv == null)
                                    pnlModel.CostOfGoodsSold.Add("");
                                else
                                    pnlModel.CostOfGoodsSold.Add(ExcelPaster.FormatRangeValueToString(rv));
                            }
                            plType = "";
                            continue;
                        case "Expenses:":
                            sectionRange = worksheet.Range["A" + sectionRowStartId + ":CQ" + (rowId - 1)];
                            tempRange = (Array)sectionRange.Value2;
                            foreach (var rv in tempRange)
                            {
                                if (rv == null)
                                    pnlModel.Expenses.Add("");
                                else
                                    pnlModel.Expenses.Add(ExcelPaster.FormatRangeValueToString(rv));
                            }
                            plType = "";
                            continue;
                        case "Other Expenses:":
                            sectionRange = worksheet.Range["A" + sectionRowStartId + ":CQ" + (rowId - 1)];
                            tempRange = (Array)sectionRange.Value2;
                            foreach (var rv in tempRange)
                            {
                                if (rv == null)
                                    pnlModel.OtherExpenses.Add("");
                                else
                                    pnlModel.OtherExpenses.Add(ExcelPaster.FormatRangeValueToString(rv));
                            }
                            plType = "";
                            continue;
                        case "Other Income (Loss):":
                            sectionRange = worksheet.Range["A" + sectionRowStartId + ":CQ" + (rowId - 1)];
                            tempRange = (Array)sectionRange.Value2;
                            foreach (var rv in tempRange)
                            {
                                if (rv == null)
                                    pnlModel.OtherIncomeLoss.Add("");
                                else
                                    pnlModel.OtherIncomeLoss.Add(ExcelPaster.FormatRangeValueToString(rv));
                            }
                            plType = "";
                            continue;
                        default:

                            break;
                    }
                    startDateEntry = false;
                    sectionFound = false;
                }
            }

            return pnlModel;
        }

        private static void LoadSections(PandLModel pnlData)
        {
            if (pnlData == null)
                return;
            Worksheet worksheet = Globals.ThisAddIn.Application.Worksheets["A2) Monthly P&L (Source)"];
            worksheet.Activate();
            wsDownloadBEL = Globals.ThisAddIn.GetDownloadSheet();
            Range range;
            range = worksheet.Range["A1", "A500"];
            sectionRowStartId = range.Find("Revenues").Row;
            InsertRowsForClaimData(worksheet, pnlData.Revenues);

            range = worksheet.Range["A1", "A500"];
            sectionRowStartId = range.Find("Cost of Goods Sold").Row;
            InsertRowsForClaimData(worksheet, pnlData.CostOfGoodsSold);

            range = worksheet.Range["A1", "A500"];
            sectionRowStartId = range.Find("Expenses:").Row;
            InsertRowsForClaimData(worksheet, pnlData.Expenses);

            range = worksheet.Range["A1", "A500"];
            sectionRowStartId = range.Find("Other Expenses:").Row;
            InsertRowsForClaimData(worksheet, pnlData.OtherExpenses);

            range = worksheet.Range["A1", "A500"];
            sectionRowStartId = range.Find("Other Income (Loss):").Row;
            InsertRowsForClaimData(worksheet, pnlData.OtherIncomeLoss);

         }

        private static void InsertRowsForClaimData(Worksheet worksheet, List<string> sList)
        {
            int listCount = sList.Count();
            if (listCount == 0)
                return;
            rc = listCount / 95;
            object[,] valRange = new object[rc, 95];
            //row count
            int i = 0;
            int cc = 0;
            int currentRc = 0;
            do
            {
                //reset column count
                cc = 0;
                do
                {
                    string val = sList[i];
                    valRange[currentRc, cc] = val;
                    cc++; i++;
                } while (cc <= 94);
                currentRc++;
            } while (currentRc <= (rc - 1));

            sectionRowStartId = sectionRowStartId += 1;
            //should have created 2 dimensional array that matches a ranges-insert new rows.
            sectionRange = worksheet.Range["A" + sectionRowStartId + ":CQ" + sectionRowStartId];

            Range iRange = wsDownloadBEL.Range["A500:CQ" + (300 + (rc - 1))]; //the -1 accounts  for an extra index row that needs to be trimmed
            iRange.Value = valRange;
            wsDownloadBEL.Activate();
            iRange.Activate();
            iRange.Cut();
            worksheet.Activate();
            sectionRange.Activate();
            sectionRange.Insert(XlInsertShiftDirection.xlShiftDown, Type.Missing);
            // sectionRange.PasteSpecial(XlPasteType.xlPasteAll, XlPasteSpecialOperation.xlPasteSpecialOperationNone, Type.Missing, Type.Missing);
            plType = "";
            worksheet.Activate();
            sectionRange.Activate();
        }
        public static  void InsertDownloadBEL(List<string> rDownloadBEL)
        {
            Worksheet worksheet = Globals.ThisAddIn.GetDownloadSheet();
            Range sectionRange = worksheet.get_Range("C2: C" + rDownloadBEL.Count);
            for (int i = 1; i <= rDownloadBEL.Count; i++)
                sectionRange[i] = rDownloadBEL[i - 1];
            worksheet.Activate();
        }
     
    }
}
