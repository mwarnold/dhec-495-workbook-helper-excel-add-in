﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using DheccExcelAddIn2013.Models.BELAVMConstruction.TotalCalculatedLoss.v3_0;

namespace DheccExcelAddIn2013.Helpers.BELAVMConstruction.TotalCalculatedLoss.v3_0
{
    public static class TotalCalculatedLossHelper
    {
        public static TotalCalculatedLossModel Get()
        {
            Worksheet ws = Globals.ThisAddIn.Application.Worksheets["B1) Total Calculated Loss"];
            ws.Activate();
            TotalCalculatedLossModel tclm = new TotalCalculatedLossModel();
            tclm.ClaimsAdministratorSelected = ExcelPaster.GetCellValue(ws, "J26");
            tclm.ClaimsAdministratorSelected = ExcelPaster.GetCellValue(ws, "J28");
            tclm.ClaimsAdministratorSelected = ExcelPaster.GetCellValue(ws, "J30");

            return tclm;
        }

        public static void Set(TotalCalculatedLossModel tclm)
        {
            Worksheet ws = Globals.ThisAddIn.Application.Worksheets["B1) Total Calculated Loss"];
            ws.Activate();
            ExcelPaster.SetCellValue(ws, "J26", tclm.ClaimsAdministratorSelected);
            ExcelPaster.SetCellValue(ws, "J28", tclm.ClaimsAdministratorSelected);
            ExcelPaster.SetCellValue(ws, "J30", tclm.ClaimsAdministratorSelected);
        }
    }
}
