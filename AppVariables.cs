﻿using DheccExcelAddIn2013.Helpers;
using DheccExcelAddIn2013.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DheccExcelAddIn2013
{
    public static class AppVariables
    {
        private static int _workbookTypeId = 0;
        private static string _workbookName = "";
        private static string _authToken = "";
        private static string _encPW = "";
        private static bool _isLoggedIn = false;
        public static string _currentUserName = "";
        public static int _currentUserId = int.MinValue;
        private static bool _workbookOpened = false;
        private static int _currentClaimantId = 0;
        private static int _currentClaimId = 0;
        private static string _currentException = "";
        private static bool _loadingTemplate = false;
        private static string _currentUserControlTitle = "";
        public static string AuthToken { get { return _authToken; } }
        private static IDheccContract iContract = new DheccContract();
        
        public static void SetAuthToken(string token)
        {
            _authToken = token;
        }

        public static int CurrentUserId { get { return _currentUserId; } }
        public static void SetCurrentUserId(int userId)
        {
            _currentUserId = userId;
        }

        public static string CurrentUserControlTitle { get { return _currentUserControlTitle; } }
        public static void SetCurrentUserControlTitle(string uc)
        {
            _currentUserControlTitle = uc;
        }

        public static string EncryptedPassword { get { return _encPW; } }
        public static void SetEncryptedPassword(string pw)
        {
            _encPW = pw;
        }
     
        public static string RootExcelAddInFileDirectory()
        {
            return System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\DheccExcelAddIn";
        }

        public static bool IsLoggedIn { get { return _isLoggedIn; } }
        public static void SetIsLoggedIn(bool loggedIn)
        {
            _isLoggedIn = loggedIn;
        }
        private static bool _isEntered = false;
        public static bool DeveloperPasswordEntered { get { return _isEntered; } }
        public static void SetDeveloperPasswordEntered(bool isEntered)
        {
            _isEntered = isEntered;
        }

        private static bool _495WorkbookOpened = false;
        public static bool DWH495WorkbookOpened{ get { return _495WorkbookOpened; } }
        public static void SetDWH495WorkbookOpened(bool opened)
        {
            _495WorkbookOpened = opened;
        }

        public static int InitialReviewerRoleId { get { return 26; } }

        public static int QARoleId { get { return 26; } }

        public static int InitialReviewerTypeId { get { return 26; } }

        public static int QAReviewerTypeId { get { return 30; } }

        public static int ConsistencyTeamReivewerTypeId { get { return 101; } }

        public static bool TemplateIsLoading { get { return _loadingTemplate; } }
        public static void SetTemplateIsLoading(bool lt)
        {
            _loadingTemplate = lt;
        }
        public static string HelpDirectory() { return RootExcelAddInFileDirectory() + @"\HelpFiles\"; }
        public static string HelpRootDirectory() { return RootExcelAddInFileDirectory() + @"\HelpFiles"; }

        public static string LocalDataDirectory() { return RootExcelAddInFileDirectory() + @"\LocalData\"; }
        public static string LocalMacroDirectory() { return RootExcelAddInFileDirectory() + @"\MacroFiles\"; }
        public static string LocalDataRootDirectory() { return RootExcelAddInFileDirectory() + @"\LocalData"; }
        public static string LocalExcelFileDirectory() { return RootExcelAddInFileDirectory() + @"\ExcelFiles\"; }
        public static string DownloadedExcelFileDirectory() { return RootExcelAddInFileDirectory() + @"\DownloadedExcelFiles\"; }
        public static string LocalExcelFileRootDirectory() { return RootExcelAddInFileDirectory() + @"\ExcelFiles"; }
        public static string TemplateDirectory() { return RootExcelAddInFileDirectory() + @"\Templates\"; }
        public static string TemplateRootDirectory() { return RootExcelAddInFileDirectory() + @"\Templates"; }
        public static string CurrentExeption { get { return _currentException; } }
        public static void SetCurrentException(string ce) { _currentException = ce; }

        public static void RefreshSessionToken()
        {
            try
            {
                IPHostEntry SystemAC = Dns.GetHostEntry(Dns.GetHostName());
                string ipAddress = SystemAC.AddressList.FirstOrDefault(x => x.AddressFamily == AddressFamily.InterNetwork).ToString();
                string computerName = System.Environment.MachineName;
                var client = AppVariables.BGWebService();
                com.browngreer.www3.clsVerfication resp;
                resp = client.VerifyUser(
                      _currentUserName, _encPW, "12.12.4.5", "sjtylu");
                if (!WebServiceResponseHelper.ValidateResponse(resp.Result.ResultID, resp.Result.ResultMessage))
                    return;
                WorkbookTemplate.UpdateActiveWorkbookList(resp.WorkbookInfoList);
                _authToken = resp.AuthToken;
            }
            catch (Exception ex)
            {
                EmailHelper.SendExceptionEmail(ex.ToString());
                return;
            }
        }

        public static string SaltKey { get { return "d9a89ar20Osaszz"; } }
        public static string PasswordValue { get { return "zopeU37Rlz4h"; } }
     
        private static int _constructionWbTypeId = 6;
        public static int ConstructionWorkbookTypeId { get { return _constructionWbTypeId; } }

        public static void StampWorkbookAsTemp()
        {
            Microsoft.Office.Interop.Excel.Worksheet ws = Globals.ThisAddIn.GetDownloadSheet();
            ExcelPaster.SetCellValue(ws, "Z1", "1");
        }

        public static com.browngreer.www3.Service BGWebService()
        {
            var client = new com.browngreer.www3.Service();
            IDheccContract iContract = new DheccContract();
            client.Url = iContract.AppSetting().WebServiceURL;
            return client;
        }

    }
}
