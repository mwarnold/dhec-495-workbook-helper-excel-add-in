﻿namespace DheccExcelAddIn2013.UserControls
{
    partial class ucUploadClaim
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbQAUpload = new System.Windows.Forms.GroupBox();
            this.lblPDFStatus = new System.Windows.Forms.Label();
            this.lblWorkbookStatus = new System.Windows.Forms.Label();
            this.imgPNL = new System.Windows.Forms.PictureBox();
            this.imgPDF = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblPNLSummary = new System.Windows.Forms.Label();
            this.btnPNLSummary = new System.Windows.Forms.Button();
            this.lblPDFFile = new System.Windows.Forms.Label();
            this.btnAttachPDF = new System.Windows.Forms.Button();
            this.pbUpload = new System.Windows.Forms.ProgressBar();
            this.btnUpload = new System.Windows.Forms.Button();
            this.gbInitialReview = new System.Windows.Forms.GroupBox();
            this.pbInitialReviewer = new System.Windows.Forms.ProgressBar();
            this.btnInitialReviewerUpload = new System.Windows.Forms.Button();
            this.lblSummaryStatus = new System.Windows.Forms.Label();
            this.gbQAUpload.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgPNL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPDF)).BeginInit();
            this.gbInitialReview.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbQAUpload
            // 
            this.gbQAUpload.Controls.Add(this.lblPDFStatus);
            this.gbQAUpload.Controls.Add(this.lblSummaryStatus);
            this.gbQAUpload.Controls.Add(this.lblWorkbookStatus);
            this.gbQAUpload.Controls.Add(this.imgPNL);
            this.gbQAUpload.Controls.Add(this.imgPDF);
            this.gbQAUpload.Controls.Add(this.label2);
            this.gbQAUpload.Controls.Add(this.lblPNLSummary);
            this.gbQAUpload.Controls.Add(this.btnPNLSummary);
            this.gbQAUpload.Controls.Add(this.lblPDFFile);
            this.gbQAUpload.Controls.Add(this.btnAttachPDF);
            this.gbQAUpload.Controls.Add(this.pbUpload);
            this.gbQAUpload.Controls.Add(this.btnUpload);
            this.gbQAUpload.Location = new System.Drawing.Point(14, 14);
            this.gbQAUpload.Margin = new System.Windows.Forms.Padding(2);
            this.gbQAUpload.Name = "gbQAUpload";
            this.gbQAUpload.Padding = new System.Windows.Forms.Padding(2);
            this.gbQAUpload.Size = new System.Drawing.Size(419, 423);
            this.gbQAUpload.TabIndex = 0;
            this.gbQAUpload.TabStop = false;
            this.gbQAUpload.Text = "Q/A Final Upload";
            // 
            // lblPDFStatus
            // 
            this.lblPDFStatus.AutoSize = true;
            this.lblPDFStatus.Location = new System.Drawing.Point(28, 311);
            this.lblPDFStatus.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPDFStatus.Name = "lblPDFStatus";
            this.lblPDFStatus.Size = new System.Drawing.Size(46, 17);
            this.lblPDFStatus.TabIndex = 25;
            this.lblPDFStatus.Text = "label4";
            // 
            // lblWorkbookStatus
            // 
            this.lblWorkbookStatus.AutoSize = true;
            this.lblWorkbookStatus.Location = new System.Drawing.Point(28, 238);
            this.lblWorkbookStatus.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblWorkbookStatus.Name = "lblWorkbookStatus";
            this.lblWorkbookStatus.Size = new System.Drawing.Size(46, 17);
            this.lblWorkbookStatus.TabIndex = 23;
            this.lblWorkbookStatus.Text = "label1";
            // 
            // imgPNL
            // 
            this.imgPNL.Image = global::DheccExcelAddIn2013.Properties.Resources.FailIcon;
            this.imgPNL.Location = new System.Drawing.Point(157, 133);
            this.imgPNL.Margin = new System.Windows.Forms.Padding(2);
            this.imgPNL.Name = "imgPNL";
            this.imgPNL.Size = new System.Drawing.Size(24, 26);
            this.imgPNL.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgPNL.TabIndex = 22;
            this.imgPNL.TabStop = false;
            // 
            // imgPDF
            // 
            this.imgPDF.Image = global::DheccExcelAddIn2013.Properties.Resources.FailIcon;
            this.imgPDF.Location = new System.Drawing.Point(157, 79);
            this.imgPDF.Margin = new System.Windows.Forms.Padding(2);
            this.imgPDF.Name = "imgPDF";
            this.imgPDF.Size = new System.Drawing.Size(24, 26);
            this.imgPDF.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgPDF.TabIndex = 20;
            this.imgPDF.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 33);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(369, 17);
            this.label2.TabIndex = 19;
            this.label2.Text = "Attach the following required files and click \"Upload Now\".";
            // 
            // lblPNLSummary
            // 
            this.lblPNLSummary.AutoSize = true;
            this.lblPNLSummary.Location = new System.Drawing.Point(198, 137);
            this.lblPNLSummary.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPNLSummary.Name = "lblPNLSummary";
            this.lblPNLSummary.Size = new System.Drawing.Size(105, 17);
            this.lblPNLSummary.TabIndex = 18;
            this.lblPNLSummary.Text = "No file selected";
            // 
            // btnPNLSummary
            // 
            this.btnPNLSummary.Location = new System.Drawing.Point(10, 131);
            this.btnPNLSummary.Margin = new System.Windows.Forms.Padding(2);
            this.btnPNLSummary.Name = "btnPNLSummary";
            this.btnPNLSummary.Size = new System.Drawing.Size(142, 29);
            this.btnPNLSummary.TabIndex = 17;
            this.btnPNLSummary.Text = "P and L Summary";
            this.btnPNLSummary.UseVisualStyleBackColor = true;
            this.btnPNLSummary.Click += new System.EventHandler(this.btnPNLSummary_Click);
            // 
            // lblPDFFile
            // 
            this.lblPDFFile.AutoSize = true;
            this.lblPDFFile.Location = new System.Drawing.Point(198, 86);
            this.lblPDFFile.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPDFFile.Name = "lblPDFFile";
            this.lblPDFFile.Size = new System.Drawing.Size(105, 17);
            this.lblPDFFile.TabIndex = 15;
            this.lblPDFFile.Text = "No file selected";
            // 
            // btnAttachPDF
            // 
            this.btnAttachPDF.Location = new System.Drawing.Point(90, 77);
            this.btnAttachPDF.Margin = new System.Windows.Forms.Padding(2);
            this.btnAttachPDF.Name = "btnAttachPDF";
            this.btnAttachPDF.Size = new System.Drawing.Size(62, 29);
            this.btnAttachPDF.TabIndex = 13;
            this.btnAttachPDF.Text = "PDF";
            this.btnAttachPDF.UseVisualStyleBackColor = true;
            this.btnAttachPDF.Click += new System.EventHandler(this.btnAttachPDF_Click);
            // 
            // pbUpload
            // 
            this.pbUpload.Location = new System.Drawing.Point(179, 190);
            this.pbUpload.Margin = new System.Windows.Forms.Padding(4);
            this.pbUpload.Name = "pbUpload";
            this.pbUpload.Size = new System.Drawing.Size(231, 28);
            this.pbUpload.TabIndex = 12;
            // 
            // btnUpload
            // 
            this.btnUpload.Location = new System.Drawing.Point(26, 190);
            this.btnUpload.Margin = new System.Windows.Forms.Padding(4);
            this.btnUpload.Name = "btnUpload";
            this.btnUpload.Size = new System.Drawing.Size(126, 28);
            this.btnUpload.TabIndex = 11;
            this.btnUpload.Text = "Upload Now";
            this.btnUpload.UseVisualStyleBackColor = true;
            this.btnUpload.Click += new System.EventHandler(this.btnUpload_Click_1);
            // 
            // gbInitialReview
            // 
            this.gbInitialReview.Controls.Add(this.pbInitialReviewer);
            this.gbInitialReview.Controls.Add(this.btnInitialReviewerUpload);
            this.gbInitialReview.Location = new System.Drawing.Point(14, 443);
            this.gbInitialReview.Name = "gbInitialReview";
            this.gbInitialReview.Size = new System.Drawing.Size(419, 112);
            this.gbInitialReview.TabIndex = 1;
            this.gbInitialReview.TabStop = false;
            this.gbInitialReview.Text = "Initial Review Final Upload ";
            // 
            // pbInitialReviewer
            // 
            this.pbInitialReviewer.Location = new System.Drawing.Point(167, 42);
            this.pbInitialReviewer.Margin = new System.Windows.Forms.Padding(4);
            this.pbInitialReviewer.Name = "pbInitialReviewer";
            this.pbInitialReviewer.Size = new System.Drawing.Size(231, 28);
            this.pbInitialReviewer.TabIndex = 14;
            // 
            // btnInitialReviewerUpload
            // 
            this.btnInitialReviewerUpload.Location = new System.Drawing.Point(14, 42);
            this.btnInitialReviewerUpload.Margin = new System.Windows.Forms.Padding(4);
            this.btnInitialReviewerUpload.Name = "btnInitialReviewerUpload";
            this.btnInitialReviewerUpload.Size = new System.Drawing.Size(126, 28);
            this.btnInitialReviewerUpload.TabIndex = 13;
            this.btnInitialReviewerUpload.Text = "Upload Now";
            this.btnInitialReviewerUpload.UseVisualStyleBackColor = true;
            this.btnInitialReviewerUpload.Click += new System.EventHandler(this.btnInitialReviewerUpload_Click);
            // 
            // lblSummaryStatus
            // 
            this.lblSummaryStatus.AutoSize = true;
            this.lblSummaryStatus.Location = new System.Drawing.Point(28, 275);
            this.lblSummaryStatus.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSummaryStatus.Name = "lblSummaryStatus";
            this.lblSummaryStatus.Size = new System.Drawing.Size(46, 17);
            this.lblSummaryStatus.TabIndex = 24;
            this.lblSummaryStatus.Text = "label3";
            // 
            // ucUploadClaim
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Controls.Add(this.gbInitialReview);
            this.Controls.Add(this.gbQAUpload);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "ucUploadClaim";
            this.Size = new System.Drawing.Size(450, 606);
            this.gbQAUpload.ResumeLayout(false);
            this.gbQAUpload.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgPNL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPDF)).EndInit();
            this.gbInitialReview.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbQAUpload;
        private System.Windows.Forms.Label lblPNLSummary;
        private System.Windows.Forms.Button btnPNLSummary;
        private System.Windows.Forms.Label lblPDFFile;
        private System.Windows.Forms.Button btnAttachPDF;
        private System.Windows.Forms.ProgressBar pbUpload;
        private System.Windows.Forms.Button btnUpload;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox imgPDF;
        private System.Windows.Forms.PictureBox imgPNL;
        private System.Windows.Forms.Label lblPDFStatus;
        private System.Windows.Forms.Label lblWorkbookStatus;
        private System.Windows.Forms.GroupBox gbInitialReview;
        private System.Windows.Forms.ProgressBar pbInitialReviewer;
        private System.Windows.Forms.Button btnInitialReviewerUpload;
        private System.Windows.Forms.Label lblSummaryStatus;


    }
}
