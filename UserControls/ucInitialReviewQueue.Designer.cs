﻿namespace DheccExcelAddIn2013.UserControls
{
    partial class ucInitialReviewQueue
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlSearch = new System.Windows.Forms.Panel();
            this.aReset = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.tbSearch = new System.Windows.Forms.TextBox();
            this.lblSearchText1 = new System.Windows.Forms.Label();
            this.pnlGrid = new System.Windows.Forms.Panel();
            this.cbOnlyNew = new System.Windows.Forms.CheckBox();
            this.dgInitialReview = new System.Windows.Forms.DataGridView();
            this.pnlSearch.SuspendLayout();
            this.pnlGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgInitialReview)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlSearch
            // 
            this.pnlSearch.Controls.Add(this.aReset);
            this.pnlSearch.Controls.Add(this.btnSearch);
            this.pnlSearch.Controls.Add(this.tbSearch);
            this.pnlSearch.Controls.Add(this.lblSearchText1);
            this.pnlSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlSearch.Location = new System.Drawing.Point(0, 0);
            this.pnlSearch.Margin = new System.Windows.Forms.Padding(2);
            this.pnlSearch.Name = "pnlSearch";
            this.pnlSearch.Size = new System.Drawing.Size(450, 49);
            this.pnlSearch.TabIndex = 40;
            // 
            // aReset
            // 
            this.aReset.AutoSize = true;
            this.aReset.Location = new System.Drawing.Point(375, 18);
            this.aReset.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.aReset.Name = "aReset";
            this.aReset.Size = new System.Drawing.Size(45, 17);
            this.aReset.TabIndex = 39;
            this.aReset.Text = "Reset";
            this.aReset.Click += new System.EventHandler(this.aReset_Click_1);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(312, 15);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(2);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(46, 23);
            this.btnSearch.TabIndex = 38;
            this.btnSearch.Text = "Go";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click_1);
            // 
            // tbSearch
            // 
            this.tbSearch.Location = new System.Drawing.Point(94, 16);
            this.tbSearch.Margin = new System.Windows.Forms.Padding(2);
            this.tbSearch.Name = "tbSearch";
            this.tbSearch.Size = new System.Drawing.Size(197, 22);
            this.tbSearch.TabIndex = 37;
            // 
            // lblSearchText1
            // 
            this.lblSearchText1.AutoSize = true;
            this.lblSearchText1.Location = new System.Drawing.Point(30, 18);
            this.lblSearchText1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSearchText1.Name = "lblSearchText1";
            this.lblSearchText1.Size = new System.Drawing.Size(53, 17);
            this.lblSearchText1.TabIndex = 36;
            this.lblSearchText1.Text = "Search";
            // 
            // pnlGrid
            // 
            this.pnlGrid.Controls.Add(this.cbOnlyNew);
            this.pnlGrid.Controls.Add(this.dgInitialReview);
            this.pnlGrid.Location = new System.Drawing.Point(0, 54);
            this.pnlGrid.Name = "pnlGrid";
            this.pnlGrid.Size = new System.Drawing.Size(447, 549);
            this.pnlGrid.TabIndex = 42;
            // 
            // cbOnlyNew
            // 
            this.cbOnlyNew.AutoSize = true;
            this.cbOnlyNew.Location = new System.Drawing.Point(14, 7);
            this.cbOnlyNew.Name = "cbOnlyNew";
            this.cbOnlyNew.Size = new System.Drawing.Size(373, 21);
            this.cbOnlyNew.TabIndex = 1;
            this.cbOnlyNew.Text = "Only show claims that have previously been unopened";
            this.cbOnlyNew.UseVisualStyleBackColor = true;
            this.cbOnlyNew.CheckedChanged += new System.EventHandler(this.cbOnlyNew_CheckedChanged);
            // 
            // dgInitialReview
            // 
            this.dgInitialReview.AllowUserToResizeRows = false;
            this.dgInitialReview.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgInitialReview.Location = new System.Drawing.Point(4, 39);
            this.dgInitialReview.MultiSelect = false;
            this.dgInitialReview.Name = "dgInitialReview";
            this.dgInitialReview.RowTemplate.Height = 24;
            this.dgInitialReview.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgInitialReview.Size = new System.Drawing.Size(440, 507);
            this.dgInitialReview.TabIndex = 0;
            this.dgInitialReview.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgInitialReview_CellContentDoubleClick);
            this.dgInitialReview.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgInitialReview_DataBindingComplete);
            // 
            // ucInitialReviewQueue
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlGrid);
            this.Controls.Add(this.pnlSearch);
            this.Name = "ucInitialReviewQueue";
            this.Size = new System.Drawing.Size(450, 606);
            this.pnlSearch.ResumeLayout(false);
            this.pnlSearch.PerformLayout();
            this.pnlGrid.ResumeLayout(false);
            this.pnlGrid.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgInitialReview)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlSearch;
        private System.Windows.Forms.Label aReset;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TextBox tbSearch;
        private System.Windows.Forms.Label lblSearchText1;
        private System.Windows.Forms.Panel pnlGrid;
        private System.Windows.Forms.DataGridView dgInitialReview;
        private System.Windows.Forms.CheckBox cbOnlyNew;
    }
}
