﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DheccExcelAddIn2013.Forms
{
    public partial class frmAdmin : Form
    {
        public frmAdmin()
        {
            InitializeComponent();
            _isValid = false;
        }
        private bool _isValid = false;
        public bool IsValidPassword { get { return _isValid; } }
        private void btnValidatePw_Click(object sender, EventArgs e)
        {
            if (tbDevPassword.Text == "495T3$t1ng")
                _isValid = true;
            AppVariables.SetDeveloperPasswordEntered(_isValid);
            this.Close();
        }
    }
}
