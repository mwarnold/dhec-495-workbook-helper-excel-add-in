﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DheccExcelAddIn2013.Models
{
    public class MeasureModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime? ActivatedDate { get; set; }
        public DateTime? DeActivatedDate { get; set; }
        public int? OrderByIndex { get; set; }
    }
}
