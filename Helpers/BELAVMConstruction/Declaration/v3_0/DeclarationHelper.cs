﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using DheccExcelAddIn2013.Models.BELAVMConstruction.Declaration.v3_0;

namespace DheccExcelAddIn2013.Helpers.BELAVMConstruction.Declaration.v3_0
{
    public static class DeclarationHelper
    {
        public static DeclarationModel Get()
        {
            Worksheet ws = Globals.ThisAddIn.Application.Worksheets["Declaration Analysis (Source)"];
            ws.Activate();
            DeclarationModel dm = new DeclarationModel();

            OLEObject obj = (OLEObject)ws.OLEObjects("CheckBox1");
            dm.YearsProvided2007 = obj.Object.Value;

            obj = (OLEObject)ws.OLEObjects("CheckBox2");
            dm.YearsProvided2008 = obj.Object.Value;

            obj = (OLEObject)ws.OLEObjects("CheckBox3");
            dm.YearsProvided2009 = obj.Object.Value;

            obj = (OLEObject)ws.OLEObjects("CheckBox4");
            dm.YearsProvided2010 = obj.Object.Value;

            obj = (OLEObject)ws.OLEObjects("CheckBox5");
            dm.YearsProvided2011 = obj.Object.Value;

            dm.LineItemsFlagged.Add(ExcelPaster.GetCellValue(ws, "K9"));
            dm.LineItemsFlagged.Add(ExcelPaster.GetCellValue(ws, "K11"));
            dm.LineItemsFlagged.Add(ExcelPaster.GetCellValue(ws, "K13"));
            dm.LineItemsFlagged.Add(ExcelPaster.GetCellValue(ws, "K15"));
            dm.LineItemsFlagged.Add(ExcelPaster.GetCellValue(ws, "K17"));
            dm.LineItemsFlagged.Add(ExcelPaster.GetCellValue(ws, "K19"));
            dm.LineItemsFlagged.Add(ExcelPaster.GetCellValue(ws, "K21"));
            dm.LineItemsFlagged.Add(ExcelPaster.GetCellValue(ws, "K23"));

            dm.Comments.Add(ExcelPaster.GetCellValue(ws, "M9"));
            dm.Comments.Add(ExcelPaster.GetCellValue(ws, "M11"));
            dm.Comments.Add(ExcelPaster.GetCellValue(ws, "M13"));
            dm.Comments.Add(ExcelPaster.GetCellValue(ws, "M15"));
            dm.Comments.Add(ExcelPaster.GetCellValue(ws, "M17"));
            dm.Comments.Add(ExcelPaster.GetCellValue(ws, "M19"));
            dm.Comments.Add(ExcelPaster.GetCellValue(ws, "M21"));
            dm.Comments.Add(ExcelPaster.GetCellValue(ws, "M23"));

            return dm;
        }

        public static void Set(DeclarationModel dm)
        {
            Worksheet ws = Globals.ThisAddIn.Application.Worksheets["Declaration Analysis (Source)"];
            ws.Activate();
            OLEObject obj = (OLEObject)ws.OLEObjects("CheckBox1");
            obj.Object.Value = dm.YearsProvided2007;

            obj = (OLEObject)ws.OLEObjects("CheckBox2");
            obj.Object.Value = dm.YearsProvided2008;

            obj = (OLEObject)ws.OLEObjects("CheckBox3");
            obj.Object.Value = dm.YearsProvided2009;

            obj = (OLEObject)ws.OLEObjects("CheckBox4");
            obj.Object.Value = dm.YearsProvided2010;

            obj = (OLEObject)ws.OLEObjects("CheckBox5");
            obj.Object.Value = dm.YearsProvided2011;

            ExcelPaster.SetCellValue(ws, "K9", dm.LineItemsFlagged[0]);
            ExcelPaster.SetCellValue(ws, "K11", dm.LineItemsFlagged[1]);
            ExcelPaster.SetCellValue(ws, "K13", dm.LineItemsFlagged[2]);
            ExcelPaster.SetCellValue(ws, "K15", dm.LineItemsFlagged[3]);
            ExcelPaster.SetCellValue(ws, "K17", dm.LineItemsFlagged[4]);
            ExcelPaster.SetCellValue(ws, "K19", dm.LineItemsFlagged[5]);
            ExcelPaster.SetCellValue(ws, "K21", dm.LineItemsFlagged[6]);
            ExcelPaster.SetCellValue(ws, "K23", dm.LineItemsFlagged[7]);

            ExcelPaster.SetCellValue(ws, "M9", dm.Comments[0]);
            ExcelPaster.SetCellValue(ws, "M11", dm.Comments[1]);
            ExcelPaster.SetCellValue(ws, "M13", dm.Comments[2]);
            ExcelPaster.SetCellValue(ws, "M15", dm.Comments[3]);
            ExcelPaster.SetCellValue(ws, "M17", dm.Comments[4]);
            ExcelPaster.SetCellValue(ws, "M19", dm.Comments[5]);
            ExcelPaster.SetCellValue(ws, "M21", dm.Comments[6]);
            ExcelPaster.SetCellValue(ws, "M23", dm.Comments[7]);

        }
    }
}
