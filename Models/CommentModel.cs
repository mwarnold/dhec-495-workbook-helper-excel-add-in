﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DheccExcelAddIn2013.Models
{
    public class CommentModel
    {
        public int Id { get; set; }
        public int ClaimId { get; set; }
        public int ClaimantId { get; set; }
        public String Text { get; set; }
        public DateTime Date { get; set; }
        public string UserName { get; set; }
    }
}
