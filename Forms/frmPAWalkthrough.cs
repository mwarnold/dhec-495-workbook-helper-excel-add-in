﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DheccExcelAddIn2013.Forms
{
    public partial class frmPAWalkthrough : Form
    {
        public frmPAWalkthrough()
        {
            InitializeComponent();
        }

        private void btnS2_Click(object sender, EventArgs e)
        {
            tcPAWalkthrough.SelectedIndex = 1;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            tcPAWalkthrough.SelectedIndex = 2;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            tcPAWalkthrough.SelectedIndex = 3;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            tcPAWalkthrough.SelectedIndex = 4;
        }
    }
}
