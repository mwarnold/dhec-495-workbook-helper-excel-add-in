﻿using DheccExcelAddIn2013.Models;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DheccExcelAddIn2013.Helpers
{
    public static class DownloadBelHelper
    {
        public static void Set(List<string> dbList)
        {
            Worksheet ws = Globals.ThisAddIn.GetDownloadSheet();
            ws.Activate();
            Range rng = ws.Range["C2", "C" + dbList.Count()]; //default 
            ExcelPaster.PasteVerticleSingleDimension(dbList, rng);
        }

        public static void CopyDownloadFieldNames(int count)
        {
            Worksheet ws = Globals.ThisAddIn.GetDownloadSheet();
            ws.Activate();
            Range rng = ws.Range["B2", "B" + count]; //default 
            rng.Copy(Type.Missing);

        }
    }
}
