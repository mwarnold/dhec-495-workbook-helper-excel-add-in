﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DheccExcelAddIn2013.Models.BELAVMConstruction.SeafoodRetailerTest.v3_0
{
    public class SeafoodRetailerTestModel
    {
        public String SeafoodRetailerTestNotes { get; set; }
        public List<GenericList> DocumentationReferenced { get; set; }
        public List<GenericList> SeafoodVendorMixTest { get; set; }
        public string TotalFoodSales2009 { get; set; }

        public SeafoodRetailerTestModel()
        {
            DocumentationReferenced = new List<GenericList>();
            SeafoodVendorMixTest = new List<GenericList>();
        }
    }
}
