﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using DheccExcelAddIn2013.Models.BELAVMConstruction.Causation.v3_0;

namespace DheccExcelAddIn2013.Helpers.BELAVMConstruction.Causation.v3_0
{
    public static class CausationHelper
    {
        public static CausationModel Get()
        {
            Worksheet ws = Globals.ThisAddIn.Application.Worksheets["Causation"];
            ws.Activate();
            CausationModel cm = new CausationModel();

            Range rng = ws.Range["D14", "F14"];
            foreach (Range rv in rng.Columns)
                cm.ModifiedVTestNonLocalCustomer.Add(string.IsNullOrEmpty(rv.Value2) ? "" : rv.Value2);
         
            rng = ws.Range["D15", "F15"];
            foreach (Range rv in rng.Columns)
                cm.ModifiedVTestZonesAToC.Add(string.IsNullOrEmpty(rv.Value2) ? "" : rv.Value2);
         
            rng = ws.Range["D19", "F19"];
            foreach (Range rv in rng.Columns)
                cm.DownOnlyTestNonLocalCustomer.Add(string.IsNullOrEmpty(rv.Value2) ? "" : rv.Value2);
        
            rng = ws.Range["D20", "F20"];
            foreach (Range rv in rng.Columns)
                cm.DownOnlyTestZonesAToC.Add(string.IsNullOrEmpty(rv.Value2) ? "" : rv.Value2);
          

            cm.WasThe2011MonthlyPnLAndTaxReturnProvided = ExcelPaster.GetCellValue(ws, "D3");

            OLEObject obj = (OLEObject)ws.OLEObjects("Seafood_Retailer");
            cm.ShowSeafoodRetailerTest = obj.Object.Value;
            return cm;
        }

        public static void Set(CausationModel cm)
        {
            Worksheet ws = Globals.ThisAddIn.Application.Worksheets["Causation"];
            ws.Activate();
            Range rng = ws.Range["D14", "F14"]; ;
            rng.Cells[1, 1].Value2 = cm.ModifiedVTestNonLocalCustomer[0];
            rng.Cells[1, 2].Value2 = cm.ModifiedVTestNonLocalCustomer[1];
            rng.Cells[1, 3].Value2 = cm.ModifiedVTestNonLocalCustomer[2];

            rng = ws.get_Range("D15", "F15").Rows[0]; ;
            rng.Cells[1, 1].Value2 = cm.ModifiedVTestZonesAToC[0];
            rng.Cells[1, 2].Value2 = cm.ModifiedVTestZonesAToC[1];
            rng.Cells[1, 3].Value2 = cm.ModifiedVTestZonesAToC[2];

            rng = ws.get_Range("D19", "F19").Rows[0]; ;
            rng.Cells[1, 1].Value2 = cm.DownOnlyTestNonLocalCustomer[0];
            rng.Cells[1, 2].Value2 = cm.DownOnlyTestNonLocalCustomer[1];
            rng.Cells[1, 3].Value2 = cm.DownOnlyTestNonLocalCustomer[2];

            rng = ws.get_Range("D20", "F20").Rows[0]; ;
            rng.Cells[1, 1].Value2 = cm.DownOnlyTestZonesAToC[0];
            rng.Cells[1, 2].Value2 = cm.DownOnlyTestZonesAToC[1];
            rng.Cells[1, 3].Value2 = cm.DownOnlyTestZonesAToC[2];

           ExcelPaster.SetCellValue(ws, "D3", cm.WasThe2011MonthlyPnLAndTaxReturnProvided);
            
            OLEObject obj = (OLEObject)ws.OLEObjects("Seafood_Retailer");
            obj.Object.Value = cm.ShowSeafoodRetailerTest;
        }
    }
}
