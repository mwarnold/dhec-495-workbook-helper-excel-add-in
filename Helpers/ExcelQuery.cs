﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DheccExcelAddIn2013.Helpers
{
    public class ExcelQuery
    {
        public DataSet QueryExcelFile(string filePath, string range, string sheetName)
        {
            List<string> ExcelSheetList = new List<string>();
            String connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data source='" + filePath + "';Extended Properties='Excel 12.0;HDR=YES;IMEX=1';";
            // Create connection object by using the preceding connection string.
            OleDbConnection ExcelCon = new OleDbConnection(connString);
            // Open connection with the database.

            // Get the data table containg the schema guid.
            ExcelCon.Open();
            DataTable dt = ExcelCon.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
            // Add the sheet name to the string array.
            foreach (DataRow row in dt.Rows)
                ExcelSheetList.Add(row["TABLE_NAME"].ToString());
            ExcelCon.Close();
            DataSet ds = new DataSet();

            string excelSql;
            foreach (var sn in ExcelSheetList)
            {
                excelSql = string.Format("Select * FROM [{0}{1}]", sn.Replace("'", ""), range);
                dt = new System.Data.DataTable();
                string conString = "Provider=Microsoft.ACE.OLEDB.12.0;Data source='" + filePath + "'; Extended Properties= 'Excel 12.0;HDR=YES;IMEX=1';";
                ExcelCon = new OleDbConnection(conString);
                OleDbDataAdapter ExcelAdp;
                OleDbCommand ExcelComm;
                ExcelCon.Open();

                ExcelComm = new OleDbCommand(excelSql, ExcelCon);
                ExcelAdp = new OleDbDataAdapter(ExcelComm);

                System.Data.DataTable dataTable = new System.Data.DataTable();
                try
                {
                    ExcelAdp.Fill(dataTable);
                }
                catch (Exception) { continue; }
                ExcelCon.Close();
                if (dataTable.Rows.Count > 0)
                    ds.Tables.Add(dataTable);
                ExcelComm.Dispose();
                ExcelAdp.Dispose();
            }
            return ds;
        }
    }
}
