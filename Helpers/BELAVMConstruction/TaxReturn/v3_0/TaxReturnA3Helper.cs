﻿using DheccExcelAddIn2013.Models.BELAVMConstruction.TaxReturn.v3_0;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using DheccExcelAddIn2013.Models;

namespace DheccExcelAddIn2013.Helpers.BELAVMConstruction.TaxReturn.v3_0
{
    public static class TaxReturnA3Helper
    {
        public static TaxReturnA3Model Get()
        { 
            Worksheet ws = Globals.ThisAddIn.Application.Worksheets["A3) Tax Return"];
            ws.Activate();
            TaxReturnA3Model tr = new TaxReturnA3Model();
            tr.TaxReturnFormUsed = ExcelPaster.GetCellValue(ws, "H3");
            tr.MonthOfTaxYearEnd = ExcelPaster.GetCellValue(ws, "H5");
            tr.MethodOFAcocuntingPerTaxReturn = ExcelPaster.GetCellValue(ws, "L5");

            Range rng = ws.get_Range("H12", "M12");
            List<string> gl = new List<string>();
            gl.Add(rng.Cells[1, 1].Value2 == null ? "" : Convert.ToString(rng.Cells[1, 1].Value2));
            gl.Add(rng.Cells[1, 2].Value2 == null ? "" : Convert.ToString(rng.Cells[1, 2].Value2));
            gl.Add(rng.Cells[1, 3].Value2 == null ? "" : Convert.ToString(rng.Cells[1, 3].Value2));
            gl.Add(rng.Cells[1, 4].Value2 == null ? "" : Convert.ToString(rng.Cells[1, 4].Value2));
            gl.Add(rng.Cells[1, 5].Value2 == null ? "" : Convert.ToString(rng.Cells[1, 5].Value2));
            gl.Add(rng.Cells[1, 6].Value2 == null ? "" : Convert.ToString(rng.Cells[1, 6].Value2));
            tr.GrossReceiptsOrSalesPerTaxReturn = gl;

            rng = ws.get_Range("H17", "M17");
            gl = new List<string>();
            gl.Add(rng.Cells[1, 1].Value2 == null ? "" : Convert.ToString(rng.Cells[1, 1].Value2));
            gl.Add(rng.Cells[1, 2].Value2 == null ? "" : Convert.ToString(rng.Cells[1, 2].Value2));
            gl.Add(rng.Cells[1, 3].Value2 == null ? "" : Convert.ToString(rng.Cells[1, 3].Value2));
            gl.Add(rng.Cells[1, 4].Value2 == null ? "" : Convert.ToString(rng.Cells[1, 4].Value2));
            gl.Add(rng.Cells[1, 5].Value2 == null ? "" : Convert.ToString(rng.Cells[1, 5].Value2));
            gl.Add(rng.Cells[1, 6].Value2 == null ? "" : Convert.ToString(rng.Cells[1, 6].Value2));
            tr.NetIncomeLossPerTaxReturn = gl;

            OLEObject obj = (OLEObject)ws.OLEObjects("chkTaxRecon");
            tr.TaxReconsiliationTab = obj.Object.Value;

            obj = (OLEObject)ws.OLEObjects("chkShowM3");
            tr.ShowScheduleM3 = obj.Object.Value;

            rng = ws.get_Range("B24", "AZ581");
            GenericList genList = new GenericList();
            foreach (Range r in rng.Rows)
            {
                foreach (Range c in rng.Cells)
                {
                    if (!c.HasFormula)
                        genList.Items.Add(c.Value2 == null ? "" :c.Value2.ToString());
                }
            }
            tr.TaxFormData = genList;
            return tr;
        }

        public static void Set(TaxReturnA3Model tr)
        {
            Worksheet ws = Globals.ThisAddIn.Application.Worksheets["A3) Tax Return"];
            ws.Activate();
            ExcelPaster.SetCellValue(ws, "H3", tr.TaxReturnFormUsed);
            ExcelPaster.SetCellValue(ws, "H5",tr.MonthOfTaxYearEnd);
            ExcelPaster.SetCellValue(ws, "L5",tr.MethodOFAcocuntingPerTaxReturn);

            Range rng = ws.get_Range("H12", "M12");
            ExcelPaster.PasteHorizontalSingleDimension(tr.GrossReceiptsOrSalesPerTaxReturn, rng);
           
            rng = ws.get_Range("H17", "M17");
            ExcelPaster.PasteHorizontalSingleDimension(tr.NetIncomeLossPerTaxReturn, rng);
           
            OLEObject obj = (OLEObject)ws.OLEObjects("chkTaxRecon");
            obj.Object.Value = tr.TaxReconsiliationTab;

            obj = (OLEObject)ws.OLEObjects("chkShowM3");
            obj.Object.Value = tr.ShowScheduleM3;

            rng = ws.get_Range("B24", "AZ581");
            GenericList genList = new GenericList();
            int i = 0;
            foreach (Range r in rng.Rows)
            {
                foreach (Range c in rng.Cells)
                {
                    if (!c.HasFormula())
                        continue;
                    c.Value2 = tr.TaxFormData.Items[i];
                    i++;
                }
            }
            tr.TaxFormData = genList;
        }
    }
}
