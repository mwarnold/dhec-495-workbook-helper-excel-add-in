﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DheccExcelAddIn2013.Models.BELAVMConstruction.TotalCalculatedLoss.v3_0
{
    public class TotalCalculatedLossModel
    {
        public string ClaimsAdministratorSelected { get; set; }
        public string PleaseSelectStep1Option { get; set; }
        public string PleaseSelectStep21Option { get; set; }
    }
}
