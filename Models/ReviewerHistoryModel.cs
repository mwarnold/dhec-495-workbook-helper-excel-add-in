﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DheccExcelAddIn2013.Models
{
    public class ReviewerHistoryModel
    {
        public string ReviewedBy { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
    }
}
