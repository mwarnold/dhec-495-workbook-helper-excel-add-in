﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DheccExcelAddIn2013.Interfaces;
using System.IO;
using DheccExcelAddIn2013.Helpers;
using DheccExcelAddIn2013.Models;
using DheccExcelAddIn2013.Forms;


namespace DheccExcelAddIn2013.UserControls
{
    public partial class ucMyAccount : UserControl
    {
        private IDheccContract iContract;
        public TabControl MATabControl;
        public ucMyAccount()
        {
            this.iContract = new DheccContract();
            InitializeComponent();
            MATabControl = tcMyAccount;
        }
        public void RefreshControl()
        {
            CheckForNotices();
            lblWelcomeMessage.Text = "Welcome, " + iContract.AppSetting().User.UserName;
            string url = iContract.AppSetting().WebServiceURL;
            ddlServiceUrl.Text = iContract.AppSetting().WebServiceEnvironment;
            if (string.IsNullOrEmpty(url))
            {
                tcMyAccount.SelectedIndex = 2;
                MessageBox.Show("Please set the web service url.");
            }
            btnUpdate.Enabled = false;
          
        }

        private void ucMyAccount_Load(object sender, EventArgs e)
        {

        }

        private bool NewVersionAvailable()
        {
            string v = iContract.Version();
            try
            {
                string cv = iContract.CurrentAvailableVersion().ToLower();
                if (iContract.AppSetting().UserAddIn.Version.ToLower() == cv)
                    return false;
                else
                    return true;
            }
            catch { return false; }
        }


        private void btnMaUploadChanges_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            //do upload here

            Cursor.Current = Cursors.Arrow;
        }

        private void CheckForNotices()
        {
            bool isNotice = false;
            StringBuilder sb = new StringBuilder();
            if (!string.IsNullOrEmpty(iContract.CurrentAvailableVersion()))
            {
                if (iContract.CurrentAvailableVersion().ToLower() != iContract.AppSetting().UserAddIn.Version.ToLower())
                {
                    isNotice = true;
                    sb.Append("\n\nPlease update your version of the DHECC Excel Add-In to the newest version " + iContract.CurrentAvailableVersion());
                    tcMyAccount.SelectedIndex = 0;
                }
            }
            if (isNotice)
            {
                tcMyAccount.SelectedTab = tpMaNotifications;
                tbMAMessages.Text = sb.ToString();
            }
            else
                tbMAMessages.Text = "You have no notifications.";
            try
            {
                lblWelcomeMessage.Text = string.Format("Welcome, {0}", iContract.AppSetting().User.UserName);
            }
            catch
            {
                iContract.AppSetting().User.UserName = "Not Set - (Pending Login)";
                iContract.AppSettingHelper().SaveSettings(iContract.AppSetting());
            }
        }

        private void btnGetLatestVersion_Click(object sender, EventArgs e)
        {

        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (Globals.ThisAddIn.Application.Workbooks.Count > 1 || AppVariables.IsLoggedIn)
            {
                MessageBox.Show("You must close all workbooks and log out before making this change.");
                return;
            }
            string selectedEnv = ddlServiceUrl.Text;
            if (selectedEnv == "--Environments--")
                return;
            string url = "https://www3.browngreer.com/dheccexcel_ws/service.asmx";
            if (selectedEnv == "Training")
                url = "https://495.deepwaterhorizoneconomicsettlement.com/495_BGFE/Service.asmx";
            else if (selectedEnv == "Live")
                url = "";
           
            if (string.IsNullOrEmpty(url))
            {
                MessageBox.Show("Url cannot be blank");
                return;
            }
            iContract.AppSetting().WebServiceURL = url;
            iContract.AppSetting().WebServiceEnvironment = selectedEnv;
            new AppSettingHelper().SaveSettings(iContract.AppSetting());
            btnUpdate.Enabled = false;
            iContract.CurrentWorkbookWrapper().ReviewerHistory.Add
                   (new ReviewerHistoryModel()
                   {
                       Date = DateTime.Now,
                       Description = "Environment changed to: " + selectedEnv,
                       ReviewedBy = iContract.AppSetting().User.UserName,
                   });

            MessageBox.Show("Successfully set to " + selectedEnv);
            //Globals.ThisAddIn.SetEnvironmentIndicator();
            Globals.ThisAddIn.SetCustomTaskPane("My Account");
        }

        private void ddlServiceUrl_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlServiceUrl.Text == "--Environments--")
                btnUpdate.Enabled = false;
            else
                btnUpdate.Enabled = true;
        }

        private void tcMyAccount_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcMyAccount.SelectedIndex == 2 && (Globals.ThisAddIn.Application.Workbooks.Count > 1 || AppVariables.IsLoggedIn))
            {
                MessageBox.Show("You must be logged out of the Add In and all workbooks must be closed prior to changing enviornments.");
                return;
            }
        }

        private void btnViewInst_Click(object sender, EventArgs e)
        {
            Globals.ThisAddIn.OpenVBAAccessHelpFile();
        }

        private void btnDemoDownloadDiff_Click(object sender, EventArgs e)
        {
            if (AppVariables.IsLoggedIn)
            {
                var client = AppVariables.BGWebService();
                com.browngreer.www3.clsDownloadExcelInfo belData = client.GetDownloadExcelInfo(AppVariables.CurrentUserId,
                    iContract.CurrentWorkbookWrapper().ClaimInfo.ClaimantId, iContract.CurrentWorkbookWrapper().ClaimInfo.ClaimId, AppVariables.AuthToken);

                List<string> currentDBList = iContract.CurrentWorkbookWrapper().ClaimInfo.DownloadBel;
                List<string> updatedDbList = new List<string>();
                foreach (var bv in belData.DownloadExcelAttrInfoList)
                    updatedDbList.Add(string.IsNullOrEmpty(bv.AttributeValue) ? "" : bv.AttributeValue);
                var diff = currentDBList.Except(updatedDbList);

                frmDownloadChanged frmDC = new frmDownloadChanged(currentDBList, updatedDbList);
                frmDC.Show();
            }     
        }
    }
}
