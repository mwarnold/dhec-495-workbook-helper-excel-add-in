﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DheccExcelAddIn2013.Models.BELAVMConstruction.TaxVariance.v3_0
{
    public class TaxVarianceA31Model
    {
        public List<string> GrossReceiptsOrSalesPerTaxReturn { get; set; }
        public List<string> NetIncomeLossPerTaxReturn { get; set; }
        public List<string> Documentation { get; set; }
        public List<string> DetailedAccounts { get; set; }
        public List<GenericList> PAndLGeneralAcctDetails { get; set; }
        public List<GenericList> TaxReturnGeneralAcctDetails { get; set; }
        public List<GenericList> VarianceOf { get; set; }

        public TaxVarianceA31Model()
        {
            GrossReceiptsOrSalesPerTaxReturn = new List<string>();
            NetIncomeLossPerTaxReturn = new List<string>();
            Documentation = new List<string>();
            DetailedAccounts = new List<string>();
            PAndLGeneralAcctDetails = new List<GenericList>();
            TaxReturnGeneralAcctDetails = new List<GenericList>();
            VarianceOf = new List<GenericList>();
        }
    }
}
