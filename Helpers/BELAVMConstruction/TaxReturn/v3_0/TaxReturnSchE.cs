﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using DheccExcelAddIn2013.Models.BELAVMConstruction.TaxReturn.v3_0;
using DheccExcelAddIn2013.Models;

namespace DheccExcelAddIn2013.Helpers.BELAVMConstruction.TaxReturn.v3_0
{
    public static class TaxReturnSchEHelper
    {
        public static TaxReturnSchEModel Get()
        {
            TaxReturnSchEModel trm = new TaxReturnSchEModel();
            Worksheet ws = Globals.ThisAddIn.Application.Worksheets["A3) Tax Return - Sch E"];
            ws.Activate();
            Range rng = ws.get_Range("G7", "EA7");
            foreach (var rv in rng.Value2)
                trm.PhysicalAddress.Add(ExcelPaster.FormatRangeValueToString(rv));

            rng = ws.get_Range("G8", "EA8");
            foreach (var rv in rng.Value2)
                trm.UsedByFamily.Add(ExcelPaster.FormatRangeValueToString(rv));

            rng = ws.get_Range("G9", "EA9");
            foreach (var rv in rng.Value2)
                trm.RentsReceived.Add(ExcelPaster.FormatRangeValueToString(rv));

            rng = ws.get_Range("G10", "EA10");
            foreach (var rv in rng.Value2)
                trm.RoyaltiesReceived.Add(ExcelPaster.FormatRangeValueToString(rv));

            rng = ws.get_Range("G11", "EA11");
            foreach (var rv in rng.Value2)
                trm.Advertising.Add(ExcelPaster.FormatRangeValueToString(rv));

            rng = ws.get_Range("G12", "EA12");
            foreach (var rv in rng.Value2)
                trm.AutoAndTravel.Add(ExcelPaster.FormatRangeValueToString(rv));

            rng = ws.get_Range("G13", "EA13");
            foreach (var rv in rng.Value2)
                trm.CleaningAndMaintenance.Add(ExcelPaster.FormatRangeValueToString(rv));

            rng = ws.get_Range("G14", "EA14");
            foreach (var rv in rng.Value2)
                trm.Commissions.Add(ExcelPaster.FormatRangeValueToString(rv));

            rng = ws.get_Range("G15", "EA15");
            foreach (var rv in rng.Value2)
                trm.Insurance.Add(ExcelPaster.FormatRangeValueToString(rv));

            rng = ws.get_Range("G16", "EA16");
            foreach (var rv in rng.Value2)
                trm.LegalAndOtherProfessionalFees.Add(ExcelPaster.FormatRangeValueToString(rv));

            rng = ws.get_Range("G17", "EA17");
            foreach (var rv in rng.Value2)
                trm.ManagementFees.Add(ExcelPaster.FormatRangeValueToString(rv));

            rng = ws.get_Range("G18", "EA18");
            foreach (var rv in rng.Value2)
                trm.MortgageInterestPaidToBanksEtc.Add(ExcelPaster.FormatRangeValueToString(rv));

            rng = ws.get_Range("G19", "EA19");
            foreach (var rv in rng.Value2)
                trm.OtherInterest.Add(ExcelPaster.FormatRangeValueToString(rv));

            rng = ws.get_Range("G20", "EA20");
            foreach (var rv in rng.Value2)
                trm.Repairs.Add(ExcelPaster.FormatRangeValueToString(rv));

            rng = ws.get_Range("G21", "EA21");
            foreach (var rv in rng.Value2)
                trm.Supplies.Add(ExcelPaster.FormatRangeValueToString(rv));

            rng = ws.get_Range("G22", "EA22");
            foreach (var rv in rng.Value2)
                trm.Taxes.Add(ExcelPaster.FormatRangeValueToString(rv));
            
            rng = ws.get_Range("G23", "EA23");
            foreach (var rv in rng.Value2)
                trm.Utilities.Add(ExcelPaster.FormatRangeValueToString(rv));

            rng = ws.get_Range("D25", "EA150");
            int i = 25;
            foreach (Range r in rng.Rows)
            {
                string val = r.Cells[i, 1].Value2;
                if (string.IsNullOrEmpty(val) || val.Contains("Depreciation"))
                    break;
                GenericList gl = new GenericList();
                foreach (var rv in r.Value2)
                    gl.Items.Add(ExcelPaster.FormatRangeValueToString(rv));
                trm.Other.Add(gl);
                i++;
            }

            rng = ws.get_Range("D40", "EA40");
            foreach (var rv in rng.Value2)
                trm.DepreciationExpensesOrDepletion.Add(ExcelPaster.FormatRangeValueToString(rv));

            rng = ws.get_Range("D43", "EA43");
            foreach (var rv in rng.Value2)
                trm.DeductibleRentalRealEstateLoss.Add(ExcelPaster.FormatRangeValueToString(rv));

            return trm;
        }

        public static void Set(TaxReturnSchEModel trm)
        {
            Worksheet ws = Globals.ThisAddIn.Application.Worksheets["A3) Tax Return - Form 8825"];
            ws.Activate();
            Range rng = ws.get_Range("G7", "EA8");
            ExcelPaster.PasteHorizontalSingleDimension(trm.PhysicalAddress, rng);

            rng = ws.get_Range("H8", "EA8");
            ExcelPaster.PasteHorizontalSingleDimension(trm.UsedByFamily, rng);

            rng = ws.get_Range("H9", "EA9");
            ExcelPaster.PasteHorizontalSingleDimension(trm.RentsReceived, rng);

            rng = ws.get_Range("G10", "EA10");
            ExcelPaster.PasteHorizontalSingleDimension(trm.RoyaltiesReceived, rng);

            rng = ws.get_Range("G11", "EA11");
            ExcelPaster.PasteHorizontalSingleDimension(trm.Advertising, rng);

            rng = ws.get_Range("G12", "EA12");
            ExcelPaster.PasteHorizontalSingleDimension(trm.AutoAndTravel, rng);

            rng = ws.get_Range("G13", "EA13");
            ExcelPaster.PasteHorizontalSingleDimension(trm.CleaningAndMaintenance, rng);

            rng = ws.get_Range("G14", "EA14");
            ExcelPaster.PasteHorizontalSingleDimension(trm.Commissions, rng);

            rng = ws.get_Range("G15", "EA15");
            ExcelPaster.PasteHorizontalSingleDimension(trm.Insurance, rng);

            rng = ws.get_Range("H6", "EA16");
            ExcelPaster.PasteHorizontalSingleDimension(trm.LegalAndOtherProfessionalFees, rng);

            rng = ws.get_Range("G17", "EA17");
            ExcelPaster.PasteHorizontalSingleDimension(trm.ManagementFees, rng);

            rng = ws.get_Range("G18", "EA18");
            ExcelPaster.PasteHorizontalSingleDimension(trm.MortgageInterestPaidToBanksEtc, rng);

            rng = ws.get_Range("G19", "EA19");
            ExcelPaster.PasteHorizontalSingleDimension(trm.OtherInterest, rng);

            rng = ws.get_Range("G20", "EA20");
            ExcelPaster.PasteHorizontalSingleDimension(trm.Repairs, rng);

            rng = ws.get_Range("G21", "EA21");
            ExcelPaster.PasteHorizontalSingleDimension(trm.Supplies, rng);

            rng = ws.get_Range("G22", "EA22");
            ExcelPaster.PasteHorizontalSingleDimension(trm.Taxes, rng);

            rng = ws.get_Range("G23", "EA23");
            ExcelPaster.PasteHorizontalSingleDimension(trm.Utilities, rng);

            Worksheet wsDownloadBEL = Globals.ThisAddIn.GetDownloadSheet();
            int rc = trm.Other.Count;
            Range iRange = wsDownloadBEL.Range["B500:H" + (300 + (rc - 1))]; //the -1 accounts  for an extra index row that needs to be trimmed
            iRange.Value = ExcelPaster.GetRangeValuesToInsert(trm.Other);
            wsDownloadBEL.Activate();
            iRange.Activate();
            iRange.Cut();
            ws.Activate();
            Range sectionRange = ws.get_Range("D8", "EA8");
            sectionRange.Activate();
            sectionRange.Insert(XlInsertShiftDirection.xlShiftDown, Type.Missing);
        }
    }
}
