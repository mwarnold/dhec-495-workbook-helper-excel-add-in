﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DheccExcelAddIn2013.Models.BELAVMConstruction.Causation.v3_0
{
    public class CausationModel
    {
        public string WasThe2011MonthlyPnLAndTaxReturnProvided  {get; set;}
        public bool ShowSeafoodRetailerTest { get; set; }
        public List<string> ModifiedVTestNonLocalCustomer { get; set; }
        public List<string> ModifiedVTestZonesAToC { get; set; }
        public List<string> DownOnlyTestNonLocalCustomer { get; set; }
        public List<string> DownOnlyTestZonesAToC { get; set; }

        public CausationModel()
        {
            ModifiedVTestZonesAToC = new List<string>();
            ModifiedVTestNonLocalCustomer = new List<string>();
            DownOnlyTestZonesAToC = new List<string>();
            DownOnlyTestNonLocalCustomer = new List<string>();
        }
    }
}
