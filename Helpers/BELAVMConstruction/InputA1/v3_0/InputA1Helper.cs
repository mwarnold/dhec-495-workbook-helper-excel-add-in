﻿using DheccExcelAddIn2013.Models.BELAVMConstruction.InputA1.v3_0;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using DheccExcelAddIn2013.Models;

namespace DheccExcelAddIn2013.Helpers.BELAVMConstruction.InputA1.v3_0
{
    public static class InputA1Helper
    {
        public static InputA1Model Get()
        {
            InputA1Model itm = new InputA1Model();
            Worksheet ws = Globals.ThisAddIn.Application.Worksheets["A1) Input"];
            ws.Activate();
            Range rng = ws.get_Range("C33", "D57");
            int i = 33;
            List<GenericList> cList = new List<GenericList>();
            foreach (Range r in rng.Rows)
            {
                string val = r.Cells[1, 2].Value2;
                if (string.IsNullOrEmpty(val))
                    break;

                GenericList gl = new GenericList();
                gl.Items.Add(val);
                gl.Items.Add(r.Cells[1, 2].Value2);
                cList.Add(gl);
                i++;
            }

            OLEObject obj = (OLEObject)ws.OLEObjects("CheckBox3");
            itm.OnlyBenchmarkPeriod2009 = obj.Object.Value;

            obj = (OLEObject)ws.OLEObjects("CheckBox1");            
            itm.BenchMarkPeriod20082009 = obj.Object.Value;

            obj = (OLEObject)ws.OLEObjects("CheckBox2");            
            itm.BenchMarkPeriod20072008 = obj.Object.Value;

            itm.OptimumBenchmarkPeriod = ExcelPaster.GetCellValue(ws, "G24");
            itm.Step1Period = ExcelPaster.GetCellValue(ws, "G26");
            itm.Step2Period = ExcelPaster.GetCellValue(ws, "G28");

            obj = (OLEObject)ws.OLEObjects("CheckBox6");            
            itm.MultiFacilityClaim = obj.Object.Value;

            rng = ws.get_Range("N6", "R18");
            i = 6;
            List<Models.BELAVMConstruction.InputA1.v3_0.ReviewerHistoryModel> rhList = new List<Models.BELAVMConstruction.InputA1.v3_0.ReviewerHistoryModel>();
            foreach (Range r in rng.Rows)
            {
                GenericList gl = new GenericList();
                string revName =  r.Cells[1, 1].Value2;
                string revDate =  Convert.ToString(r.Cells[1, 3].Value2);
                string revType = r.Cells[1, 5].Value2;
                rhList.Add(
                    new Models.BELAVMConstruction.InputA1.v3_0.ReviewerHistoryModel()
                    {
                        ReviewerName = revName,
                        DateReviewed = revDate,
                        ReviewType = revType
                    });
                i++;
            }

            Range rg = ws.Range["R21", "R21"];
            decimal rVal = (Convert.ToDecimal(rg.Value2) * 100);
            itm.ClaimantPropertyOwnershipInterest = rVal.ToString();

            rg = ws.Range["R24", "R24"];
            itm.ModelType = string.IsNullOrEmpty(rg.Value2) ? "" : rg.Value2;

            obj = (OLEObject)ws.OLEObjects("CheckBox5");            
            itm.SelfManagedProperty = obj.Object.Value;

            obj = (OLEObject)ws.OLEObjects("CheckBox4");            
            itm.ShowProrationCalculator = obj.Object.Value;

            rng = ws.get_Range("N35", "T57");
            i = 35;
            List<Models.BELAVMConstruction.InputA1.v3_0.PreviousPaymentDetailsModel> ppList = new List<Models.BELAVMConstruction.InputA1.v3_0.PreviousPaymentDetailsModel>();
            foreach (Range r in rng.Rows)
            {
                GenericList gl = new GenericList();
                string val = r.Cells[1, 1].Value2;
                if (string.IsNullOrEmpty(val))
                    continue;
                string pmtCat = string.IsNullOrEmpty(r.Cells[1, 1].Value2) ? "" : r.Cells[1, 1].Value2;
                string pmtDesc = string.IsNullOrEmpty(r.Cells[1, 3].Value2) ? "" : r.Cells[1, 3].Value2;
                string pmtAmt = Convert.ToString(r.Cells[1, 5].Value2);
                 string pmtDate = Convert.ToString(r.Cells[1, 7].Value2);
                ppList.Add(
                    new Models.BELAVMConstruction.InputA1.v3_0.PreviousPaymentDetailsModel()
                    {
                        PaymentCategory = pmtCat ,
                        PaymentDescription = pmtDesc,
                        Amount = pmtAmt,
                        DatePaid = pmtDate
                    });
                i++;
            }
            itm.PreviousPaymentDetails = ppList;

            return itm;
        }

        public static void Set(InputA1Model itm)
        {
            Worksheet ws = Globals.ThisAddIn.Application.Worksheets["A1) Input"];
            ws.Activate();
            Range rng = ws.get_Range("C33", "D57");
            if (itm.InteralReviewComments.Count > 23)
                itm.InteralReviewComments = itm.InteralReviewComments.Skip(0).Take(24).ToList();

            ExcelPaster.PasteTwoDimensionalList(itm.InteralReviewComments, rng);

            OLEObject obj = (OLEObject)ws.OLEObjects("CheckBox3");
            obj.Object.Value = itm.OnlyBenchmarkPeriod2009;

            obj = (OLEObject)ws.OLEObjects("CheckBox1");            
            obj.Object.Value = itm.BenchMarkPeriod20082009;

            obj = (OLEObject)ws.OLEObjects("CheckBox2");            
            obj.Object.Value = itm.BenchMarkPeriod20072008;

            ExcelPaster.SetCellValue(ws, "G24", itm.OptimumBenchmarkPeriod);
            ExcelPaster.SetCellValue(ws, "G26", itm.Step1Period);
            ExcelPaster.SetCellValue(ws, "G28", itm.Step2Period);

            obj = (OLEObject)ws.OLEObjects("CheckBox6");            
            obj.Object.Value = itm.MultiFacilityClaim;

            rng = ws.get_Range("N6", "R18");
            int i = 0;
            object[,] valRange = new object[itm.ReviewerHistory.Count, 3];
            int rowCount = 0;

            foreach (var item in itm.ReviewerHistory)
            {
                valRange[rowCount, 0] = item.ReviewerName;
                valRange[rowCount, 1] = item.DateReviewed;
                valRange[rowCount, 2] = item.ReviewType;
                rowCount++;
            }
            rng.Value = valRange;

            ExcelPaster.SetCellValue(ws, "R21", itm.ClaimantPropertyOwnershipInterest);
            ExcelPaster.SetCellValue(ws, "R24", itm.ModelType);

            obj = (OLEObject)ws.OLEObjects("CheckBox5");            
            obj.Object.Value = itm.SelfManagedProperty;

            obj = (OLEObject)ws.OLEObjects("CheckBox4");            
            obj.Object.Value = itm.ShowProrationCalculator;

            rng = ws.get_Range("N35", "T57");
            i = 35;
            foreach (var pp in itm.PreviousPaymentDetails)
            {
                ExcelPaster.SetCellValue(ws, "N" + i, pp.PaymentCategory);
                ExcelPaster.SetCellValue(ws, "P" + i, pp.PaymentDescription);
                ExcelPaster.SetCellValue(ws, "R" + i, pp.Amount);
                ExcelPaster.SetCellValue(ws, "T" + i, pp.DatePaid);
                i = i + 2;
            }
        }
    }
}
