﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using DheccExcelAddIn2013.Models;
using DheccExcelAddIn2013.Helpers;
using DheccExcelAddIn2013.Interfaces;

namespace DheccExcelAddIn2013.UserControls
{
    public partial class ucImportWorkbook : UserControl
    {
        public ucImportWorkbook()
        {
            InitializeComponent();
        }

        private IDheccContract iContract;
        public void RefreshControl()
        {
            ddlReviewType.Text = "";
            tbFilePath.Text = "";
            ofdFileNames = new List<string>();
            iContract = new DheccContract();
        }

        private static List<string> ofdFileNames = new List<string>();
        private void btnSelectWorkbook_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = String.Empty;
            ofd.Multiselect = false;
            ofd.Filter = "Excel Files|*.xls;*.xlsx;*.xlsm";

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                ofdFileNames = ofd.FileNames.ToList();
                tbFilePath.Text = ofdFileNames.FirstOrDefault();
            }
            ValidateData();
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            int reviewQueueTypeId = AppVariables.InitialReviewerTypeId;
            switch (ddlReviewType.Text)
            {
                case "Q/A":
                    reviewQueueTypeId = AppVariables.QAReviewerTypeId;
                    break;
                case "Consistency":
                    reviewQueueTypeId = AppVariables.ConsistencyTeamReivewerTypeId;
                    break;
                default:
                    reviewQueueTypeId = AppVariables.InitialReviewerTypeId; ;
                    break;
            }
            foreach (var fn in ofdFileNames)
            {
                string strippedName = Path.GetFileName(fn);
                strippedName = strippedName.Replace(".xlsm", "_Import.xlsm");
                string lfp = AppVariables.LocalExcelFileDirectory() + strippedName;
                try
                {
                    File.Copy(fn, lfp);
                }
                catch (Exception ex)
                {
                    if (ex.ToString().Contains("exists"))
                        MessageBox.Show("File already exists. Check your local copies.");
                    else
                    {
                        EmailHelper.SendExceptionEmail(ex.ToString());
                        MessageBox.Show("An error occured while saving importing your file. Our dev team has been notified.");
                    }
                    return;
                }
                WorkbookWrapper ww = new WorkbookWrapper()
              {
                  CanEditCompensationMethod = false,
                  ReviewQueueTypeId = reviewQueueTypeId,
              };
                WorkbookModel w = new WorkbookModel()
                {
                    Active = true,
                    Id = 0,
                    FileName = strippedName,
                    LocalFilePath = lfp,
                    DateAdded = DateTime.Now
                };
                try
                {
                    ww.Workbook = w;
                    iContract.SetCurrentWorkbookWrapper(ww);
                    Globals.ThisAddIn.Application.Workbooks.Add(ww.Workbook.LocalFilePath);
                    Globals.ThisAddIn.SetActiveWorkbook();
                    var claimInfo = WorkbookTemplate.GetActiveWorkbookClaimInfo();
                    ww.ClaimInfo.ClaimantId = claimInfo.ClaimantId;
                    ww.ClaimInfo.ClaimantName = claimInfo.ClaimantName;
                    ww.ClaimInfo.ClaimId = claimInfo.ClaimId;
                    ww.CanEdit = iContract.UsersClaims().Any(x => x == ww.ClaimInfo.ClaimId);
                    ww.IsImportedClaim = true;
                    new WIPHelper().SaveWIP(ww);
                    iContract.SetCurrentWorkbookWrapper(ww);
                    Globals.ThisAddIn.SetActiveWorkbook();
                    Globals.ThisAddIn.ResetUserControls();
                    RefreshControl();
                   // WorkbookTemplate.TestAddingMacros();
                    MessageBox.Show("Successfully Imported Claim #" + ww.ClaimInfo.ClaimId + " for claimant " + ww.ClaimInfo.ClaimantName + " #" + ww.ClaimInfo.ClaimantId);
                }
                catch (Exception ex)
                {
                    EmailHelper.SendExceptionEmail(ex.ToString());
                    MessageBox.Show("An error occured while importing the workbook.");
                }
            }
        }

        private void ddlReviewType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ValidateData();
        }

        private void tbFilePath_TextChanged(object sender, EventArgs e)
        {
            ValidateData();
        }

        private void ValidateData()
        {
            if (!string.IsNullOrEmpty(tbFilePath.Text) && !string.IsNullOrEmpty(ddlReviewType.Text))
                btnImport.Enabled = true;

            else
                btnImport.Enabled = false;
        }
    }
}
