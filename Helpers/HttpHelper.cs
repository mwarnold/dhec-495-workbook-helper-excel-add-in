﻿using DheccExcelAddIn2013.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DheccExcelAddIn2013.Helpers
{
    public static class HttpHelper
    {
        private static DheccContract iContract;
        public static string DoGetRequest(string requestIn)
        {
            try
            {
                iContract = new DheccContract();
                string url = "https://" + iContract.AppSetting().WebServiceURL;
                HttpWebRequest webRequest = (HttpWebRequest)HttpWebRequest.Create(url + requestIn);
                HttpWebResponse myHttpWebResponse = (HttpWebResponse)webRequest.GetResponse();
                Stream responseStream = myHttpWebResponse.GetResponseStream();
                StreamReader reader = new StreamReader(responseStream);
                return reader.ReadToEnd();
            }
            catch
            {
                return "";
            }
        }

        public static HttpWebResponse DoPostRequest(string contractName, string xml)
        {
            iContract = new DheccContract();
            string url = "https://" + iContract.AppSetting().WebServiceURL;
            HttpWebRequest webRequest = (HttpWebRequest)HttpWebRequest.Create(url + contractName);
            StreamWriter requestWriter;
            webRequest.Method = WebRequestMethods.Http.Post;
            webRequest.ServicePoint.Expect100Continue = false;
            webRequest.Timeout = 20000;
            webRequest.ContentType = @"text/xml";
            webRequest.Accept = @"text/xml";
            //POST the data. 
            using (requestWriter = new StreamWriter(webRequest.GetRequestStream()))
            {
                requestWriter.Write(xml);
            }
            HttpWebResponse resp;
            try
            {
                resp = (HttpWebResponse)webRequest.GetResponse();
            }
            catch (WebException e) { resp = (HttpWebResponse)e.Response; }
            return resp;
        }

        public static string ReadResults(HttpWebResponse resp)
        {
            Stream resStream = resp.GetResponseStream();
            resStream = resp.GetResponseStream();
            StreamReader reader = new StreamReader(resStream);
            reader = new StreamReader(resStream);
            return reader.ReadToEnd();
        }
    }
}
