﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DheccExcelAddIn2013.Interfaces;
using System.IO;
using Microsoft.Office.Interop.Excel;
using DheccExcelAddIn2013.Helpers;
using System.Reflection;
using DheccExcelAddIn2013.Models;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace DheccExcelAddIn2013.UserControls
{
    public partial class ucMyQueue : UserControl
    {
        private IDheccContract iContract;
        private OpenFileDialog ofd = new OpenFileDialog();
        private static List<com.browngreer.www3.ClaimInfo> allClaims = new List<com.browngreer.www3.ClaimInfo>();
        private static List<com.browngreer.www3.ClaimInfo> initRevList = new List<com.browngreer.www3.ClaimInfo>();
        private static List<com.browngreer.www3.ClaimInfo> qaList = new List<com.browngreer.www3.ClaimInfo>();
        private static List<com.browngreer.www3.ClaimInfo> consistList = new List<com.browngreer.www3.ClaimInfo>();
        private static List<com.browngreer.www3.ClaimInfo> wipList = new List<com.browngreer.www3.ClaimInfo>();
        private static List<WorkbookWrapper> _wipItemsFromLocal = new List<WorkbookWrapper>();
        private static WorkbookWrapper _selectedWorkbook = new WorkbookWrapper();
        private static List<int> wipIdList = new List<int>();
        private static com.browngreer.www3.ClaimInfo clsClaimInfo = new com.browngreer.www3.ClaimInfo();

        private static string _selectedCompensationMethod = "";

        public ucMyQueue()
        {
            this.iContract = new DheccContract();
            InitializeComponent();
            HideCompMethods();
        }

        private void ShowCompMethods()
        {
            pnlGrid.Location = new System.Drawing.Point() { X = 0, Y = 360 };
            pnlGrid.Height = 811;
            tcQueue.Height = 771;
            pnlCompMethod.Visible = true;
        }

        private void HideCompMethods()
        {
            pnlCompMethod.Visible = false;
            pnlGrid.Location = new System.Drawing.Point() { X = 0, Y = 95 };
            pnlGrid.Height = 1010;
            tcQueue.Height = 970;
        }


        public void RefreshControl()
        {
            tbSearch.Text = "";
            //if this explodes, user hasn't set up programmatic access to the vba module and cannot continue. 
            try
            {
                var i = Globals.ThisAddIn.Application.ActiveWorkbook.VBProject.VBComponents.Count;
            }
            catch
            {
                MessageBox.Show("The DWH Workbook Helper Add In required programmatic access to the VBA Module. You will be redirected to the Add In Settings. In the 'Notifications' tab, you will find the instructions for setting this up in Excel. It should take less than two minutes.");
                Globals.ThisAddIn.SetCustomTaskPane("My Account");
                return;
            }
            tcQueue.Enabled = true;
            qaList = new List<com.browngreer.www3.ClaimInfo>();
            consistList = new List<com.browngreer.www3.ClaimInfo>();
            initRevList = new List<com.browngreer.www3.ClaimInfo>();
            wipList = new List<com.browngreer.www3.ClaimInfo>();
           
            if (iContract.BackgroundUploadWorker() != null)
            {
                if (iContract.BackgroundUploadWorker().IsBusy)
                {
                    WorkbookTemplate.InitialWbLoadComplete = false;
                    MessageBox.Show("The Add In is collecting files from the server. This process usually takes less than a minute depending on your internet connection. " +
                        "The Add In will display a message when this process is complete. You will be sent to the \"My Account\" area until this is process is complete. Please click ok.");
                    Globals.ThisAddIn.SetCustomTaskPane("My Account");
                }
            }
            LoadQueue();
        }

        private void LoadQueue()
        {
            try
            {
                //get list of wip 
                var wipHelper = new WIPHelper();
                wipIdList = wipHelper.GetWIP().Select(x => x.ClaimInfo.ClaimId).ToList();
                List<int> usersClaimIds = new List<int>();
                if (AppVariables.IsLoggedIn)
                {
                    var client = AppVariables.BGWebService();
                    var claims = client.GetBELExcelUserClaims(AppVariables.CurrentUserId, AppVariables.AuthToken);
                    if (!WebServiceResponseHelper.ValidateResponse(claims.Result.ResultID, claims.Result.ResultMessage))
                        return;

                    WorkbookTemplate.UpdateActiveWorkbookList(claims.WorkbookInfoList);
                  
                    foreach (var clsClaimInfo in claims.ClaimInfoList)
                    {
                        //these are all the user's queue items. 
                        //Lets split them up into New/Unworked and Work In Progress  (WIP)
                        if (wipIdList.Contains(clsClaimInfo.ClaimID))
                            wipList.Add(clsClaimInfo);
                        else
                        {
                            switch (clsClaimInfo.ReviewQueueTypeID)
                            {
                                case 26:
                                    initRevList.Add(clsClaimInfo);
                                    break;
                                case 30:
                                    qaList.Add(clsClaimInfo);
                                    break;
                                case 101:
                                    consistList.Add(clsClaimInfo);
                                    break;
                            }
                        }
                        allClaims.Add(clsClaimInfo);
                        usersClaimIds.Add(clsClaimInfo.ClaimID);
                    }
                    BindGridViews();
                    iContract.AppSetting().MyClaims = usersClaimIds;
                    iContract.AppSettingHelper().SaveSettings(iContract.AppSetting());
                }
                else
                    BindWIP();

            }
            catch (Exception ex)
            {
                WebServiceResponseHelper.ValidateResponse(0, "An unhandled exception has occured. Our dev team has been notifiied. We apologize for the inconvenience");
                EmailHelper.SendExceptionEmail(ex.ToString());
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (AppVariables.IsLoggedIn)
                BindGridViews();
            else
                BindWIP();
        }

        private void BindGridViews()
        {
            string ss = tbSearch.Text;
            var initialRev = (from ir in initRevList
                                where ir.ReviewQueueTypeID == AppVariables.InitialReviewerTypeId && 
                                (ir.ClaimantInfo.ClaimantName.ToLower().Contains(ss.ToLower()) ||
                                ir.ClaimantInfo.ClaimantID.ToString().Contains(ss) || ir.ClaimID.ToString().Contains(ss))
                              select new
                              {
                                  Claimant = string.Format("{0}-#{1}", ir.ClaimantInfo.ClaimantName, ir.ClaimantInfo.ClaimantID),
                                  Claim = ir.ClaimID
                              });
            dgInitialReview.DataSource = initialRev.ToList();

            var consisitRev = (from cr in consistList
                               where cr.ReviewQueueTypeID == AppVariables.ConsistencyTeamReivewerTypeId 
                               && (cr.ClaimantInfo.ClaimantName.ToLower().Contains(ss.ToLower()) ||
                               cr.ClaimantInfo.ClaimantID.ToString().Contains(ss) || cr.ClaimID.ToString().Contains(ss))
                               select new
                               {
                                   Claimant = string.Format("{0}-#{1}", cr.ClaimantInfo.ClaimantName, cr.ClaimantInfo.ClaimantID),
                                   Claim = cr.ClaimID
                               });
            dgConsistancy.DataSource = consisitRev.ToList();

            var qaRev = (from qa in qaList
                         where qa.ClaimantInfo.ClaimantName.ToLower().Contains(ss.ToLower()) ||
                               qa.ClaimantInfo.ClaimantID.ToString().Contains(ss) || qa.ClaimID.ToString().Contains(ss)
                         select new
                         {
                             Claimant = string.Format("{0}-#{1}", qa.ClaimantInfo.ClaimantName, qa.ClaimantInfo.ClaimantID),
                             Claim = qa.ClaimID
                         });
            dgQAReview.DataSource = qaRev.ToList();

            var wipInitialRev = (from w in wipList
                                 where w.ReviewQueueTypeID == AppVariables.InitialReviewerTypeId
                                 && (w.ClaimantInfo.ClaimantName.ToLower().Contains(ss.ToLower()) ||
                                    w.ClaimantInfo.ClaimantID.ToString().Contains(ss) || w.ClaimID.ToString().Contains(ss))
                                 select new
                                 {
                                     Claimant = string.Format("{0}-#{1}", w.ClaimantInfo.ClaimantName, w.ClaimantInfo.ClaimantID),
                                     Claim = w.ClaimID
                                 });
            dgWIP_InitialReview.DataSource = wipInitialRev.ToList();

            var wipConsistRev = (from w in wipList
                                 where w.ReviewQueueTypeID == AppVariables.ConsistencyTeamReivewerTypeId
                                   && (w.ClaimantInfo.ClaimantName.ToLower().Contains(ss.ToLower()) ||
                                    w.ClaimantInfo.ClaimantID.ToString().Contains(ss) || w.ClaimID.ToString().Contains(ss))
                                 select new
                                 {
                                     Claimant = string.Format("{0}-#{1}", w.ClaimantInfo.ClaimantName, w.ClaimantInfo.ClaimantID),
                                     Claim = w.ClaimID
                                 });
            dgWIP_Consistancy.DataSource = wipConsistRev.ToList();
            
            var wipQARev = (from w in wipList
                                where w.ReviewQueueTypeID == AppVariables.QAReviewerTypeId
                                  && (w.ClaimantInfo.ClaimantName.ToLower().Contains(ss.ToLower()) ||
                                    w.ClaimantInfo.ClaimantID.ToString().Contains(ss) || w.ClaimID.ToString().Contains(ss))
                                select new
                                {
                                    Claimant = string.Format("{0}-#{1}", w.ClaimantInfo.ClaimantName, w.ClaimantInfo.ClaimantID),
                                    Claim = w.ClaimID
                                });
            dgWIP_QA.DataSource = wipQARev.ToList();

            int width = ((dgInitialReview.Width / 3) - 15);
            int widthBig = width * 2;
            dgInitialReview.Columns[0].Width = widthBig;
            dgInitialReview.Columns[1].Width = width;

            dgConsistancy.Columns[0].Width = widthBig;
            dgConsistancy.Columns[1].Width = width;

            dgQAReview.Columns[0].Width = widthBig;
            dgQAReview.Columns[1].Width = width;

            dgWIP_QA.Columns[0].Width = widthBig;
            dgWIP_QA.Columns[1].Width = width;

            dgWIP_Consistancy.Columns[0].Width = widthBig;
            dgWIP_Consistancy.Columns[1].Width = width;

            dgWIP_InitialReview.Columns[0].Width = widthBig;
            dgWIP_InitialReview.Columns[1].Width = width;
        }

        private void BindWIP()
        {
            tcQueue.Enabled = false;
            tcQueueMain.SelectedIndex = 1;
            string ss = tbSearch.Text;
            _wipItemsFromLocal = new WIPHelper().GetWIP();
            int width = 75;
            int widthBig = 240;
            
            var wipQAList = (from w in _wipItemsFromLocal
                             where w.ReviewQueueTypeId == AppVariables.QAReviewerTypeId
                              && w.ClaimInfo.ClaimantName.ToLower().Contains(ss.ToLower()) ||
                                    w.ClaimInfo.ClaimantId.ToString().Contains(ss) || w.ClaimInfo.ClaimId.ToString().Contains(ss)
                             select new
                             {
                                 Claimant = string.Format("{0}-#{1}", w.ClaimInfo.ClaimantName, w.ClaimInfo.ClaimantId),
                                 Claim = w.ClaimInfo.ClaimId
                             });
            dgWIP_QA.DataSource = wipQAList.ToList();

            var wipInitialRevList = (from w in _wipItemsFromLocal
                                     where w.ReviewQueueTypeId == AppVariables.InitialReviewerTypeId
                                      && w.ClaimInfo.ClaimantName.ToLower().Contains(ss.ToLower()) ||
                                        w.ClaimInfo.ClaimantId.ToString().Contains(ss) || w.ClaimInfo.ClaimId.ToString().Contains(ss)
                                     select new
                                     {
                                         Claimant = string.Format("{0}-#{1}", w.ClaimInfo.ClaimantName, w.ClaimInfo.ClaimantId),
                                         Claim = w.ClaimInfo.ClaimId
                                     });
            dgWIP_InitialReview.DataSource = wipInitialRevList.ToList();

            var wipConsistancyList = (from w in _wipItemsFromLocal
                                      where w.ReviewQueueTypeId == AppVariables.ConsistencyTeamReivewerTypeId
                                         && w.ClaimInfo.ClaimantName.ToLower().Contains(ss.ToLower()) ||
                                        w.ClaimInfo.ClaimantId.ToString().Contains(ss) || w.ClaimInfo.ClaimId.ToString().Contains(ss)
                                      select new
                                      {
                                          Claimant = string.Format("{0}-#{1}", w.ClaimInfo.ClaimantName, w.ClaimInfo.ClaimantId),
                                          Claim = w.ClaimInfo.ClaimId
                                      });
            dgWIP_Consistancy.DataSource = wipConsistancyList.ToList();

            if (_wipItemsFromLocal.Count() > 0)
            {
                dgWIP_QA.Columns[0].Width = widthBig;
                dgWIP_QA.Columns[1].Width = width;

                dgWIP_Consistancy.Columns[0].Width = widthBig;
                dgWIP_Consistancy.Columns[1].Width = width;

                dgWIP_InitialReview.Columns[0].Width = widthBig;
                dgWIP_InitialReview.Columns[1].Width = width;
            }
        }

        private void DownloadClaimDataAndLoad()
        {
            var client = AppVariables.BGWebService();
            WorkbookWrapper ww = new WorkbookWrapper();
            ww.CanEdit = true;
            ww.CanEditCompensationMethod = clsClaimInfo.CompMethodOptions.PreSelectionCompMethodID > 0;
            ww.ClaimInfo.ClaimId = clsClaimInfo.ClaimID;
            ww.ClaimInfo.ClaimantId = clsClaimInfo.ClaimantInfo.ClaimantID;
            ww.ClaimInfo.ClaimantName = clsClaimInfo.ClaimantInfo.ClaimantName;
            var selCompMethod = clsClaimInfo.CompMethodOptions.CompCalcMethodIDList.SingleOrDefault(x => x.CompCalcMethodDesc == _selectedCompensationMethod);
            ww.CompensationMethodId = selCompMethod.CompCalcMethodID;
            ww.ReviewQueueTypeId = clsClaimInfo.ReviewQueueTypeID;
            ww.Workbook = iContract.AppSetting().ActiveWorkbooks
                .SingleOrDefault(x => x.WorkbookTypeId == selCompMethod.WorkbookTypeID);
            ReviewerHistoryModel rhm =
                new ReviewerHistoryModel()
                {
                    Date = DateTime.Now,
                    Description = "Claim Downloaded",
                    ReviewedBy = iContract.AppSetting().User.UserName,
                };
            ww.ReviewerHistory.Add(rhm);
            com.browngreer.www3.clsDownloadExcelInfo belData = client.GetDownloadExcelInfo(AppVariables.CurrentUserId, ww.ClaimInfo.ClaimantId,
            ww.ClaimInfo.ClaimId, AppVariables.AuthToken);
            if (!WebServiceResponseHelper.ValidateResponse(belData.Result.ResultID, belData.Result.ResultMessage))
                return;
            WorkbookTemplate.UpdateActiveWorkbookList(belData.WorkbookInfoList);
            List<string> dbList = new List<string>();
            foreach (var bv in belData.DownloadExcelAttrInfoList)
                dbList.Add(string.IsNullOrEmpty(bv.AttributeValue) ? "" : bv.AttributeValue);
            ww.AddIn = iContract.AppSetting().UserAddIn;
            ww.ClaimInfo.DownloadBel = dbList;
            if (!WorkbookTemplate.OpenNewWorkbook(ww))
            {
                MessageBox.Show("An error occured while opening the workbook template: " + ww.Workbook.Name);
                return;
            }
            DownloadBelHelper.Set(dbList);
            WorkbookTemplate.SaveWorkbook(false);
            new WIPHelper().SaveWIP(ww);

        }

        private void LoadClaimFromRemote()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                var client = AppVariables.BGWebService();
                var resp = client.GetInterimSaveInfoByHistIDorClaimInfo(AppVariables.CurrentUserId, AppVariables.AuthToken, clsClaimInfo.ClaimantInfo.ClaimantID,
                    clsClaimInfo.ClaimID, clsClaimInfo.ReviewQueueTypeID, clsClaimInfo.SaveHistID);
                if (!WebServiceResponseHelper.ValidateResponse(resp.Result.ResultID, resp.Result.ResultMessage))
                    return;
                WorkbookWrapper ww = new WorkbookWrapper();
                JObject joTemp = (JObject)JsonConvert.DeserializeObject(resp.InterimInfo.ClaimData);
                ww = (WorkbookWrapper)joTemp.ToObject<WorkbookWrapper>();
                ww.CanEdit = iContract.UsersClaims().Any(x => x == ww.ClaimInfo.ClaimId);
                ww.ReviewQueueTypeId = clsClaimInfo.ReviewQueueTypeID;
                WorkbookTemplate.OpenRemoteWorkbook(ww, (int)clsClaimInfo.SaveHistID);
                Cursor.Current = Cursors.Arrow;
            }
            catch (Exception ex)
            {
                WebServiceResponseHelper.ValidateResponse(0, "An unhandled exception has occured. Our dev team has been notifiied. We apologize for the inconvenience");
                EmailHelper.SendExceptionEmail(ex.ToString());
            }
        }


      

        private void aReset_Click(object sender, EventArgs e)
        {
            tbSearch.Text = "";
            if (AppVariables.IsLoggedIn)
                BindGridViews();
            else
                BindWIP();           
        }

        private void btnLoadClaim_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            _selectedCompensationMethod = ddlCompensationMethod.Text;
            if (string.IsNullOrEmpty(_selectedCompensationMethod))
                MessageBox.Show("Please select a compensation method.");
            else
                DownloadClaimDataAndLoad();
            Cursor.Current = Cursors.Arrow;
        }

        private void GridViewRowClicked(DataGridView gv, DataGridViewCellEventArgs e, bool isWIP)
        {
            try
            {
                if (e.RowIndex < 0)
                    return;
                Cursor.Current = Cursors.WaitCursor;
                DataGridViewRow dr = gv.Rows[e.RowIndex];
                int claimId = int.Parse(dr.Cells[1].Value.ToString());
                clsClaimInfo = allClaims.FirstOrDefault(x => x.ClaimID == claimId);

                if (isWIP || clsClaimInfo.ReviewQueueTypeID == AppVariables.QAReviewerTypeId)
                {
                    //check to see if the Q/A reviwer has prev saved
                    if (wipIdList.Contains(claimId))
                        WorkbookTemplate.OpenWorkInProgress(claimId);
                    else
                        LoadClaimFromRemote();
                }
                else //This is a brand new claim and needs to be downloaded
                {
                    //check the compensation method. If prev selected get the workbook type id and open wb. Else, display
                    //options to the user and set the options for later
                    var initCompMethod = new CompensationMethod() { CompCalcMethodId = 0, CompCalcMethodDesc = "", ClaimId = 0, WorkbookTypeId = 0 };
                    List<CompensationMethod> compList = new List<CompensationMethod>();
                    if (clsClaimInfo.CompMethodOptions.PreSelectionCompMethodID > 0)
                    {
                        var preSelectedCompMeth = clsClaimInfo.CompMethodOptions.CompCalcMethodIDList.FirstOrDefault(x => x.CompCalcMethodID == clsClaimInfo.CompMethodOptions.PreSelectionCompMethodID);
                        compList.Add(new CompensationMethod()
                        {
                            CompCalcMethodId = clsClaimInfo.CompMethodOptions.PreSelectionCompMethodID,
                            CompCalcMethodDesc = preSelectedCompMeth.CompCalcMethodDesc,
                            WorkbookTypeId = preSelectedCompMeth.WorkbookTypeID,
                            ClaimId = preSelectedCompMeth.ClaimID
                        });
                        _selectedCompensationMethod = compList.FirstOrDefault().CompCalcMethodDesc;
                        DownloadClaimDataAndLoad();
                    }
                    else
                    {
                        foreach (var c in clsClaimInfo.CompMethodOptions.CompCalcMethodIDList)
                        {
                            if (!compList.Select(x => x.CompCalcMethodDesc).Contains(c.CompCalcMethodDesc))
                                compList.Add(
                                    new CompensationMethod()
                                    {
                                        CompCalcMethodDesc = c.CompCalcMethodDesc,
                                        CompCalcMethodId = c.CompCalcMethodID,
                                        WorkbookTypeId = c.WorkbookTypeID,
                                        ClaimId = c.ClaimID
                                    });
                        }
                        if (compList.Count > 1)
                        {
                            ddlCompensationMethod.Visible = true;
                            compList.Insert(0, initCompMethod);
                            ddlCompensationMethod.DataSource = compList.Select(x => x.CompCalcMethodDesc).ToList();
                            ShowCompMethods();
                            ddlCompensationMethod.Enabled = true;
                            lblCompMethodTxt.Text = "Please select a compensation method for claim #" + claimId;
                        }
                        else
                        {
                            _selectedCompensationMethod = compList.FirstOrDefault().CompCalcMethodDesc;
                            DownloadClaimDataAndLoad();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WebServiceResponseHelper.ValidateResponse(0, "An unhandled exception has occured. Our dev team has been notifiied. We apologize for the inconvenience");
                EmailHelper.SendExceptionEmail(ex.ToString());
            }
        }

        private void gvInitialReview_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            GridViewRowClicked(dgInitialReview, e, false);
        }

        private void dgQAReview_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            GridViewRowClicked(dgQAReview, e, false);
        }

        private void dgConsistancy_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            GridViewRowClicked(dgConsistancy, e, false);
        }

        private void dgWIP_InitialReview_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            GridViewRowClicked(dgWIP_InitialReview, e, true);
        }

        private void dgWIP_QA_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            GridViewRowClicked(dgWIP_QA, e, true);
        }

        private void dgWIP_Consistancy_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            GridViewRowClicked(dgWIP_Consistancy , e, true);
        }     

    }
}
