﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DheccExcelAddIn2013.Models
{
    public class MacroModel
    {
        public int Id { get; set; }
        public int WorkbookId { get; set; }
        public DateTime? DateAdded { get; set; }
        public bool Active { get; set; }
        public string Description { get; set; }
        public string FileType { get; set; }
        public string CodeName { get; set; }
        public string ModuleName { get; set; }
        public string ServerFilePath { get; set; }
    }
}
