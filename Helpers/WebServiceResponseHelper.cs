﻿using DheccExcelAddIn2013.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DheccExcelAddIn2013.Helpers
{
    public static class WebServiceResponseHelper
    {
        private static IDheccContract iContract = new DheccContract();
        public static bool ValidateResponse(int responseId, string responseMessage)
        {
           if (responseId == 0 && AppVariables.IsLoggedIn) 
            {
                MessageBox.Show(responseMessage);
                if (responseMessage.ToLower().Contains("session"))
                {
                    AppVariables.SetIsLoggedIn(false);
                    AppVariables.SetCurrentUserId(0);
                    AppVariables.SetAuthToken("");
                    //Globals.ThisAddIn.AddUserControlsToTaskPaneCollection();
                    Globals.ThisAddIn.SetCustomTaskPane("Reset");
                    iContract.ShowHideButtons();
                }
                return false;
            }
           else if (responseId == 0)
                return false;
           return true;
        }

    }
}
