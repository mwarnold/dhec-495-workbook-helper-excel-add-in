﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DheccExcelAddIn2013.Interfaces;
using DheccExcelAddIn2013.Helpers;
using DheccExcelAddIn2013.Models;

namespace DheccExcelAddIn2013.UserControls
{
    public partial class ucCompensationMethod : UserControl
    {
        private IDheccContract iContract;
        public ucCompensationMethod()
        {
            InitializeComponent();
        }

        public void RefreshControl()
        {
            //iContract = new DheccContract();
            //lblClaimantNo.Text = "Claimant #" + iContract.CurrentClaimantId().ToString();
            //lblClaimNo.Text = "Claim #" + iContract.CurrentClaimId().ToString();
            //lblCompMeth.Text = "Current Method " + iContract.UploadModel().CompensationMethod.CompCalcMethodDesc;
            //ddlCompMethods.DataSource = iContract.UploadModel().CompensationMethods.Select(x => x.CompCalcMethodDesc).ToList();
            //ddlCompMethods.Text = iContract.UploadModel().CompensationMethod.CompCalcMethodDesc;
        }
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                //if (!AppVariables.IsLoggedIn)
                //{
                //    MessageBox.Show("You must be logged in to perform this operation.");
                //    return;
                //}
                //iContract.UploadModel().ReviewerHistory.Add(
                //    new ReviewerHistoryModel()
                //    {
                //        Date = DateTime.Now,
                //        Description = "Comp method chngd to " + ddlCompMethods.SelectedValue,
                //        ReviewedBy = AppVariables.CurrentUserName,
                //    });
                //iContract.UploadModel().CompensationMethod = iContract.UploadModel().CompensationMethods
                //    .FirstOrDefault(x => x.CompCalcMethodDesc == ddlCompMethods.SelectedValue.ToString());
                //iContract.UploadModel().Workbook = iContract.ActiveWorkbooks().SingleOrDefault(x => x.WorkbookTypeId == iContract.UploadModel().CompensationMethod.WorkbookTypeId);
                //new LocalCopiesHelper().SaveLocalCopy(iContract.UploadModel());
                ////Globals.ThisAddIn.Application.ActiveWorkbook.Close(Microsoft.Office.Interop.Excel.XlSaveAction.xlDoNotSaveChanges);
                //if (!WorkbookTemplate.OpenNewWorkbook(iContract.UploadModel()))
                //{
                //    MessageBox.Show("An error occured while opening the workbook template: " + iContract.UploadModel().Workbook.Name);
                //    return;
                //}
                //DownloadBelHelper.Set(iContract.UploadModel().DownloadBel);
            }
            catch (Exception ex)
            {
                WebServiceResponseHelper.ValidateResponse(0, "An unhandled exception has occured. Our dev team has been notifiied. We apologize for the inconvenience");
                EmailHelper.SendExceptionEmail(ex.ToString());
            }
            MessageBox.Show("Compensation method updated and workbook saved.");
        }
    }
}
