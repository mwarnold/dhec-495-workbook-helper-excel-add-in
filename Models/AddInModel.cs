﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DheccExcelAddIn2013.Models
{
    public class AddInModel
    {
        public int Id { get; set; }
        public bool Active { get; set; }
        public string Version { get; set; }
        public string Description { get; set; }

    }
}
