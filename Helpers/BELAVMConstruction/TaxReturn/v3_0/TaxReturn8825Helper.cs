﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using DheccExcelAddIn2013.Models.BELAVMConstruction.TaxReturn.v3_0;
using DheccExcelAddIn2013.Models;


namespace DheccExcelAddIn2013.Helpers.BELAVMConstruction.TaxReturn.v3_0
{
    public static class TaxReturnForm8825Helper
    {
        public static TaxReturnForm8825Model Get()
        {
            TaxReturnForm8825Model trm = new TaxReturnForm8825Model();
            Worksheet ws = Globals.ThisAddIn.Application.Worksheets["A3) Tax Return - Form 8825"];
            ws.Activate();
            Range rng = ws.get_Range("H8", "EB8");
            foreach (var rv in rng.Value2)
                trm.PhysicalAddress.Add(ExcelPaster.FormatRangeValueToString(rv));

            rng = ws.get_Range("H9", "EB9");
            foreach (var rv in rng.Value2)
                trm.GrossRents.Add(ExcelPaster.FormatRangeValueToString(rv));

            rng = ws.get_Range("H10", "EB10");
            foreach (var rv in rng.Value2)
                trm.Advertising.Add(ExcelPaster.FormatRangeValueToString(rv));

            rng = ws.get_Range("H11", "EB11");
            foreach (var rv in rng.Value2)
                trm.AutoAndTravel.Add(ExcelPaster.FormatRangeValueToString(rv));

            rng = ws.get_Range("H12", "EB12");
            foreach (var rv in rng.Value2)
                trm.CleaningAndMaintenance.Add(ExcelPaster.FormatRangeValueToString(rv));

            rng = ws.get_Range("H13", "EB13");
            foreach (var rv in rng.Value2)
                trm.Commissions.Add(ExcelPaster.FormatRangeValueToString(rv));

            rng = ws.get_Range("H14", "EB14");
            foreach (var rv in rng.Value2)
                trm.Insurance.Add(ExcelPaster.FormatRangeValueToString(rv));

            rng = ws.get_Range("H15", "EB15");
            foreach (var rv in rng.Value2)
                trm.LegalAndOtherProfessionalFees.Add(ExcelPaster.FormatRangeValueToString(rv));

            rng = ws.get_Range("H6", "EB16");
            foreach (var rv in rng.Value2)
                trm.Interest.Add(ExcelPaster.FormatRangeValueToString(rv));

            rng = ws.get_Range("H17", "EB17");
            foreach (var rv in rng.Value2)
                trm.Repairs.Add(ExcelPaster.FormatRangeValueToString(rv));

            rng = ws.get_Range("H18", "EB18");
            foreach (var rv in rng.Value2)
                trm.Taxes.Add(ExcelPaster.FormatRangeValueToString(rv));

            rng = ws.get_Range("H19", "EB19");
            foreach (var rv in rng.Value2)
                trm.Utilities.Add(ExcelPaster.FormatRangeValueToString(rv));

            rng = ws.get_Range("H20", "EB20");
            foreach (var rv in rng.Value2)
                trm.WagesAndSalaries.Add(ExcelPaster.FormatRangeValueToString(rv));

            rng = ws.get_Range("H21", "EB21");
            foreach (var rv in rng.Value2)
                trm.Depreciation.Add(ExcelPaster.FormatRangeValueToString(rv));

            rng = ws.get_Range("E23", "EB150");
            int i = 25;
            foreach (Range r in rng.Rows)
            {
                string val = Convert.ToString(r.Cells[i, 1].Value2);
                if (string.IsNullOrEmpty(val) || val.Contains("Total Expenses"))
                    break;
                GenericList gl = new GenericList();
                foreach (var rv in r.Value2)
                    gl.Items.Add(ExcelPaster.FormatRangeValueToString(rv));
                trm.Other.Add(gl);
                i++;
            }

            return trm;
        }

        public static void Set(TaxReturnForm8825Model trm)
        {
            Worksheet ws = Globals.ThisAddIn.Application.Worksheets["A3) Tax Return - Form 8825"];
            ws.Activate();
            Range rng = ws.get_Range("H8", "EB8");
            ExcelPaster.PasteHorizontalSingleDimension(trm.PhysicalAddress, rng);
           
            rng = ws.get_Range("H9", "EB9");
            ExcelPaster.PasteHorizontalSingleDimension(trm.GrossRents, rng);
            
            rng = ws.get_Range("H10", "EB10");
            ExcelPaster.PasteHorizontalSingleDimension(trm.Advertising, rng);
            
            rng = ws.get_Range("H11", "EB11");
            ExcelPaster.PasteHorizontalSingleDimension(trm.AutoAndTravel, rng);
           
            rng = ws.get_Range("H12", "EB12");
            ExcelPaster.PasteHorizontalSingleDimension(trm.CleaningAndMaintenance, rng);
          
            rng = ws.get_Range("H13", "EB13");
            ExcelPaster.PasteHorizontalSingleDimension(trm.Commissions, rng);
           
            rng = ws.get_Range("H14", "EB14");
            ExcelPaster.PasteHorizontalSingleDimension(trm.Insurance, rng);
         
            rng = ws.get_Range("H15", "EB15");
            ExcelPaster.PasteHorizontalSingleDimension(trm.LegalAndOtherProfessionalFees, rng);
          
            rng = ws.get_Range("H6", "EB16");
            ExcelPaster.PasteHorizontalSingleDimension(trm.Interest, rng);
            
            rng = ws.get_Range("H17", "EB17");
            ExcelPaster.PasteHorizontalSingleDimension(trm.Repairs, rng);
         
            rng = ws.get_Range("H18", "EB18");
            ExcelPaster.PasteHorizontalSingleDimension(trm.Taxes, rng);
           
            rng = ws.get_Range("H19", "EB19");
            ExcelPaster.PasteHorizontalSingleDimension(trm.Utilities, rng);
           
            rng = ws.get_Range("H20", "EB20");
            ExcelPaster.PasteHorizontalSingleDimension(trm.WagesAndSalaries, rng);
           
            rng = ws.get_Range("H21", "EB21");
            ExcelPaster.PasteHorizontalSingleDimension(trm.Depreciation, rng);
            
            Worksheet wsDownloadBEL = Globals.ThisAddIn.GetDownloadSheet();
            int rc = trm.Other.Count;
            Range iRange = wsDownloadBEL.Range["B500:H" + (300 + (rc - 1))]; //the -1 accounts  for an extra index row that needs to be trimmed
            iRange.Value = ExcelPaster.GetRangeValuesToInsert(trm.Other);
            wsDownloadBEL.Activate();
            iRange.Activate();
            iRange.Cut();
            ws.Activate();
            Range sectionRange = ws.get_Range("E8", "EB8");
            sectionRange.Activate();
            sectionRange.Insert(XlInsertShiftDirection.xlShiftDown, Type.Missing);
        }
    }
}
