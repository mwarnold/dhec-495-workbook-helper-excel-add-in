﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DheccExcelAddIn2013.Models.BELAVMConstruction.TaxReturn.v3_0
{
    public class TaxReturnA3Model
    {
        public string TaxReturnFormUsed { get; set; }
        public string MonthOfTaxYearEnd { get; set; }
        public string MethodOFAcocuntingPerTaxReturn { get; set; }
        public List<string> GrossReceiptsOrSalesPerTaxReturn { get; set; }
        public List<string> NetIncomeLossPerTaxReturn { get; set; }
        public bool TaxReconsiliationTab { get; set; }
        public bool ShowScheduleM3 { get; set; }

        public GenericList TaxFormData { get; set; }

        public TaxReturnA3Model()
        {
            GrossReceiptsOrSalesPerTaxReturn = new List<string>();
            NetIncomeLossPerTaxReturn = new List<string>();
            TaxFormData = new GenericList();
        }
    }
}
