﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DheccExcelAddIn2013.Interfaces;
using DheccExcelAddIn2013.Helpers;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Net;
using DheccExcelAddIn2013.Models;
using System.IO;
using System.Net.Sockets;

namespace DheccExcelAddIn2013.UserControls
{
    public partial class ucLogin : UserControl
    {
        private IDheccContract iContract;

        public ucLogin()
        {
            this.iContract = new DheccContract();
            InitializeComponent();
            lblLoginError.Text = "";
        }

        public void RefreshControl()
        {
            if (string.IsNullOrEmpty(iContract.AppSetting().WebServiceURL))
                Globals.ThisAddIn.SetCustomTaskPane("My Account");
            lblLoginError.Text = "";
            tbPassword.Text = "";
            tbUserName.Focus();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            string userName = "";
            List<WorkbookModel> workbooks = new List<WorkbookModel>();
            bool loginError = false;

            var client = AppVariables.BGWebService();
            com.browngreer.www3.clsVerfication resp;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                loginError = false;
                userName = tbUserName.Text;
                IPHostEntry SystemAC = Dns.GetHostEntry(Dns.GetHostName());
                string ipAddress = SystemAC.AddressList.FirstOrDefault(x => x.AddressFamily == AddressFamily.InterNetwork).ToString();
                string computerName = System.Environment.MachineName;
                string encPw = BGCrypto.BGCryptography.Encrypt(tbPassword.Text);
                AppVariables.SetEncryptedPassword(encPw);
                resp = client.VerifyUser(
                    userName, encPw, ipAddress, computerName);
                if (!WebServiceResponseHelper.ValidateResponse(resp.Result.ResultID, resp.Result.ResultMessage))
                {
                    lblLoginError.Text = resp.Result.ResultMessage;
                    return;
                }
                if (resp.Result.ResultMessage.Contains("Invalid"))
                {
                    lblLoginError.Text = resp.Result.ResultMessage;
                    return;
                }
                iContract.SetCurrentAvailableVersion(resp.AddIn.Version);
            }
            catch (Exception ex)
            {
                lblLoginError.Text = "Web Server Error";
                EmailHelper.SendExceptionEmail(ex.ToString());
                return;
            }
            try
            {
                if (resp.UserID == 0)
                    loginError = true;
                if (loginError)
                {
                    lblLoginError.Text = "Login failed. Check user name & password.";
                    return;
                }
                AppVariables.SetIsLoggedIn(true);
                AppVariables.SetCurrentUserId(resp.UserID);
                AppVariables.SetAuthToken(resp.AuthToken);
                WorkbookTemplate.UpdateActiveWorkbookList(resp.WorkbookInfoList);

                //if this is the user's first time using the add in there will be no add in object. 
                //check to see if the user has the most recent version of the add in. If not prompt to download
                //and run update routine.
                string userAddInVersion = iContract.AppSetting().UserAddIn.Version;
                AppSetting _sett = iContract.AppSetting();
                if (userAddInVersion == "0.0" || userAddInVersion != resp.AddIn.Version)
                {                   
                    _sett.UserAddIn = new AddInModel()
                        {
                            Version = resp.AddIn.Version,
                            Id = resp.AddIn.AddInID,
                            Description = resp.AddIn.Description,
                            Active = resp.AddIn.Active
                        };
                    
                    //this is wehre we would run the addin update routine converting the 
                }
                //set roles
                _sett.User.Roles = new List<RoleModel>();
                foreach (var role in resp.UserAccessRolesList)
                {
                   _sett.User.Roles.Add(
                        new RoleModel()
                        {
                             Description = role.UserAccessTypeDesc,
                             Id = role.UserAccessTypeID
                        });
                }
                _sett.User.UserName = userName;
                new AppSettingHelper().SaveSettings(_sett);
                iContract.SetAppSetting(_sett);

                //make sure that the add in has the most recent list of measures
                foreach (var m in resp.AcctMeasureInfoList)
                {
                    if (iContract.AppSetting().Measures.SingleOrDefault(x => x.Id == m.MeasureID) == null)
                        iContract.AppSetting().Measures.Add(
                            new MeasureModel()
                            {
                                Id = m.MeasureID,
                                ActivatedDate = m.DateActivated,
                                DeActivatedDate = m.DateDeActivated == null ? DateTime.MinValue : m.DateDeActivated,
                                Name = m.MeasureName,
                                OrderByIndex = m.OrderBy
                            });
                }
                //if the user has been working off line and has pending measures to upload this is where we get them in sync
                WorkbookTemplate.CheckForPendingMeasureUpload();
                iContract.AppSettingHelper().SaveSettings(iContract.AppSetting());
                iContract.SetAppSetting(new AppSettingHelper().GetSettings());
                iContract.ShowHideButtons();
                Globals.ThisAddIn.SetCustomTaskPane("My Portal Queue");
            }
            catch (Exception ex)
            {
                WebServiceResponseHelper.ValidateResponse(0, "An unhandled exception has occured. Our dev team has been notifiied. We apologize for the inconvenience");
                EmailHelper.SendExceptionEmail(ex.ToString());
            }
            Cursor.Current = Cursors.Arrow;
        }

        private void tbPassword_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                btnLogin.PerformClick();
            }
        }

        private void btnLogin_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                btnLogin.PerformClick();
            }
        }
    }
}