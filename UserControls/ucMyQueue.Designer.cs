﻿namespace DheccExcelAddIn2013.UserControls
{
    partial class ucMyQueue
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlSearch = new System.Windows.Forms.Panel();
            this.aReset = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.tbSearch = new System.Windows.Forms.TextBox();
            this.lblSearchText1 = new System.Windows.Forms.Label();
            this.pnlCompMethod = new System.Windows.Forms.Panel();
            this.gbCompMethod = new System.Windows.Forms.GroupBox();
            this.btnLoadClaim = new System.Windows.Forms.Button();
            this.ddlCompensationMethod = new System.Windows.Forms.ComboBox();
            this.lblCompMethodTxt = new System.Windows.Forms.Label();
            this.pnlGrid = new System.Windows.Forms.Panel();
            this.tcQueueMain = new System.Windows.Forms.TabControl();
            this.tpNewItems = new System.Windows.Forms.TabPage();
            this.tcQueue = new System.Windows.Forms.TabControl();
            this.tpInitRev = new System.Windows.Forms.TabPage();
            this.dgInitialReview = new System.Windows.Forms.DataGridView();
            this.tpQAReview = new System.Windows.Forms.TabPage();
            this.dgQAReview = new System.Windows.Forms.DataGridView();
            this.tpConsistancy = new System.Windows.Forms.TabPage();
            this.dgConsistancy = new System.Windows.Forms.DataGridView();
            this.tpWip = new System.Windows.Forms.TabPage();
            this.tcWIP = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dgWIP_InitialReview = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dgWIP_QA = new System.Windows.Forms.DataGridView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.dgWIP_Consistancy = new System.Windows.Forms.DataGridView();
            this.pnlSearch.SuspendLayout();
            this.pnlCompMethod.SuspendLayout();
            this.gbCompMethod.SuspendLayout();
            this.pnlGrid.SuspendLayout();
            this.tcQueueMain.SuspendLayout();
            this.tpNewItems.SuspendLayout();
            this.tcQueue.SuspendLayout();
            this.tpInitRev.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgInitialReview)).BeginInit();
            this.tpQAReview.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgQAReview)).BeginInit();
            this.tpConsistancy.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgConsistancy)).BeginInit();
            this.tpWip.SuspendLayout();
            this.tcWIP.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgWIP_InitialReview)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgWIP_QA)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgWIP_Consistancy)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlSearch
            // 
            this.pnlSearch.Controls.Add(this.aReset);
            this.pnlSearch.Controls.Add(this.btnSearch);
            this.pnlSearch.Controls.Add(this.tbSearch);
            this.pnlSearch.Controls.Add(this.lblSearchText1);
            this.pnlSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlSearch.Location = new System.Drawing.Point(0, 0);
            this.pnlSearch.Margin = new System.Windows.Forms.Padding(2);
            this.pnlSearch.Name = "pnlSearch";
            this.pnlSearch.Size = new System.Drawing.Size(450, 49);
            this.pnlSearch.TabIndex = 37;
            // 
            // aReset
            // 
            this.aReset.AutoSize = true;
            this.aReset.Location = new System.Drawing.Point(375, 18);
            this.aReset.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.aReset.Name = "aReset";
            this.aReset.Size = new System.Drawing.Size(45, 17);
            this.aReset.TabIndex = 39;
            this.aReset.Text = "Reset";
            this.aReset.Click += new System.EventHandler(this.aReset_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(312, 15);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(2);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(46, 23);
            this.btnSearch.TabIndex = 38;
            this.btnSearch.Text = "Go";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // tbSearch
            // 
            this.tbSearch.Location = new System.Drawing.Point(94, 16);
            this.tbSearch.Margin = new System.Windows.Forms.Padding(2);
            this.tbSearch.Name = "tbSearch";
            this.tbSearch.Size = new System.Drawing.Size(197, 22);
            this.tbSearch.TabIndex = 37;
            // 
            // lblSearchText1
            // 
            this.lblSearchText1.AutoSize = true;
            this.lblSearchText1.Location = new System.Drawing.Point(30, 18);
            this.lblSearchText1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSearchText1.Name = "lblSearchText1";
            this.lblSearchText1.Size = new System.Drawing.Size(53, 17);
            this.lblSearchText1.TabIndex = 36;
            this.lblSearchText1.Text = "Search";
            // 
            // pnlCompMethod
            // 
            this.pnlCompMethod.Controls.Add(this.gbCompMethod);
            this.pnlCompMethod.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlCompMethod.Location = new System.Drawing.Point(0, 49);
            this.pnlCompMethod.Margin = new System.Windows.Forms.Padding(2);
            this.pnlCompMethod.Name = "pnlCompMethod";
            this.pnlCompMethod.Size = new System.Drawing.Size(450, 137);
            this.pnlCompMethod.TabIndex = 38;
            // 
            // gbCompMethod
            // 
            this.gbCompMethod.Controls.Add(this.btnLoadClaim);
            this.gbCompMethod.Controls.Add(this.ddlCompensationMethod);
            this.gbCompMethod.Controls.Add(this.lblCompMethodTxt);
            this.gbCompMethod.Location = new System.Drawing.Point(23, 0);
            this.gbCompMethod.Margin = new System.Windows.Forms.Padding(2);
            this.gbCompMethod.Name = "gbCompMethod";
            this.gbCompMethod.Padding = new System.Windows.Forms.Padding(2);
            this.gbCompMethod.Size = new System.Drawing.Size(404, 127);
            this.gbCompMethod.TabIndex = 37;
            this.gbCompMethod.TabStop = false;
            this.gbCompMethod.Text = "Compensation Method";
            // 
            // btnLoadClaim
            // 
            this.btnLoadClaim.Location = new System.Drawing.Point(288, 88);
            this.btnLoadClaim.Margin = new System.Windows.Forms.Padding(2);
            this.btnLoadClaim.Name = "btnLoadClaim";
            this.btnLoadClaim.Size = new System.Drawing.Size(103, 27);
            this.btnLoadClaim.TabIndex = 2;
            this.btnLoadClaim.Text = "Load Claim";
            this.btnLoadClaim.UseVisualStyleBackColor = true;
            this.btnLoadClaim.Click += new System.EventHandler(this.btnLoadClaim_Click);
            // 
            // ddlCompensationMethod
            // 
            this.ddlCompensationMethod.FormattingEnabled = true;
            this.ddlCompensationMethod.Location = new System.Drawing.Point(10, 55);
            this.ddlCompensationMethod.Margin = new System.Windows.Forms.Padding(2);
            this.ddlCompensationMethod.Name = "ddlCompensationMethod";
            this.ddlCompensationMethod.Size = new System.Drawing.Size(383, 24);
            this.ddlCompensationMethod.TabIndex = 1;
            // 
            // lblCompMethodTxt
            // 
            this.lblCompMethodTxt.AutoSize = true;
            this.lblCompMethodTxt.Location = new System.Drawing.Point(16, 30);
            this.lblCompMethodTxt.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCompMethodTxt.Name = "lblCompMethodTxt";
            this.lblCompMethodTxt.Size = new System.Drawing.Size(182, 17);
            this.lblCompMethodTxt.TabIndex = 0;
            this.lblCompMethodTxt.Text = "Select a compensation type";
            // 
            // pnlGrid
            // 
            this.pnlGrid.Controls.Add(this.tcQueueMain);
            this.pnlGrid.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlGrid.Location = new System.Drawing.Point(0, 186);
            this.pnlGrid.Margin = new System.Windows.Forms.Padding(2);
            this.pnlGrid.Name = "pnlGrid";
            this.pnlGrid.Size = new System.Drawing.Size(450, 419);
            this.pnlGrid.TabIndex = 39;
            // 
            // tcQueueMain
            // 
            this.tcQueueMain.Controls.Add(this.tpNewItems);
            this.tcQueueMain.Controls.Add(this.tpWip);
            this.tcQueueMain.Location = new System.Drawing.Point(5, 8);
            this.tcQueueMain.Margin = new System.Windows.Forms.Padding(2);
            this.tcQueueMain.Name = "tcQueueMain";
            this.tcQueueMain.SelectedIndex = 0;
            this.tcQueueMain.Size = new System.Drawing.Size(440, 401);
            this.tcQueueMain.TabIndex = 0;
            // 
            // tpNewItems
            // 
            this.tpNewItems.Controls.Add(this.tcQueue);
            this.tpNewItems.Location = new System.Drawing.Point(4, 25);
            this.tpNewItems.Margin = new System.Windows.Forms.Padding(2);
            this.tpNewItems.Name = "tpNewItems";
            this.tpNewItems.Padding = new System.Windows.Forms.Padding(2);
            this.tpNewItems.Size = new System.Drawing.Size(432, 372);
            this.tpNewItems.TabIndex = 0;
            this.tpNewItems.Text = "New/Unworked";
            this.tpNewItems.UseVisualStyleBackColor = true;
            // 
            // tcQueue
            // 
            this.tcQueue.Controls.Add(this.tpInitRev);
            this.tcQueue.Controls.Add(this.tpQAReview);
            this.tcQueue.Controls.Add(this.tpConsistancy);
            this.tcQueue.Location = new System.Drawing.Point(14, 11);
            this.tcQueue.Margin = new System.Windows.Forms.Padding(2);
            this.tcQueue.Name = "tcQueue";
            this.tcQueue.SelectedIndex = 0;
            this.tcQueue.Size = new System.Drawing.Size(404, 352);
            this.tcQueue.TabIndex = 1;
            // 
            // tpInitRev
            // 
            this.tpInitRev.Controls.Add(this.dgInitialReview);
            this.tpInitRev.Location = new System.Drawing.Point(4, 25);
            this.tpInitRev.Margin = new System.Windows.Forms.Padding(2);
            this.tpInitRev.Name = "tpInitRev";
            this.tpInitRev.Padding = new System.Windows.Forms.Padding(2);
            this.tpInitRev.Size = new System.Drawing.Size(396, 323);
            this.tpInitRev.TabIndex = 0;
            this.tpInitRev.Text = "Initial Review";
            this.tpInitRev.UseVisualStyleBackColor = true;
            // 
            // dgInitialReview
            // 
            this.dgInitialReview.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgInitialReview.Location = new System.Drawing.Point(3, 3);
            this.dgInitialReview.Margin = new System.Windows.Forms.Padding(2);
            this.dgInitialReview.Name = "dgInitialReview";
            this.dgInitialReview.ReadOnly = true;
            this.dgInitialReview.RowTemplate.Height = 40;
            this.dgInitialReview.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgInitialReview.Size = new System.Drawing.Size(394, 323);
            this.dgInitialReview.TabIndex = 3;
            this.dgInitialReview.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gvInitialReview_CellClick);
            // 
            // tpQAReview
            // 
            this.tpQAReview.Controls.Add(this.dgQAReview);
            this.tpQAReview.Location = new System.Drawing.Point(4, 25);
            this.tpQAReview.Margin = new System.Windows.Forms.Padding(2);
            this.tpQAReview.Name = "tpQAReview";
            this.tpQAReview.Padding = new System.Windows.Forms.Padding(2);
            this.tpQAReview.Size = new System.Drawing.Size(396, 323);
            this.tpQAReview.TabIndex = 1;
            this.tpQAReview.Text = "Q/A Review";
            this.tpQAReview.UseVisualStyleBackColor = true;
            // 
            // dgQAReview
            // 
            this.dgQAReview.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgQAReview.Location = new System.Drawing.Point(3, 3);
            this.dgQAReview.Margin = new System.Windows.Forms.Padding(2);
            this.dgQAReview.Name = "dgQAReview";
            this.dgQAReview.ReadOnly = true;
            this.dgQAReview.RowTemplate.Height = 40;
            this.dgQAReview.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgQAReview.Size = new System.Drawing.Size(394, 323);
            this.dgQAReview.TabIndex = 4;
            this.dgQAReview.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgQAReview_CellClick);
            // 
            // tpConsistancy
            // 
            this.tpConsistancy.Controls.Add(this.dgConsistancy);
            this.tpConsistancy.Location = new System.Drawing.Point(4, 25);
            this.tpConsistancy.Margin = new System.Windows.Forms.Padding(2);
            this.tpConsistancy.Name = "tpConsistancy";
            this.tpConsistancy.Padding = new System.Windows.Forms.Padding(2);
            this.tpConsistancy.Size = new System.Drawing.Size(396, 323);
            this.tpConsistancy.TabIndex = 2;
            this.tpConsistancy.Text = "Consistancy Review";
            this.tpConsistancy.UseVisualStyleBackColor = true;
            // 
            // dgConsistancy
            // 
            this.dgConsistancy.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgConsistancy.Location = new System.Drawing.Point(3, 3);
            this.dgConsistancy.Margin = new System.Windows.Forms.Padding(2);
            this.dgConsistancy.Name = "dgConsistancy";
            this.dgConsistancy.ReadOnly = true;
            this.dgConsistancy.RowTemplate.Height = 40;
            this.dgConsistancy.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgConsistancy.Size = new System.Drawing.Size(394, 323);
            this.dgConsistancy.TabIndex = 4;
            this.dgConsistancy.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgConsistancy_CellClick);
            // 
            // tpWip
            // 
            this.tpWip.Controls.Add(this.tcWIP);
            this.tpWip.Location = new System.Drawing.Point(4, 25);
            this.tpWip.Margin = new System.Windows.Forms.Padding(2);
            this.tpWip.Name = "tpWip";
            this.tpWip.Padding = new System.Windows.Forms.Padding(2);
            this.tpWip.Size = new System.Drawing.Size(432, 372);
            this.tpWip.TabIndex = 1;
            this.tpWip.Text = "Work In Progress";
            this.tpWip.UseVisualStyleBackColor = true;
            // 
            // tcWIP
            // 
            this.tcWIP.Controls.Add(this.tabPage1);
            this.tcWIP.Controls.Add(this.tabPage2);
            this.tcWIP.Controls.Add(this.tabPage3);
            this.tcWIP.Location = new System.Drawing.Point(14, 10);
            this.tcWIP.Margin = new System.Windows.Forms.Padding(2);
            this.tcWIP.Name = "tcWIP";
            this.tcWIP.SelectedIndex = 0;
            this.tcWIP.Size = new System.Drawing.Size(404, 352);
            this.tcWIP.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dgWIP_InitialReview);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage1.Size = new System.Drawing.Size(396, 323);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Initial Review";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dgWIP_InitialReview
            // 
            this.dgWIP_InitialReview.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgWIP_InitialReview.Location = new System.Drawing.Point(3, 3);
            this.dgWIP_InitialReview.Margin = new System.Windows.Forms.Padding(2);
            this.dgWIP_InitialReview.Name = "dgWIP_InitialReview";
            this.dgWIP_InitialReview.ReadOnly = true;
            this.dgWIP_InitialReview.RowTemplate.Height = 40;
            this.dgWIP_InitialReview.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgWIP_InitialReview.Size = new System.Drawing.Size(394, 323);
            this.dgWIP_InitialReview.TabIndex = 3;
            this.dgWIP_InitialReview.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgWIP_InitialReview_CellContentDoubleClick);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dgWIP_QA);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage2.Size = new System.Drawing.Size(396, 323);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Q/A Review";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dgWIP_QA
            // 
            this.dgWIP_QA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgWIP_QA.Location = new System.Drawing.Point(3, 3);
            this.dgWIP_QA.Margin = new System.Windows.Forms.Padding(2);
            this.dgWIP_QA.Name = "dgWIP_QA";
            this.dgWIP_QA.ReadOnly = true;
            this.dgWIP_QA.RowTemplate.Height = 40;
            this.dgWIP_QA.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgWIP_QA.Size = new System.Drawing.Size(394, 323);
            this.dgWIP_QA.TabIndex = 4;
            this.dgWIP_QA.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgWIP_QA_CellContentDoubleClick);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.dgWIP_Consistancy);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage3.Size = new System.Drawing.Size(396, 323);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Consistancy Review";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // dgWIP_Consistancy
            // 
            this.dgWIP_Consistancy.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgWIP_Consistancy.Location = new System.Drawing.Point(3, 3);
            this.dgWIP_Consistancy.Margin = new System.Windows.Forms.Padding(2);
            this.dgWIP_Consistancy.Name = "dgWIP_Consistancy";
            this.dgWIP_Consistancy.ReadOnly = true;
            this.dgWIP_Consistancy.RowTemplate.Height = 40;
            this.dgWIP_Consistancy.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgWIP_Consistancy.Size = new System.Drawing.Size(394, 323);
            this.dgWIP_Consistancy.TabIndex = 4;
            this.dgWIP_Consistancy.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgWIP_Consistancy_CellContentDoubleClick);
            // 
            // ucMyQueue
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Controls.Add(this.pnlGrid);
            this.Controls.Add(this.pnlCompMethod);
            this.Controls.Add(this.pnlSearch);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "ucMyQueue";
            this.Size = new System.Drawing.Size(450, 606);
            this.pnlSearch.ResumeLayout(false);
            this.pnlSearch.PerformLayout();
            this.pnlCompMethod.ResumeLayout(false);
            this.gbCompMethod.ResumeLayout(false);
            this.gbCompMethod.PerformLayout();
            this.pnlGrid.ResumeLayout(false);
            this.tcQueueMain.ResumeLayout(false);
            this.tpNewItems.ResumeLayout(false);
            this.tcQueue.ResumeLayout(false);
            this.tpInitRev.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgInitialReview)).EndInit();
            this.tpQAReview.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgQAReview)).EndInit();
            this.tpConsistancy.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgConsistancy)).EndInit();
            this.tpWip.ResumeLayout(false);
            this.tcWIP.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgWIP_InitialReview)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgWIP_QA)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgWIP_Consistancy)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlSearch;
        private System.Windows.Forms.Label aReset;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TextBox tbSearch;
        private System.Windows.Forms.Label lblSearchText1;
        private System.Windows.Forms.Panel pnlCompMethod;
        private System.Windows.Forms.GroupBox gbCompMethod;
        private System.Windows.Forms.Button btnLoadClaim;
        private System.Windows.Forms.ComboBox ddlCompensationMethod;
        private System.Windows.Forms.Label lblCompMethodTxt;
        private System.Windows.Forms.Panel pnlGrid;
        private System.Windows.Forms.TabControl tcQueueMain;
        private System.Windows.Forms.TabPage tpNewItems;
        private System.Windows.Forms.TabPage tpWip;
        private System.Windows.Forms.TabControl tcQueue;
        private System.Windows.Forms.TabPage tpInitRev;
        private System.Windows.Forms.DataGridView dgInitialReview;
        private System.Windows.Forms.TabPage tpQAReview;
        private System.Windows.Forms.DataGridView dgQAReview;
        private System.Windows.Forms.TabPage tpConsistancy;
        private System.Windows.Forms.DataGridView dgConsistancy;
        private System.Windows.Forms.TabControl tcWIP;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView dgWIP_InitialReview;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dgWIP_QA;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridView dgWIP_Consistancy;

    }
}
