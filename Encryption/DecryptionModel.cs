﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DheccExcelAddIn2013.Encryption
{
    public class DecryptionModel
    {
        public byte[] DecryptedBytes { get; set; }
        public String Md5Value { get; set; }
    }
}
