﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DheccExcelAddIn2013.Models
{
    public class AcctStepModel
    {
        public int MeasureId { get; set; }
        public int UserId { get; set; }
        public string StepName { get; set; }
        public string UserName { get; set; }        
        public DateTime Started { get; set; }
        public DateTime Stopped { get; set; }
        public bool PendingUpload { get; set; }
        public bool Checked { get; set; }
    }
}
