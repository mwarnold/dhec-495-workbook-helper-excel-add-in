﻿namespace DheccExcelAddIn2013.Forms
{
    partial class frmAdmin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTxt = new System.Windows.Forms.Label();
            this.btnValidatePw = new System.Windows.Forms.Button();
            this.tbDevPassword = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lblTxt
            // 
            this.lblTxt.AutoSize = true;
            this.lblTxt.Location = new System.Drawing.Point(133, 46);
            this.lblTxt.Name = "lblTxt";
            this.lblTxt.Size = new System.Drawing.Size(506, 32);
            this.lblTxt.TabIndex = 1;
            this.lblTxt.Text = "Please enter your developer password.";
            // 
            // btnValidatePw
            // 
            this.btnValidatePw.Location = new System.Drawing.Point(562, 149);
            this.btnValidatePw.Name = "btnValidatePw";
            this.btnValidatePw.Size = new System.Drawing.Size(129, 60);
            this.btnValidatePw.TabIndex = 2;
            this.btnValidatePw.Text = "Submit";
            this.btnValidatePw.UseVisualStyleBackColor = true;
            this.btnValidatePw.Click += new System.EventHandler(this.btnValidatePw_Click);
            // 
            // tbDevPassword
            // 
            this.tbDevPassword.Location = new System.Drawing.Point(112, 161);
            this.tbDevPassword.Name = "tbDevPassword";
            this.tbDevPassword.Size = new System.Drawing.Size(404, 38);
            this.tbDevPassword.TabIndex = 3;
            // 
            // frmAdmin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(768, 276);
            this.Controls.Add(this.tbDevPassword);
            this.Controls.Add(this.btnValidatePw);
            this.Controls.Add(this.lblTxt);
            this.Name = "frmAdmin";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Developer Login";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTxt;
        private System.Windows.Forms.Button btnValidatePw;
        private System.Windows.Forms.TextBox tbDevPassword;
    }
}