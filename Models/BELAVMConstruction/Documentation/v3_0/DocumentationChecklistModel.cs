﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DheccExcelAddIn2013.Models.BELAVMConstruction.Documentation.v3_0
{
    public class DocumentationChecklistModel
    {
        public GenericList ClaimsPreparation { get; set; }
        public GenericList Contracts { get; set; }
        public GenericList FinancialStatements2006 { get; set; }
        public GenericList FinancialStatements2007 { get; set; }
        public GenericList FinancialStatements2008 { get; set; }
        public GenericList FinancialStatements2009 { get; set; }
        public GenericList FinancialStatements2010 { get; set; }
        public GenericList FinancialStatements2011 { get; set; }
        public GenericList FinancialStatements2012 { get; set; }
        public GenericList FinancialStatementsOther { get; set; }
        public GenericList RentalProperty { get; set; }
        public GenericList SupplementalFinancialDocuments { get; set; }
        public GenericList TaxReturn2006 { get; set; }
        public GenericList TaxReturn2007 { get; set; }
        public GenericList TaxReturn2008 { get; set; }
        public GenericList TaxReturn2009 { get; set; }
        public GenericList TaxReturn2010 { get; set; }
        public GenericList TaxReturn2011 { get; set; }
        public GenericList TaxReturn2012 { get; set; }
        public GenericList TaxReturnOther { get; set; }
    }
}
