﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Excel = Microsoft.Office.Interop.Excel;
using Office = Microsoft.Office.Core;
using Microsoft.Office.Tools.Excel;
using System.ComponentModel;
using DheccExcelAddIn2013.Interfaces;
using DheccExcelAddIn2013.Helpers;
using DheccExcelAddIn2013.UserControls;
using DheccExcelAddIn2013.Models;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Net;
using System.IO;
using System.Security.Cryptography;
using Microsoft.Office.Tools;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;
using VBIDE = Microsoft.Vbe.Interop;
using DheccExcelAddIn2013.Forms;
using System.Reflection;
using Microsoft.Office.Interop.Word;

namespace DheccExcelAddIn2013
{
    public partial class ThisAddIn
    {
        private IDheccContract iContract;
        private static CustomTaskPane _customTaskPane;
        private static CustomTaskPane _environmentIndicatorTaskPane;
        private static string _currentUserControl;
        private ucComments _ucComments = new ucComments();
        private ucLogin _ucLogin = new ucLogin();
        private ucMyQueue _ucMyQueue = new ucMyQueue();
        private ucMyAccount _ucMyAccount = new ucMyAccount();
        private ucReviewerHistory _ucReviewerHistory = new ucReviewerHistory();
        private ucSaveRemote _ucSaveRemote = new ucSaveRemote();
        private ucLocalCopies _ucLocalCopies = new ucLocalCopies();
        private ucRemoteCopies _ucRemoteCopies = new ucRemoteCopies();
        private ucUploadClaim _ucUploadClaim = new ucUploadClaim();
        private ucMeasures _ucAcctMeasures = new ucMeasures();
        private ucCompensationMethod _ucCompensationMethod = new ucCompensationMethod();
        private ucEnvironmentIndicator _ucEnviromentIndicator = new ucEnvironmentIndicator();
        private ucImportWorkbook _ucImportWorkbook = new ucImportWorkbook();
        private ucInitialReviewQueue _ucInitialReviewQueue = new ucInitialReviewQueue();
        private static Office.IRibbonUI ribbon;


        private static bool isWorkbookClosing = false;
        public Office.IRibbonUI RibbonMain;

        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            this.iContract = new DheccContract();
            this.iContract.SetAppSettingsHelper(new AppSettingHelper());
            this.iContract.SetAppSetting(iContract.AppSettingHelper().GetSettings());

            //Init User Controls
            _ucComments = new ucComments();
            _ucLogin = new ucLogin();
            _ucMyQueue = new ucMyQueue();
            _ucMyAccount = new ucMyAccount();
            _ucReviewerHistory = new ucReviewerHistory();
            _ucSaveRemote = new ucSaveRemote();
            _ucLocalCopies = new ucLocalCopies();
            _ucRemoteCopies = new ucRemoteCopies();
            _ucUploadClaim = new ucUploadClaim();
            _ucCompensationMethod = new ucCompensationMethod();
            _ucAcctMeasures = new ucMeasures();
            _ucImportWorkbook = new ucImportWorkbook();
            _ucInitialReviewQueue = new ucInitialReviewQueue();
            AddUserControlsToTaskPaneCollection();

            //Excel events
            //Globals.ThisAddIn.Application.WorkbookAfterSave += new Excel.AppEvents_WorkbookAfterSaveEventHandler(Workbook_After_Save);
            Globals.ThisAddIn.Application.WorkbookOpen += new Excel.AppEvents_WorkbookOpenEventHandler(Workbook_Open);
            Globals.ThisAddIn.Application.WorkbookBeforeClose += new Excel.AppEvents_WorkbookBeforeCloseEventHandler(Workbook_Close);
            Globals.ThisAddIn.Application.WorkbookActivate += new Excel.AppEvents_WorkbookActivateEventHandler(Application_ActiveWorkbookChanges);
            // Globals.ThisAddIn.Application.ShowDevTools = false;
            SetEnvironmentIndicator();

            if (!iContract.AppSetting().ProgrammaticAccessTutorialShown)
            {
                OpenVBAAccessHelpFile();
                iContract.AppSetting().ProgrammaticAccessTutorialShown = true;
                iContract.AppSettingHelper().SaveSettings(iContract.AppSetting());
                MessageBox.Show("The DWH 495 Helper Add In requires programmatic access to the VBA module of Excel. A MS WORD document should have opened with instructions on how to set this up in Excel. If MS WORD did not open please click the 'Settings' menue item, or visit the DWH SharePoint site and download the instructions. The SharePoint site url is https://dheclaims.sharepoint.com/infotech/495 DWH Helper");
            }

            if (string.IsNullOrEmpty(iContract.AppSetting().WebServiceURL))
                SetCustomTaskPane("My Account");
        }

        public void OpenVBAAccessHelpFile()
        {
            if (!Directory.Exists(AppVariables.HelpRootDirectory()))
                Directory.CreateDirectory(AppVariables.HelpRootDirectory());
            string helpPath = AppVariables.HelpDirectory() + "VBAAccess.doc";
            if (!File.Exists(helpPath))
                File.WriteAllBytes(helpPath, DheccExcelAddIn2013.Properties.Resources.VBAAccess);
            System.Diagnostics.Process.Start("\"" + helpPath + "\"");
        }

        // TODO:  Follow these steps to enable the Ribbon (XML) item:

        // 1: Copy the following code block into the ThisAddin, ThisWorkbook, or ThisDocument class.

        protected override Microsoft.Office.Core.IRibbonExtensibility CreateRibbonExtensibilityObject()
        {
            return new Ribbon();
        }

        // 2. Create callback methods in the "Ribbon Callbacks" region of this class to handle user
        //    actions, such as clicking a button. Note: if you have exported this Ribbon from the Ribbon designer,
        //    move your code from the event handlers to the callback methods and modify the code to work with the
        //    Ribbon extensibility (RibbonX) programming model.

        // 3. Assign attributes to the control tags in the Ribbon XML file to identify the appropriate callback methods in your code.  

        // For more information, see the Ribbon XML documentation in the Visual Studio Tools for Office Help.


        private void LoadMacros()
        {
            VBIDE.VBComponent oModule;
            oModule = this.Application.ActiveWorkbook.VBProject.VBComponents.Add(VBIDE.vbext_ComponentType.vbext_ct_StdModule);
            oModule.Name = "test_import_macro";
            string sCode = "sub VBAMacro()\r\n" +
                           "   msgbox \"VBA Macro called\"\r\n" +
                           "end sub";
            oModule.CodeModule.AddFromString(sCode);
            //oModule.CodeModule.AddFromFile(file_name);
        }

        private void RunMacro(object oApp, object[] oRunArgs)
        {
            oApp.GetType().InvokeMember("Run",
                System.Reflection.BindingFlags.Default |
                System.Reflection.BindingFlags.InvokeMethod,
                null, oApp, oRunArgs);
        }

        private void Application_ActiveWorkbookChanges(Excel.Workbook Wb)
        {
            SetActiveWorkbook();
        }

        public void SetActiveWorkbook()
        {
            try
            {
                _ucCheckOnce = true;
                var claimInfo = WorkbookTemplate.GetActiveWorkbookClaimInfo();
                //get and set info about this workbook
                if (claimInfo.ClaimantId > 0)
                {
                    if (!AppVariables.TemplateIsLoading)
                    {
                        ResetUserControls();
                        SetCustomTaskPane(_currentUserControl);
                    }
                    try
                    {
                        WorkbookWrapper ww = new WorkbookWrapper();
                        string fileName = Globals.ThisAddIn.Application.ActiveWorkbook.FullName;
                        if (fileName.Contains("RemoteCopy"))
                            ww = new WIPHelper().GetWIP().SingleOrDefault(x => x.ClaimInfo.ClaimId == claimInfo.ClaimId && x.IsRemoteCopy == true);
                        else if (fileName.Contains("Import"))
                            ww = new WIPHelper().GetWIP().SingleOrDefault(x => x.ClaimInfo.ClaimId == claimInfo.ClaimId && x.IsImportedClaim == true);
                        else
                            ww = new WIPHelper().GetWIP().FirstOrDefault(x => x.ClaimInfo.ClaimId == claimInfo.ClaimId);
                        if (ww == null)
                        {
                            ww = new WorkbookWrapper();
                            ww.ClaimInfo =
                                new ClaimInfoModel()
                                {
                                    ClaimantId = claimInfo.ClaimantId,
                                    ClaimantName = claimInfo.ClaimantName,
                                    ClaimId = claimInfo.ClaimId

                                };
                            new WIPHelper().SaveWIP(ww);
                        }
                        iContract.SetCurrentWorkbookWrapper(ww);
                    }
                    catch { iContract.SetCurrentWorkbookWrapper(new WorkbookWrapper()); }
                }
                iContract.ShowHideButtons();
            }
            catch
            {
                //not a 495 workbook
                iContract.SetCurrentWorkbookWrapper(new WorkbookWrapper() { CanEdit = false });
                ResetUserControls();
            }
            iContract.ShowHideButtons();          
        }

        public void ResetUserControls()
        {
            _ucComments = new ucComments();
            _ucLogin = new ucLogin();
            _ucMyQueue = new ucMyQueue();
            _ucMyAccount = new ucMyAccount();
            _ucReviewerHistory = new ucReviewerHistory();
            _ucSaveRemote = new ucSaveRemote();
            _ucLocalCopies = new ucLocalCopies();
            _ucRemoteCopies = new ucRemoteCopies();
            _ucUploadClaim = new ucUploadClaim();
            _ucCompensationMethod = new ucCompensationMethod();
            _ucAcctMeasures = new ucMeasures();
            _ucImportWorkbook = new ucImportWorkbook();
            _ucInitialReviewQueue = new ucInitialReviewQueue();
            AddUserControlsToTaskPaneCollection();
            // if (AppVariables.IsLoggedIn)
            //SetCustomTaskPane(AppVariables.CurrentUserControlTitle);
        }

        private void Workbook_Close(Excel.Workbook Wb, ref bool Cancel)
        {
            //Init User Controls
            isWorkbookClosing = true;
        }

        private void Workbook_Open(Excel.Workbook Wb)
        {
            if (AppVariables.TemplateIsLoading)
            {
                ResetUserControls();
            }
        }

        public void AddUserControlsToTaskPaneCollection()
        {
            if (this.CustomTaskPanes.Count() != 0)
            {
                int i = 1;
                while (i != 0)
                {
                    i = this.CustomTaskPanes.Count();
                    this.CustomTaskPanes.RemoveAt(i - 1);
                    i--;
                }
            }
            this.CustomTaskPanes.Add(_ucLogin, "Login");
            this.CustomTaskPanes.Add(_ucComments, "Comments");
            this.CustomTaskPanes.Add(_ucMyQueue, "My Portal Queue");
            this.CustomTaskPanes.Add(_ucMyAccount, "My Account");
            this.CustomTaskPanes.Add(_ucReviewerHistory, "Reviewer History");
            this.CustomTaskPanes.Add(_ucSaveRemote, "Check-In Claim");
            this.CustomTaskPanes.Add(_ucLocalCopies, "My Local Copies");
            this.CustomTaskPanes.Add(_ucRemoteCopies, "Open Existing Claim");
            this.CustomTaskPanes.Add(_ucUploadClaim, "Upload Claim");
            this.CustomTaskPanes.Add(_ucCompensationMethod, "Compensation Method");
            this.CustomTaskPanes.Add(_ucAcctMeasures, "Measures");
            this.CustomTaskPanes.Add(_ucImportWorkbook, "Import Workbook");
            this.CustomTaskPanes.Add(_ucEnviromentIndicator, "EnvironmentIndicator");
            this.CustomTaskPanes.Add(_ucInitialReviewQueue, "Initial Review Queue");
        }

        private static bool _ucCheckOnce = false;
        public void SetCustomTaskPane(string title)
        {
            Cursor.Current = Cursors.WaitCursor;
            _currentUserControl = title;
            try
            {
                try
                {
                    var prevUserControl = this.CustomTaskPanes.FirstOrDefault(x => x.Title == _currentUserControl).Control;
                    prevUserControl.Visible = false;
                    _customTaskPane.Visible = false;
                }
                catch { } //there isn't one.
                if (title == "Reset")
                {
                    _customTaskPane = this.CustomTaskPanes.FirstOrDefault(x => x.Title == AppVariables.CurrentUserControlTitle);
                    _customTaskPane.Visible = false;
                }
                title = title.Replace("Reset", "Login");
                AppVariables.SetCurrentUserControlTitle(title);
                _customTaskPane = this.CustomTaskPanes.FirstOrDefault(x => x.Title == title);
                switch (title)
                {
                    case "Login":
                        {
                            _ucLogin = new ucLogin();
                            _ucLogin.RefreshControl();
                            break;
                        }
                    case "My Portal Queue":
                        {
                            _ucMyQueue.RefreshControl();
                            break;
                        }
                    case "Comments":
                        {
                            _ucComments.RefreshControl();
                            break;
                        }
                    case "My Account":
                        {
                            _ucMyAccount.RefreshControl();
                            break;
                        }
                    case "Reviewer History":
                        {
                            _ucReviewerHistory.RefreshControl();
                            break;
                        }
                    case "Check-In Claim":
                        {
                            _ucSaveRemote.RefreshControl();
                            break;
                        }
                    case "My Local Copies":
                        {
                            _ucLocalCopies.RefreshControl();
                            break;
                        }
                    case "Open Existing Claim":
                        {
                            _ucRemoteCopies.RefreshControl();
                            break;
                        }
                    case "Upload Claim":
                        {
                            _ucUploadClaim.RefreshControl();
                            break;
                        }
                    case "Compensation Method":
                        {
                            _ucCompensationMethod.RefreshControl();
                            break;
                        }
                    case "Measures":
                        {
                            _ucAcctMeasures.RefreshControl();
                            break;
                        }
                    case "Import Workbook":
                        {
                            _ucImportWorkbook.RefreshControl();
                            break;
                        }
                    case "Initial Review Queue":
                        {
                            _ucInitialReviewQueue.RefreshControl();
                            break;
                        }
                }
                _customTaskPane.DockPosition = Microsoft.Office.Core.MsoCTPDockPosition.msoCTPDockPositionLeft;
                _customTaskPane.DockPositionRestrict = Microsoft.Office.Core.MsoCTPDockPositionRestrict.msoCTPDockPositionRestrictNoChange;
                _customTaskPane.Width = 450;

                Cursor.Current = Cursors.Arrow;
                this.RibbonMain.ActivateTabMso("TabAddIns");
                if (title.Contains("Save") || title.Contains("Upload") && !_ucCheckOnce)
                    SetActiveWorkbook();
                _customTaskPane.Visible = true;
                _customTaskPane.Control.Visible = true;
                _ucCheckOnce = false;
            }
            catch
            {
                ResetUserControls();
            }
        }

        //private void Workbook_After_Save(Excel.Workbook Wb, bool Success)
        //{
        //    Cursor.Current = Cursors.WaitCursor;
        //    if (iContract.CurrentClaimantId() != 0)
        //    {
        //        try
        //        {
        //            if (Globals.ThisAddIn.Application.Workbooks.Count == 0)
        //            {
        //                MessageBox.Show("No workbook to save.");
        //                return;
        //            }
        //            new LocalCopiesHelper().SaveLocalCopy(iContract.UploadModel());
        //            MessageBox.Show("Successfully saved to local.");
        //        }
        //        catch (Exception ex)
        //        {
        //            MessageBox.Show("An error occured while saving to local. Details: " + ex.ToString());
        //        }
        //    }
        //    Cursor.Current = Cursors.Arrow;
        //}


        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {

        }

        public Excel.Worksheet GetDownloadSheet()
        {
            try
            {
                foreach (Excel.Worksheet ws in Application.ActiveWorkbook.Worksheets)
                {
                    if (ws.Name.ToLower().Contains("download"))
                        return ws;
                }
            }
            catch { return null; }
            return null;
        }

        public void SetEnvironmentIndicator()
        {
            foreach (var csp in this.CustomTaskPanes)
            {
                if (csp.Title == "EnvironmentIndicator")
                {
                    this.CustomTaskPanes.Remove(csp);
                    _ucEnviromentIndicator = new ucEnvironmentIndicator();
                    this.CustomTaskPanes.Add(_ucEnviromentIndicator, "EnvironmentIndicator");
                    break;
                }
            }

            _environmentIndicatorTaskPane = this.CustomTaskPanes.FirstOrDefault(x => x.Title == "EnvironmentIndicator");
            _ucEnviromentIndicator = (ucEnvironmentIndicator)_environmentIndicatorTaskPane.Control;
            _environmentIndicatorTaskPane.DockPosition = Microsoft.Office.Core.MsoCTPDockPosition.msoCTPDockPositionTop;
            _environmentIndicatorTaskPane.DockPositionRestrict = Microsoft.Office.Core.MsoCTPDockPositionRestrict.msoCTPDockPositionRestrictNoVertical;
            int width = Convert.ToInt32(this.Application.Width);
            _ucEnviromentIndicator.Width = width;
            _environmentIndicatorTaskPane.Height = 65;
            _environmentIndicatorTaskPane.Visible = true;
            _environmentIndicatorTaskPane.Control.Visible = true;
            string envStr = iContract.AppSetting().WebServiceEnvironment;
            System.Windows.Forms.Label lbl = new System.Windows.Forms.Label();
            lbl.Name = "lblEnv";
            lbl.Width = width;
            lbl.Height = 30;
            foreach (Control c in _ucEnviromentIndicator.Controls)
            {
                if (c.Name == "lblEnv")
                    _ucEnviromentIndicator.Controls.Remove(c);
            }
            if (envStr == "--Environments--")
            {
                lbl.BackColor = System.Drawing.Color.Red;
                lbl.ForeColor = System.Drawing.Color.White;
                lbl.Text = "NO ENVIRONMENT SELECTED -- GO TO YOUR SETTINGS AND UPDATE NOW!!!!!";

            }
            else if (envStr == "Development")
            {
                lbl.BackColor = System.Drawing.Color.Orange;
                lbl.ForeColor = System.Drawing.Color.White;
                lbl.Text = "Development - DO NOT USE IF YOU ARE NOT A DEVELOPER";
            }
            else if (envStr == "Training")
            {
                lbl.BackColor = System.Drawing.Color.Green;
                lbl.ForeColor = System.Drawing.Color.White;
                lbl.Text = "495 TRAINING ENVIRONMENT";
            }
            else if (envStr == "Live")
            {
                _environmentIndicatorTaskPane.Control.Visible = false;
            }
            _ucEnviromentIndicator.Controls.Add(lbl);
        }



        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }

        #endregion
    }
}
