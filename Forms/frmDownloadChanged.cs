﻿using DheccExcelAddIn2013.Helpers;
using DheccExcelAddIn2013.Interfaces;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DheccExcelAddIn2013.Forms
{
    public partial class frmDownloadChanged : Form
    {
        private static List<string> _updatedDbList;
        private static List<string> _currentDbList;
        private IDheccContract iContract;

        public frmDownloadChanged(List<string> updatedDbList, List<string> currentDbList)
        {
            InitializeComponent();
            _updatedDbList = updatedDbList;
            _currentDbList = currentDbList;
            iContract = new DheccContract();
        }


        private void btnUpdate_Click(object sender, EventArgs e)
        {
            DownloadBelHelper.Set(_updatedDbList);
            iContract.CurrentWorkbookWrapper().ClaimInfo.DownloadBel = _updatedDbList;
            new WIPHelper().SaveWIP(iContract.CurrentWorkbookWrapper());
            WorkbookTemplate.SaveWorkbook(false);
            MessageBox.Show("Download data successfully updated.");
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            DownloadBelHelper.CopyDownloadFieldNames(_currentDbList.Count);
            Microsoft.Office.Interop.Excel.Workbook wb = Globals.ThisAddIn.Application.Workbooks.Add();
            var ws = wb.Worksheets.Add();

            //current values
            Range rng = ws.Range["C1", "C1"];
            ExcelPaster.SetCellValue(ws, "C1", "Current Download Values");
            rng = ws.Range["C2", "C" + _currentDbList.Count()];
            ExcelPaster.PasteVerticleSingleDimension(_currentDbList, rng);

            ExcelPaster.SetCellValue(ws, "D1", "Updated Download Values");
            rng = ws.Range["D2", "D" + _updatedDbList.Count()];
            ExcelPaster.PasteVerticleSingleDimension(_updatedDbList, rng);

            ws.Activate();
            rng = ws.Range["B2", "B" + _updatedDbList.Count()];
            rng.Select();
            ws.Paste(Type.Missing, Type.Missing);

            for (int i = 2; i <= _updatedDbList.Count; i++)
            {
                string val1 = ExcelPaster.GetCellValue(ws, "C" + i);
                string val2 = ExcelPaster.GetCellValue(ws, "D" + i);
                if (val1 != val2)
                {
                    rng = ws.Range["B" + i, "D" + i];
                    rng.Interior.Color = System.Drawing.Color.Yellow;
                }
            }
        }
    }
}
