﻿namespace DheccExcelAddIn2013.UserControls
{
    partial class ucMeasures
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbMeasures = new System.Windows.Forms.GroupBox();
            this.pnlMeasureCb = new System.Windows.Forms.Panel();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.gbMeasures.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbMeasures
            // 
            this.gbMeasures.Controls.Add(this.pnlMeasureCb);
            this.gbMeasures.Location = new System.Drawing.Point(35, 33);
            this.gbMeasures.Name = "gbMeasures";
            this.gbMeasures.Size = new System.Drawing.Size(817, 562);
            this.gbMeasures.TabIndex = 8;
            this.gbMeasures.TabStop = false;
            this.gbMeasures.Text = "Measures";
            // 
            // pnlMeasureCb
            // 
            this.pnlMeasureCb.Location = new System.Drawing.Point(38, 53);
            this.pnlMeasureCb.Name = "pnlMeasureCb";
            this.pnlMeasureCb.Size = new System.Drawing.Size(736, 472);
            this.pnlMeasureCb.TabIndex = 0;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(364, 617);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(155, 55);
            this.btnUpdate.TabIndex = 9;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // ucMeasures
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.gbMeasures);
            this.Name = "ucMeasures";
            this.Size = new System.Drawing.Size(900, 1174);
            this.gbMeasures.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbMeasures;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Panel pnlMeasureCb;
    }
}

