﻿using DheccExcelAddIn2013.Helpers;
using DheccExcelAddIn2013.Models;
using Microsoft.Office.Tools;
using Microsoft.Office.Tools.Ribbon;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Office.Core;

namespace DheccExcelAddIn2013.Interfaces
{
    public interface IDheccContract
    {

        
        #region Main Area
        BackgroundWorker BackgroundUploadWorker();
        void SetBackgroudUploadWorker(BackgroundWorker bgw);
        void SetAppSettingsHelper(AppSettingHelper appsetting);

        AppSettingHelper AppSettingHelper();
        
        #endregion

        #region MyAccount

        string Version();
        string CurrentAvailableVersion();
        void SetCurrentAvailableVersion(string currentVersion);
        AppSetting AppSetting();
        void SetAppSetting(AppSetting appSettings);

        #endregion

        #region Wrapper
        void SetCurrentWorkbookWrapper(WorkbookWrapper ww);
        WorkbookWrapper CurrentWorkbookWrapper();
        int CurrentClaimId();
        int CurrentClaimantId();
        void SetUsersClaims(List<int> clms);
        List<int> UsersClaims();
      #endregion

        #region Revision

       
        void ShowHideButtons();

       


        #endregion

    }
}
