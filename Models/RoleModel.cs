﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DheccExcelAddIn2013.Models
{
    public class RoleModel
    {
        public int Id { get; set; }
        public string Description { get; set; }       
    }
}
