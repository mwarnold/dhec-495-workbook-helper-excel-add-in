﻿using DheccExcelAddIn2013.Models.BELAVMConstruction.CalculationNotes.v3_0;
using DheccExcelAddIn2013.Models.BELAVMConstruction.Causation.v3_0;
using DheccExcelAddIn2013.Models.BELAVMConstruction.Declaration.v3_0;
using DheccExcelAddIn2013.Models.BELAVMConstruction.DeclarationRestated.v3_0;
using DheccExcelAddIn2013.Models.BELAVMConstruction.Documentation.v3_0;
using DheccExcelAddIn2013.Models.BELAVMConstruction.InputA1.v3_0;
using DheccExcelAddIn2013.Models.BELAVMConstruction.MethodologySelection.v3_0;
using DheccExcelAddIn2013.Models.BELAVMConstruction.PnL.v3_0;
using DheccExcelAddIn2013.Models.BELAVMConstruction.PnLRestated.v3_0;
using DheccExcelAddIn2013.Models.BELAVMConstruction.SeafoodRetailerTest.v3_0;
using DheccExcelAddIn2013.Models.BELAVMConstruction.TaxReconciliation.v3_0;
using DheccExcelAddIn2013.Models.BELAVMConstruction.TaxReturn.v3_0;
using DheccExcelAddIn2013.Models.BELAVMConstruction.TaxVariance.v3_0;
using DheccExcelAddIn2013.Models.BELAVMConstruction.TotalCalculatedLoss.v3_0;
using DheccExcelAddIn2013.Models.BELAVMConstruction.VariableExpenses.v3_0;
using DheccExcelAddIn2013.Models.BELAVMConstruction.VariableExpensesRestated.v3_0;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DheccExcelAddIn2013.Models.BELAVMConstruction.Workbook.v3_0
{
    public class WorkbookDataModel
    {
        public List<string> DownloadBel { get; set; }
        public List<CalculationNoteModel> CalculationNotes { get; set; }
        public CausationModel Causation { get; set; }
        public DeclarationModel Delcaration { get; set; }
        public DeclarationRestatedModel DeclarationRestated { get; set; }
        public DocumentationChecklistModel Documentation { get; set; }
        public InputA1Model InputTab { get; set; }
        public MethodologySelectionModel MethodologySelection { get; set; }
        public PandLModel PnL { get; set; }
        public PandLRestatedModel PnLRestated { get; set; }
        public SeafoodRetailerTestModel SeafoodRetailerTest { get; set; }
        public TaxReconciliationModel TaxReconciliation { get; set; }
        public TaxReturnA3Model TaxReturnA3 { get; set; }
        public TaxReturnForm8825Model TaxReturnForm8825 { get; set; }
        public TaxReturnSchEModel TaxReturnSchE { get; set; }
        public TaxVarianceA31Model TaxVarianceA31 { get; set; }
        public TotalCalculatedLossModel TotalCalculatedLoss { get; set; }
        public VariableExpensesModel VariableExpenses { get; set; }
        public VariableExpensesRestatedModel VariableExpensesRestated { get; set; }

    }
}
