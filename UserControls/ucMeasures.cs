﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DheccExcelAddIn2013.Interfaces;
using DheccExcelAddIn2013.Models;
using DheccExcelAddIn2013.Helpers;

namespace DheccExcelAddIn2013.UserControls
{
    public partial class ucMeasures : UserControl
    {
        private IDheccContract iContract;
        private static List<AcctStepModel> _measures;
        private static List<MeasureModel> _availMeasures;
        public ucMeasures()
        {
            InitializeComponent();
        }

        public void RefreshControl()
        {
            iContract = new DheccContract();
            _availMeasures = new List<MeasureModel>();
            _availMeasures = iContract.AppSetting().Measures.FindAll(x => x.DeActivatedDate == null || x.DeActivatedDate == DateTime.MinValue)
                    .OrderBy(x => x.OrderByIndex).Distinct().ToList();
            pnlMeasureCb.Controls.Clear();
            foreach (var m in _availMeasures)
            {
                CheckBox cb = new CheckBox();
                cb.Name = string.Format("cb{0}", m.Id);
                cb.Text = m.Name;
                cb.Dock = DockStyle.Top;
                cb.Width = 715;
                cb.Height = 40;
                cb.CheckedChanged += cb_CheckedChanged;
                pnlMeasureCb.Controls.Add(cb);
            }
            _measures = new List<AcctStepModel>();

            this.iContract = new DheccContract();
            var acctSteps = iContract.CurrentWorkbookWrapper().Measures;
            var results = (from r in acctSteps
                           group r by r.StepName into g
                           select g.OrderByDescending(x => x.Started).First());

            foreach (var result in results)
            {
                if (!result.Checked)
                    continue;
                foreach (Control c in pnlMeasureCb.Controls)
                {
                    if (c is CheckBox)
                    {
                        if (c.Text == result.StepName)
                        {
                            CheckBox cb = (CheckBox)c;
                            cb.Checked = true;
                        }
                    }
                }
            }
            btnUpdate.Enabled = false;
        }

        void cb_CheckedChanged(object sender, EventArgs e)
        {
            btnUpdate.Enabled = true;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            AcctStepModel asm = new AcctStepModel();
            foreach (Control c in pnlMeasureCb.Controls)
            {
                if (c is CheckBox)
                {
                    CheckBox cb = (CheckBox)c;
                    var result = iContract.CurrentWorkbookWrapper().Measures.OrderByDescending(d => d.Started)
                        .FirstOrDefault(x => x.StepName == cb.Text && x.Checked != cb.Checked);
                    if (result == null && !cb.Checked)
                        continue;
                    string cbName = cb.Name.Replace("cb", "");
                    int mId = int.Parse(cbName);
                    asm =
                      new AcctStepModel()
                      {
                          Started = DateTime.Now,
                          StepName = cb.Text,
                          Checked = cb.Checked,
                          UserName = iContract.AppSetting().User.UserName,
                          MeasureId = mId
                      };
                    iContract.CurrentWorkbookWrapper().ReviewerHistory.Add(
                   new ReviewerHistoryModel()
                   {
                       Date = DateTime.Now,
                       Description = cb.Text + " set to " + (cb.Checked ? "Complete" : "Incomplete"),
                       ReviewedBy = iContract.AppSetting().User.UserName,
                   });
                    try
                    {
                        if (AppVariables.IsLoggedIn)
                        {
                            try
                            {
                                var client = AppVariables.BGWebService();
                                var cResult = client.clsAddAcctMeasure(AppVariables.CurrentUserId, AppVariables.AuthToken, mId,
                                    iContract.CurrentClaimId(), iContract.CurrentClaimantId(),
                                    asm.Started, asm.Started, asm.Checked);
                                if (WebServiceResponseHelper.ValidateResponse(cResult.Result.ResultID, cResult.Result.ResultMessage))
                                    asm.PendingUpload = false;
                            }
                            catch { }
                        }
                        else
                            asm.PendingUpload = true;

                        iContract.CurrentWorkbookWrapper().Measures.Add(asm);
                    }
                    catch { }
                }
            }
            new WIPHelper().SaveWIP(iContract.CurrentWorkbookWrapper());
            Cursor.Current = Cursors.Arrow;
            btnUpdate.Enabled = false;
        }
    }
}
