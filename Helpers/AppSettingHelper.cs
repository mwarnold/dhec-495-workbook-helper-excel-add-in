﻿using DheccExcelAddIn2013.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DheccExcelAddIn2013.Helpers
{
    public class AppSettingHelper
    {
        private const string _appSettings = "AppSettings.txt";
        public AppSettingHelper()
        {
            DoCheck();
        }

        public AppSetting GetSettings()
        {
            String json = "";
            try
            {
                string fp = AppVariables.LocalDataDirectory();
                using (StreamReader sr = new StreamReader(fp + _appSettings))
                    json = sr.ReadToEnd();
            }
            catch { }
            if (string.IsNullOrEmpty(json))
            {
                var newSetting = new AppSetting()
                {
                    UserAddIn = new AddInModel()
                    {
                        Active = true,
                        Description = "",
                        Id = 0,
                        Version = "0.0"
                    }
                };
                SaveSettings(newSetting);
                return newSetting;
            }
            var axs = (AppSetting)JsonConvert.DeserializeObject<AppSetting>(json);
            return axs;
        }

        public bool SaveSettings(AppSetting settings)
        {
            try
            {
                using (StreamWriter sw = new StreamWriter(AppVariables.LocalDataDirectory() + _appSettings))
                {
                    sw.WriteLine(JsonConvert.SerializeObject(settings));
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        private void DoCheck()
        {
            if (!Directory.Exists(AppVariables.RootExcelAddInFileDirectory()))
                Directory.CreateDirectory(AppVariables.RootExcelAddInFileDirectory());
            if (!Directory.Exists(AppVariables.LocalDataRootDirectory()))
                Directory.CreateDirectory(AppVariables.LocalDataRootDirectory());
            if (!Directory.Exists(AppVariables.LocalExcelFileRootDirectory()))
                Directory.CreateDirectory(AppVariables.LocalExcelFileRootDirectory());
           
        }
    }
}

