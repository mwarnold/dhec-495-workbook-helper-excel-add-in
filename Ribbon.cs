﻿using DheccExcelAddIn2013.Forms;
using DheccExcelAddIn2013.Helpers;
using DheccExcelAddIn2013.Interfaces;
using DheccExcelAddIn2013.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using Office = Microsoft.Office.Core;

// TODO:  Follow these steps to enable the Ribbon (XML) item:

// 1: Copy the following code block into the ThisAddin, ThisWorkbook, or ThisDocument class.

//  protected override Microsoft.Office.Core.IRibbonExtensibility CreateRibbonExtensibilityObject()
//  {
//      return new Ribbon();
//  }

// 2. Create callback methods in the "Ribbon Callbacks" region of this class to handle user
//    actions, such as clicking a button. Note: if you have exported this Ribbon from the Ribbon designer,
//    move your code from the event handlers to the callback methods and modify the code to work with the
//    Ribbon extensibility (RibbonX) programming model.

// 3. Assign attributes to the control tags in the Ribbon XML file to identify the appropriate callback methods in your code.  

// For more information, see the Ribbon XML documentation in the Visual Studio Tools for Office Help.


namespace DheccExcelAddIn2013
{
    [ComVisible(true)]
    public class Ribbon : Office.IRibbonExtensibility
    {
        private Office.IRibbonUI ribbon;
        IDheccContract iContract;
        public Ribbon()
        {
            iContract = new DheccContract();
            if (iContract.CurrentWorkbookWrapper() == null)
                iContract.SetCurrentWorkbookWrapper(new WorkbookWrapper());
        }

        public void Refresh()
        {
            Ribbon_Load(this.ribbon);
        }

        #region IRibbonExtensibility Members

        public string GetCustomUI(string ribbonID)
        {
            return GetResourceText("DheccExcelAddIn2013.Ribbon.xml");
        }

        #endregion

        #region Ribbon Callbacks
        //Create callback methods here. For more information about adding callback methods, visit http://go.microsoft.com/fwlink/?LinkID=271226

        public void Ribbon_Load(Office.IRibbonUI ribbonUI)
        {
            this.ribbon = ribbonUI;
            Globals.ThisAddIn.RibbonMain = this.ribbon;
        }
        public void KillDeveloper(Office.IRibbonControl control, bool cancelDefault)
        {
            //reqire password if 495 wb openeed
            if (AppVariables.DWH495WorkbookOpened)
            {
                //if prev entered pw don't prompt again
                if (!AppVariables.DeveloperPasswordEntered)
                {
                    frmAdmin _frmAdmin = new frmAdmin();
                    _frmAdmin.ShowDialog();
                    if (_frmAdmin.IsValidPassword)
                        Globals.ThisAddIn.Application.VBE.MainWindow.Visible = true;
                }
                else
                    Globals.ThisAddIn.Application.VBE.MainWindow.Visible = true;
            }
            else
                Globals.ThisAddIn.Application.VBE.MainWindow.Visible = true;
        }

        public void btnLogin_Click(Office.IRibbonControl control)
        {
            Globals.ThisAddIn.SetCustomTaskPane("Login");
            Globals.ThisAddIn.RibbonMain.Invalidate();
        }

        public void btnLogout_Click(Office.IRibbonControl control)
        {
            AppVariables.SetIsLoggedIn(false);
            AppVariables.SetCurrentUserId(0);
            AppVariables.SetAuthToken("");
            iContract.ShowHideButtons();
            Globals.ThisAddIn.SetCustomTaskPane("Reset");
        }

        public void btnSettings_Click(Office.IRibbonControl controlpublic)
        {
            Globals.ThisAddIn.SetCustomTaskPane("My Account");
        }

        public void btnVersion_Click(Office.IRibbonControl controlpublic)
        {
            Globals.ThisAddIn.SetCustomTaskPane("My Account");
        }

        public void btnChanges_Click(Office.IRibbonControl controlpublic)
        {
            Globals.ThisAddIn.SetCustomTaskPane("My Account");
        }

        public void btnImportBEL_Click(Office.IRibbonControl controlpublic)
        {
            Globals.ThisAddIn.SetCustomTaskPane("My Portal Queue");
        }

        public void btnInitialQueue_Click(Office.IRibbonControl controlpublic)
        {
            Globals.ThisAddIn.SetCustomTaskPane("My Portal Queue");
        }

        public void btnComments_Click(Office.IRibbonControl controlpublic)
        {
            Globals.ThisAddIn.SetCustomTaskPane("Comments");
        }

        public void btnHistory_Click(Office.IRibbonControl controlpublic)
        {
            Globals.ThisAddIn.SetCustomTaskPane("Reviewer History");
        }

        public void btnPrintPDF_Click(Office.IRibbonControl controlpublic)
        {

            RunMacro(Globals.ThisAddIn.Application, new Object[] { "Module1.Workbook_To_PDF" });
        }

        public void btnFromRepo_Click(Office.IRibbonControl controlpublic)
        {
            Globals.ThisAddIn.SetCustomTaskPane("Open Existing Claim");
        }

        public void btnSave_Click(Office.IRibbonControl control)
        {
            var wrkingCopy = new WIPHelper().GetWIP().SingleOrDefault(
                x => x.ClaimInfo.ClaimantId == iContract.CurrentClaimantId() && x.ClaimInfo.ClaimId == iContract.CurrentClaimId()
                    && x.CanEdit == true);
            if (!iContract.CurrentWorkbookWrapper().CanEdit && wrkingCopy != null)
            {
                DialogResult dialogRes = MessageBox.Show("Warning! You are attempting to overwrite a local copy of this workbook with the selected remote copy. Do you wish to continue?", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dialogRes == DialogResult.No)
                    return;
            }
            Cursor.Current = Cursors.WaitCursor;
            WorkbookTemplate.SaveWorkbook();
            Cursor.Current = Cursors.Arrow;
        }

        public void RunMacro(object oApp, object[] oRunArgs)
        {
            oApp.GetType().InvokeMember("Run",
                System.Reflection.BindingFlags.Default |
                System.Reflection.BindingFlags.InvokeMethod,
                null, oApp, oRunArgs);
        }

        public void btnSaveToRemote_Click(Office.IRibbonControl control)
        {
            Globals.ThisAddIn.SetCustomTaskPane("Check-In Claim");
        }

        public void btnUploadClaim_Click(Office.IRibbonControl control)
        {
            Globals.ThisAddIn.SetCustomTaskPane("Upload Claim");
        }

        public void btnMeasures_Click(Office.IRibbonControl control)
        {
            Globals.ThisAddIn.SetCustomTaskPane("Measures");
        }

        public void btnImportExisting_Click(Office.IRibbonControl control)
        {
            Globals.ThisAddIn.SetCustomTaskPane("Import Workbook");
        }

        public bool GetEnabled(Office.IRibbonControl control)
        {
            bool isActiveWorkbook = false;
            if (iContract.AppSetting() != null)
                isActiveWorkbook = iContract.AppSetting().ActiveWorkbooks.Any(x => x.Id == iContract.CurrentWorkbookWrapper().Workbook.Id);
            bool canEdit = iContract.CurrentWorkbookWrapper().CanEdit;
            return AppVariables.IsLoggedIn;

        }

        public bool Visibility_False(Office.IRibbonControl control)
        {
            return false;
        }

        public bool Enabled_ActiveAndCanEdit(Office.IRibbonControl control)
        {
            bool isActiveWorkbook = false;
            if (iContract.AppSetting() != null)
            {
                if (iContract.CurrentWorkbookWrapper().IsImportedClaim)
                    isActiveWorkbook = true;
                else
                    isActiveWorkbook = iContract.AppSetting().ActiveWorkbooks.Any(x => x.Id == iContract.CurrentWorkbookWrapper().Workbook.Id);
            }
            bool canEdit = iContract.CurrentWorkbookWrapper().CanEdit;
            return isActiveWorkbook && canEdit;
        }

        public bool Enabled_WorkbookOpened(Office.IRibbonControl control)
        {
            try
            {
                var claimInfo = WorkbookTemplate.GetActiveWorkbookClaimInfo();
                return claimInfo.ClaimantId > 0;
            }
            catch
            {
                return false;
            }
        }

        public bool Enabled_LoggedInAndWorkbookOpened(Office.IRibbonControl control)
        {
            try
            {
                var claimInfo = WorkbookTemplate.GetActiveWorkbookClaimInfo();
                return AppVariables.IsLoggedIn && claimInfo.ClaimantId > 0;
            }
            catch
            {
                return false;
            }
        }

        public bool Enabled_ActiveAndCanEditAndLoggedIn(Office.IRibbonControl control)
        {
            bool isActiveWorkbook = false;
            if (iContract.AppSetting() != null)
            {
                if (iContract.CurrentWorkbookWrapper().IsImportedClaim)
                    isActiveWorkbook = true;
                else
                    isActiveWorkbook = iContract.AppSetting().ActiveWorkbooks.Any(x => x.Id == iContract.CurrentWorkbookWrapper().Workbook.Id);
            }
            if (!isActiveWorkbook)
                isActiveWorkbook = iContract.CurrentWorkbookWrapper().IsImportedClaim;
            bool canEdit = iContract.CurrentWorkbookWrapper().CanEdit;
            return isActiveWorkbook && canEdit && AppVariables.IsLoggedIn;
        }

        public bool Enabled_IsLoggedIn(Office.IRibbonControl control)
        {
            return AppVariables.IsLoggedIn;
        }
        public bool Enabled_IsLoggedOut(Office.IRibbonControl control)
        {
            return !AppVariables.IsLoggedIn;
        }
        public bool Enabled_MyPortalQueue(Office.IRibbonControl control)
        {
            if (iContract.BackgroundUploadWorker() == null)
                return true;
            return !iContract.BackgroundUploadWorker().IsBusy;
        } 

        public bool Visible_IsLoggedIn(Office.IRibbonControl control)
        {
            return AppVariables.IsLoggedIn;
        }
        public bool Visible_IsLoggedOut(Office.IRibbonControl control)
        {
            return !AppVariables.IsLoggedIn;
        }


        public Image Image_btnLogin(Office.IRibbonControl control)
        {
            return DheccExcelAddIn2013.Properties.Resources.loginIcon; 
        }

        public Image Image_btnLogOut(Office.IRibbonControl control)
        {
            return DheccExcelAddIn2013.Properties.Resources.loginIcon;
        }

        public Image Image_btnQueue(Office.IRibbonControl control)
        {
            return DheccExcelAddIn2013.Properties.Resources.DownloadIcon;
        }

     
        public Image Image_btnSettings(Office.IRibbonControl control)
        {
            return DheccExcelAddIn2013.Properties.Resources.MyAccountIcon;
        }

        public Image Image_btnSave(Office.IRibbonControl control)
        {
            return DheccExcelAddIn2013.Properties.Resources.saveIcon;
        }

        public Image Image_btnSaveToRemote(Office.IRibbonControl control)
        {
            return DheccExcelAddIn2013.Properties.Resources.UploadIcon;
        }

        public Image Image_btnRemoteCopies(Office.IRibbonControl control)
        {
            return DheccExcelAddIn2013.Properties.Resources.DownloadIcon;
        }

        public Image Image_btnUploadClaim(Office.IRibbonControl control)
        {
            return DheccExcelAddIn2013.Properties.Resources.UploadFinalIcon;
        }
        public Image Image_btnImportExisting(Office.IRibbonControl control)
        {
            return DheccExcelAddIn2013.Properties.Resources.openLocalIcon;
        }
        public Image Image_btnMeasures(Office.IRibbonControl control)
        {
            return DheccExcelAddIn2013.Properties.Resources.MeasureIcon;
        }
        public Image Image_btnComments(Office.IRibbonControl control)
        {
            return DheccExcelAddIn2013.Properties.Resources.commentsIcon;
        }
        public Image Image_btnHistory(Office.IRibbonControl control)
        {
            return DheccExcelAddIn2013.Properties.Resources.HistoryIcon;
        }
       
        #endregion

        #region Helpers

        private static string GetResourceText(string resourceName)
        {
            Assembly asm = Assembly.GetExecutingAssembly();
            string[] resourceNames = asm.GetManifestResourceNames();
            for (int i = 0; i < resourceNames.Length; ++i)
            {
                if (string.Compare(resourceName, resourceNames[i], StringComparison.OrdinalIgnoreCase) == 0)
                {
                    using (StreamReader resourceReader = new StreamReader(asm.GetManifestResourceStream(resourceNames[i])))
                    {
                        if (resourceReader != null)
                        {
                            return resourceReader.ReadToEnd();
                        }
                    }
                }
            }
            return null;
        }

        #endregion


    }
}
