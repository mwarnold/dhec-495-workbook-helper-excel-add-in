﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DheccExcelAddIn2013.Interfaces;
using DheccExcelAddIn2013.Models;
using DheccExcelAddIn2013.Helpers;

namespace DheccExcelAddIn2013.UserControls
{
    public partial class ucInitialReviewQueue : UserControl
    {
        private IDheccContract iContract;
        private OpenFileDialog ofd = new OpenFileDialog();
        private static List<com.browngreer.www3.ClaimInfo> _initialReviewQueue = new List<com.browngreer.www3.ClaimInfo>();
        private static WorkbookWrapper _selectedWorkbook = new WorkbookWrapper();
        private static com.browngreer.www3.ClaimInfo clsClaimInfo = new com.browngreer.www3.ClaimInfo();
        private static string _selectedFilePathToSave = "";
        private static int _selectedCompensationMethod = 0;

        public ucInitialReviewQueue()
        {
            this.iContract = new DheccContract();
            InitializeComponent();           
        }


        public void RefreshControl()
        {
            //if this explodes, user hasn't set up programmatic access to the vba module and cannot continue. 
            try
            {
                var i = Globals.ThisAddIn.Application.ActiveWorkbook.VBProject.VBComponents.Count;
            }
            catch
            {
                MessageBox.Show("The DWH Workbook Helper Add In required programmatic access to the VBA Module. You will be redirected to the Add In Settings. In the 'Notifications' tab, you will find the instructions for setting this up in Excel. It should take less than two minutes.");
                Globals.ThisAddIn.SetCustomTaskPane("My Account");
                return;
            }
            _initialReviewQueue = new List<com.browngreer.www3.ClaimInfo>();
            cbOnlyNew.Checked = true;
            LoadQueue();
            if (iContract.BackgroundUploadWorker() != null)
            {
                if (iContract.BackgroundUploadWorker().IsBusy)
                {
                    dgInitialReview.Enabled = false;
                    MessageBox.Show("The Add In is collecting files from the server. This process usually takes less than a minute depending on your internet connection. The Add In will automatically attempt to open you queue agian. Please click ok.");
                    Cursor.Current = Cursors.WaitCursor;
                    System.Threading.Thread.Sleep(5000);
                    Cursor.Current = Cursors.Arrow;
                    RefreshControl();
                }
            }
        }

        protected class InitialReviewQueueItem
        {
            public bool New { get; set; }
            public string ClaimantInfo { get; set; }
            public string ClaimId { get; set; }
        }

        private void LoadQueue()
        {
            try
            {
                //get list of wip 
                var wipHelper = new WIPHelper();
                List<int> usersClaimIds = new List<int>();

                var client = AppVariables.BGWebService();
                var claims = client.GetBELExcelUserClaims(AppVariables.CurrentUserId, AppVariables.AuthToken);
                if (!WebServiceResponseHelper.ValidateResponse(claims.Result.ResultID, claims.Result.ResultMessage))
                    return;

                WorkbookTemplate.UpdateActiveWorkbookList(claims.WorkbookInfoList);
                _initialReviewQueue = new List<com.browngreer.www3.ClaimInfo>();
                foreach (var clsClaimInfo in claims.ClaimInfoList)
                {
                    //these are all the user's queue items. 
                    //only show the initial review items and show a checkbox if prev opened
                   // if (clsClaimInfo.ReviewQueueTypeID == AppVariables.InitialReviewerTypeId)
                        _initialReviewQueue.Add(clsClaimInfo);
                    usersClaimIds.Add(clsClaimInfo.ClaimID);
                }
                List<InitialReviewQueueItem> initRevList = new List<InitialReviewQueueItem>();
                foreach (var cls in _initialReviewQueue)
                {
                    int claimId = Convert.ToInt32(cls.ClaimID);
                    bool isWorked = iContract.AppSetting().StartedClaims.Any(x => x == claimId);
                    if (cbOnlyNew.Checked && isWorked)
                        continue;
                    initRevList.Add(new InitialReviewQueueItem()
                        {
                            ClaimantInfo = string.Format("{0}-#{1}", cls.ClaimantInfo.ClaimantName, cls.ClaimantInfo.ClaimantID),
                            ClaimId = claimId.ToString(),
                            New = !isWorked
                        });
                }

                if (!string.IsNullOrEmpty(tbSearch.Text))
                {
                    string ss = tbSearch.Text;
                    initRevList = initRevList.FindAll(x => x.ClaimantInfo.ToLower().Contains(ss.ToLower()) || x.ClaimId.ToString().Contains(ss));
                }

                dgInitialReview.DataSource = initRevList;
                if (initRevList.Count > 0)
                {
                    int width = ((dgInitialReview.Width / 3) - 15);
                    int widthBig = width * 2;
                    dgInitialReview.Columns[0].Width = 30;
                    dgInitialReview.Columns[1].Width = widthBig;
                    dgInitialReview.Columns[2].Width = width;
                }
                iContract.AppSetting().MyClaims = usersClaimIds;
                iContract.AppSettingHelper().SaveSettings(iContract.AppSetting());
            }
            catch (Exception ex)
            {
                WebServiceResponseHelper.ValidateResponse(0, "An unhandled exception has occured. Our dev team has been notifiied. We apologize for the inconvenience");
                EmailHelper.SendExceptionEmail(ex.ToString());
            }
        }



        private void DownloadClaimDataAndLoad()
        {
            var client = AppVariables.BGWebService();
            WorkbookWrapper ww = new WorkbookWrapper();
            ww.CanEdit = true;
            ww.CanEditCompensationMethod = clsClaimInfo.CompMethodOptions.PreSelectionCompMethodID > 0;
            ww.ClaimInfo.ClaimId = clsClaimInfo.ClaimID;
            ww.ClaimInfo.ClaimantId = clsClaimInfo.ClaimantInfo.ClaimantID;
            ww.ClaimInfo.ClaimantName = clsClaimInfo.ClaimantInfo.ClaimantName;
            var selCompMethod = clsClaimInfo.CompMethodOptions.CompCalcMethodIDList.SingleOrDefault(x => x.CompCalcMethodID == _selectedCompensationMethod);
            ww.CompensationMethodId = selCompMethod.CompCalcMethodID;
            ww.ReviewQueueTypeId = clsClaimInfo.ReviewQueueTypeID;
            ww.Workbook = iContract.AppSetting().ActiveWorkbooks
                .SingleOrDefault(x => x.WorkbookTypeId == selCompMethod.WorkbookTypeID);
            ReviewerHistoryModel rhm =
                new ReviewerHistoryModel()
                {
                    Date = DateTime.Now,
                    Description = "Claim Downloaded",
                    ReviewedBy = iContract.AppSetting().User.UserName,
                };
            ww.ReviewerHistory.Add(rhm);
            com.browngreer.www3.clsDownloadExcelInfo belData = client.GetDownloadExcelInfo(AppVariables.CurrentUserId, ww.ClaimInfo.ClaimantId,
            ww.ClaimInfo.ClaimId, AppVariables.AuthToken);
            if (!WebServiceResponseHelper.ValidateResponse(belData.Result.ResultID, belData.Result.ResultMessage))
                return;
            WorkbookTemplate.UpdateActiveWorkbookList(belData.WorkbookInfoList);
            List<string> dbList = new List<string>();
            foreach (var bv in belData.DownloadExcelAttrInfoList)
                dbList.Add(string.IsNullOrEmpty(bv.AttributeValue) ? "" : bv.AttributeValue);
            ww.AddIn = iContract.AppSetting().UserAddIn;
            ww.ClaimInfo.DownloadBel = dbList;
            ww.Workbook.LocalFilePath = _selectedFilePathToSave;
            if (!WorkbookTemplate.OpenNewWorkbook(ww))
            {
                MessageBox.Show("An error occured while opening the workbook template: " + ww.Workbook.Name);
                return;
            }
            DownloadBelHelper.Set(dbList);
            WorkbookTemplate.SaveWorkbook(false);
            new WIPHelper().SaveWIP(ww);
            iContract.AppSetting().StartedClaims.Add(ww.ClaimInfo.ClaimId);
            iContract.AppSettingHelper().SaveSettings(iContract.AppSetting());
        }


        private void btnSearch_Click(object sender, EventArgs e)
        {
            string ss = tbSearch.Text;
            dgInitialReview.DataSource = _initialReviewQueue.FindAll(x => x.ClaimantInfo.ClaimantName.ToLower().Contains(ss.ToLower()) ||
                    x.ClaimantInfo.ClaimantID.ToString().Contains(ss) || x.ClaimID.ToString().Contains(ss));
            tbSearch.Text = "";
        }

        private void aReset_Click(object sender, EventArgs e)
        {
            dgInitialReview.DataSource = _initialReviewQueue;
            tbSearch.Text = "";
        }

        private void dgInitialReview_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            foreach (DataGridViewRow dr in dgInitialReview.Rows)
            {
                try
                {
                    DataGridViewCheckBoxCell chkBox = new DataGridViewCheckBoxCell();
                    chkBox = (DataGridViewCheckBoxCell)dr.Cells[0];
                    chkBox.ReadOnly = true;
                }
                catch { }
            }
        }

        private void dgInitialReview_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex < 0)
                    return;

                DataGridViewRow dr = dgInitialReview.Rows[e.RowIndex];
                int claimId = int.Parse(dr.Cells[2].Value.ToString());
                if (iContract.AppSetting().StartedClaims.Any(x => x == claimId))
                {
                    DialogResult openExisting = MessageBox.Show("You have already started this claim. Would you like to open it now? If so, click 'Yes'. If you would like to start the claim over, click 'No' and a new claim will be opened. Any measure information will be lost.", "Notification", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if (openExisting == DialogResult.Yes)
                    {
                        WorkbookTemplate.OpenWorkInProgress(claimId);
                        return;
                    }
                }

                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Filter = "Excel Files(*.xlsm)|*.xlsm";
                DialogResult result = sfd.ShowDialog();
                if (result != DialogResult.OK)
                    return;
                _selectedFilePathToSave = sfd.FileNames.FirstOrDefault();
                Cursor.Current = Cursors.WaitCursor;
           
                clsClaimInfo = _initialReviewQueue.FirstOrDefault(x => x.ClaimID == claimId);

                //check the compensation method. If prev selected get the workbook type id and open wb. Else, display
                //options to the user and set the options for later
                if (clsClaimInfo.CompMethodOptions.PreSelectionCompMethodID > 0)
                    _selectedCompensationMethod = clsClaimInfo.CompMethodOptions.CompCalcMethodIDList.FirstOrDefault(x => x.CompCalcMethodID == clsClaimInfo.CompMethodOptions.PreSelectionCompMethodID).CompCalcMethodID;
                else
                    _selectedCompensationMethod = clsClaimInfo.CompMethodOptions.CompCalcMethodIDList.FirstOrDefault().CompCalcMethodID;
                DownloadClaimDataAndLoad();
            }
            catch (Exception ex)
            {
                WebServiceResponseHelper.ValidateResponse(0, "An unhandled exception has occured. Our dev team has been notifiied. We apologize for the inconvenience");
                EmailHelper.SendExceptionEmail(ex.ToString());
            }
        }

        private void cbOnlyNew_CheckedChanged(object sender, EventArgs e)
        {
            LoadQueue();
        }

        private void btnSearch_Click_1(object sender, EventArgs e)
        {
            string ss = tbSearch.Text;
            LoadQueue();
            tbSearch.Text = "";
        }

        private void aReset_Click_1(object sender, EventArgs e)
        {
            LoadQueue();
            tbSearch.Text = "";
        }

    }
}
