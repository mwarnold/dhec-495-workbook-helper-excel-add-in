﻿namespace DheccExcelAddIn2013.UserControls
{
    partial class ucMyAccount
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblWelcomeMessage = new System.Windows.Forms.Label();
            this.tpMaNotifications = new System.Windows.Forms.TabPage();
            this.btnDemoDownloadDiff = new System.Windows.Forms.Button();
            this.btnViewInst = new System.Windows.Forms.Button();
            this.tbMAMessages = new System.Windows.Forms.TextBox();
            this.tcMyAccount = new System.Windows.Forms.TabControl();
            this.tpSettings = new System.Windows.Forms.TabPage();
            this.gbSUrl = new System.Windows.Forms.GroupBox();
            this.ddlServiceUrl = new System.Windows.Forms.ComboBox();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.lblURLText = new System.Windows.Forms.Label();
            this.tpMaNotifications.SuspendLayout();
            this.tcMyAccount.SuspendLayout();
            this.tpSettings.SuspendLayout();
            this.gbSUrl.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblWelcomeMessage
            // 
            this.lblWelcomeMessage.AutoSize = true;
            this.lblWelcomeMessage.Location = new System.Drawing.Point(20, 18);
            this.lblWelcomeMessage.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblWelcomeMessage.Name = "lblWelcomeMessage";
            this.lblWelcomeMessage.Size = new System.Drawing.Size(70, 17);
            this.lblWelcomeMessage.TabIndex = 0;
            this.lblWelcomeMessage.Text = "Welcome,";
            // 
            // tpMaNotifications
            // 
            this.tpMaNotifications.Controls.Add(this.btnDemoDownloadDiff);
            this.tpMaNotifications.Controls.Add(this.btnViewInst);
            this.tpMaNotifications.Controls.Add(this.tbMAMessages);
            this.tpMaNotifications.Location = new System.Drawing.Point(4, 25);
            this.tpMaNotifications.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tpMaNotifications.Name = "tpMaNotifications";
            this.tpMaNotifications.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tpMaNotifications.Size = new System.Drawing.Size(404, 507);
            this.tpMaNotifications.TabIndex = 3;
            this.tpMaNotifications.Text = "Notifications & Help Files";
            this.tpMaNotifications.UseVisualStyleBackColor = true;
            // 
            // btnDemoDownloadDiff
            // 
            this.btnDemoDownloadDiff.Location = new System.Drawing.Point(65, 443);
            this.btnDemoDownloadDiff.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnDemoDownloadDiff.Name = "btnDemoDownloadDiff";
            this.btnDemoDownloadDiff.Size = new System.Drawing.Size(220, 28);
            this.btnDemoDownloadDiff.TabIndex = 2;
            this.btnDemoDownloadDiff.Text = "Download Tab Updated";
            this.btnDemoDownloadDiff.UseVisualStyleBackColor = true;
            this.btnDemoDownloadDiff.Click += new System.EventHandler(this.btnDemoDownloadDiff_Click);
            // 
            // btnViewInst
            // 
            this.btnViewInst.Location = new System.Drawing.Point(65, 404);
            this.btnViewInst.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnViewInst.Name = "btnViewInst";
            this.btnViewInst.Size = new System.Drawing.Size(254, 28);
            this.btnViewInst.TabIndex = 1;
            this.btnViewInst.Text = "View VBA Access Instructions";
            this.btnViewInst.UseVisualStyleBackColor = true;
            this.btnViewInst.Click += new System.EventHandler(this.btnViewInst_Click);
            // 
            // tbMAMessages
            // 
            this.tbMAMessages.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.tbMAMessages.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbMAMessages.Location = new System.Drawing.Point(15, 14);
            this.tbMAMessages.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbMAMessages.Multiline = true;
            this.tbMAMessages.Name = "tbMAMessages";
            this.tbMAMessages.ReadOnly = true;
            this.tbMAMessages.Size = new System.Drawing.Size(376, 319);
            this.tbMAMessages.TabIndex = 0;
            // 
            // tcMyAccount
            // 
            this.tcMyAccount.Controls.Add(this.tpMaNotifications);
            this.tcMyAccount.Controls.Add(this.tpSettings);
            this.tcMyAccount.Location = new System.Drawing.Point(18, 51);
            this.tcMyAccount.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tcMyAccount.Name = "tcMyAccount";
            this.tcMyAccount.SelectedIndex = 0;
            this.tcMyAccount.Size = new System.Drawing.Size(412, 536);
            this.tcMyAccount.TabIndex = 1;
            this.tcMyAccount.SelectedIndexChanged += new System.EventHandler(this.tcMyAccount_SelectedIndexChanged);
            // 
            // tpSettings
            // 
            this.tpSettings.Controls.Add(this.gbSUrl);
            this.tpSettings.Location = new System.Drawing.Point(4, 25);
            this.tpSettings.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tpSettings.Name = "tpSettings";
            this.tpSettings.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tpSettings.Size = new System.Drawing.Size(404, 507);
            this.tpSettings.TabIndex = 4;
            this.tpSettings.Text = "Settings";
            this.tpSettings.UseVisualStyleBackColor = true;
            // 
            // gbSUrl
            // 
            this.gbSUrl.Controls.Add(this.ddlServiceUrl);
            this.gbSUrl.Controls.Add(this.btnUpdate);
            this.gbSUrl.Controls.Add(this.lblURLText);
            this.gbSUrl.Location = new System.Drawing.Point(13, 17);
            this.gbSUrl.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.gbSUrl.Name = "gbSUrl";
            this.gbSUrl.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.gbSUrl.Size = new System.Drawing.Size(381, 127);
            this.gbSUrl.TabIndex = 0;
            this.gbSUrl.TabStop = false;
            this.gbSUrl.Text = "Web Service URL";
            // 
            // ddlServiceUrl
            // 
            this.ddlServiceUrl.FormattingEnabled = true;
            this.ddlServiceUrl.Items.AddRange(new object[] {
            "--Environments--",
            "Development",
            "Training"});
            this.ddlServiceUrl.Location = new System.Drawing.Point(150, 35);
            this.ddlServiceUrl.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ddlServiceUrl.Name = "ddlServiceUrl";
            this.ddlServiceUrl.Size = new System.Drawing.Size(180, 24);
            this.ddlServiceUrl.TabIndex = 3;
            this.ddlServiceUrl.SelectedIndexChanged += new System.EventHandler(this.ddlServiceUrl_SelectedIndexChanged);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(135, 82);
            this.btnUpdate.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(78, 26);
            this.btnUpdate.TabIndex = 2;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // lblURLText
            // 
            this.lblURLText.AutoSize = true;
            this.lblURLText.Location = new System.Drawing.Point(11, 39);
            this.lblURLText.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblURLText.Name = "lblURLText";
            this.lblURLText.Size = new System.Drawing.Size(134, 17);
            this.lblURLText.TabIndex = 0;
            this.lblURLText.Text = "Select Environment:";
            // 
            // ucMyAccount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Controls.Add(this.tcMyAccount);
            this.Controls.Add(this.lblWelcomeMessage);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "ucMyAccount";
            this.Size = new System.Drawing.Size(450, 606);
            this.Load += new System.EventHandler(this.ucMyAccount_Load);
            this.tpMaNotifications.ResumeLayout(false);
            this.tpMaNotifications.PerformLayout();
            this.tcMyAccount.ResumeLayout(false);
            this.tpSettings.ResumeLayout(false);
            this.gbSUrl.ResumeLayout(false);
            this.gbSUrl.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblWelcomeMessage;
        private System.Windows.Forms.TabPage tpMaNotifications;
        private System.Windows.Forms.TextBox tbMAMessages;
        private System.Windows.Forms.TabControl tcMyAccount;
        private System.Windows.Forms.TabPage tpSettings;
        private System.Windows.Forms.GroupBox gbSUrl;
        private System.Windows.Forms.Label lblURLText;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.ComboBox ddlServiceUrl;
        private System.Windows.Forms.Button btnViewInst;
        private System.Windows.Forms.Button btnDemoDownloadDiff;
    }
}
