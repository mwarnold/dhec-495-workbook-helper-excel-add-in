﻿using DheccExcelAddIn2013.Models.BELAVMConstruction.CalculationNotes.v3_0;
using DheccExcelAddIn2013.Models.BELAVMConstruction.Workbook.v3_0;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DheccExcelAddIn2013.Helpers.BELAVMConstruction;
using DheccExcelAddIn2013.Models;
using System.Windows.Forms;
using DheccExcelAddIn2013.Interfaces;

namespace DheccExcelAddIn2013.Helpers.BELAVMConstruction.Workbook.v3_0
{
    public static class ConstructionWorkbookDataHelper
    {
        private static IDheccContract iContract = new DheccContract();
        public static WorkbookDataModel Get()
        {
            return
                new WorkbookDataModel()
                {
                    DownloadBel = DownloadBEL.v3_0.DownloadBELHelper.Get(),
                    CalculationNotes = CalculationNotes.v3_0.CalculationNoteHelper.Get(),
                    Causation = Causation.v3_0.CausationHelper.Get(),
                    DeclarationRestated = DeclarationRestated.v3_0.DeclarationRestatedHelper.Get(),
                    Delcaration = Declaration.v3_0.DeclarationHelper.Get(),
                    Documentation = Documentation.v3_0.DocumentationHelper.Get(),
                    InputTab = InputA1.v3_0.InputA1Helper.Get(),
                    PnL = PnL.v3_0.PnLHelper.Get(),
                    PnLRestated = PnLRestated.v3_0.PnLRestatedHelper.Get(),
                    MethodologySelection = MethodologySelection.v3_0.MethodologySelectionHelper.Get(),
                    SeafoodRetailerTest = SeafoodRetailerTest.v3_0.SeafoodRetailerTestHelper.Get(),
                    TaxReconciliation = TaxReconsiliation.v3_0.TaxReconciliationHelper.Get(),
                    TaxReturnA3 = TaxReturn.v3_0.TaxReturnA3Helper.Get(),
                    TaxReturnSchE = TaxReturn.v3_0.TaxReturnSchEHelper.Get(),
                    TaxReturnForm8825 = TaxReturn.v3_0.TaxReturnForm8825Helper.Get(),
                    TaxVarianceA31 = TaxVariance.v3_0.TaxVarianceA31Helper.Get(),
                    TotalCalculatedLoss = TotalCalculatedLoss.v3_0.TotalCalculatedLossHelper.Get(),
                    VariableExpenses = VariableExpenses.v3_0.VariableExpensesHelper.Get(),
                    VariableExpensesRestated = VariableExpensesRestated.v3_0.VariableExpensesRestatedHelper.Get(),
                };
        }

        //public static bool OpenWorkbook(UploadModel uploadModel, bool belOnly, bool localCopy)
        //{
        //    try
        //    {
        //        Cursor.Current = Cursors.WaitCursor; 
        //        AppVariables.SetCurrentClaimantId(uploadModel.ClaimantId);
        //        AppVariables.SetCurrentClaimId(uploadModel.ClaimId);
        //        iContract.SetUploadModel(uploadModel);
        //        iContract.WorkbookOpenedAction();
        //        if (!WorkbookTemplate.OpenWorkbookTemplate(uploadModel.Workbook.WorkbookTypeId))
        //        {
        //            MessageBox.Show("An error occured while opening the workbook template: " + uploadModel.Workbook.Name);
        //            return false;
        //        }              
        //        Cursor.Current = Cursors.Arrow;
        //        return true;
        //    }
        //    catch
        //    {
        //        return false;
        //    }
        //}
    }
}
