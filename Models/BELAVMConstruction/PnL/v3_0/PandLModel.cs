﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DheccExcelAddIn2013.Models.BELAVMConstruction.PnL.v3_0
{
    public class PandLModel
    {  
        public List<string> Revenues { get; set; }
        public List<string> CostOfGoodsSold { get; set; }
        public List<string> GrossProfits { get; set; }
        public List<string> OtherIncomeLoss { get; set; }
        public List<string> OtherExpenses { get; set; }
        public List<string> Expenses { get; set; }
        public List<string> ForCalculationAboveSource { get; set; }
        public List<string> Source { get; set; }
        public List<string> Notes { get; set; }

        public PandLModel()
        {
            Revenues = new List<string>();
            CostOfGoodsSold = new List<string>();
            GrossProfits = new List<string>();
            OtherIncomeLoss = new List<string>();
            OtherExpenses = new List<string>();
            Source = new List<string>();
            Notes = new List<string>();
            Expenses = new List<string>();
            ForCalculationAboveSource = new List<string>();
        }
    }
}
