﻿namespace DheccExcelAddIn2013.UserControls
{
    partial class ucCompensationMethod
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbCompMethods = new System.Windows.Forms.GroupBox();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.ddlCompMethods = new System.Windows.Forms.ComboBox();
            this.lblClaimNo = new System.Windows.Forms.Label();
            this.lblClaimantNo = new System.Windows.Forms.Label();
            this.lblCompMeth = new System.Windows.Forms.Label();
            this.gbCompMethods.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbCompMethods
            // 
            this.gbCompMethods.Controls.Add(this.lblCompMeth);
            this.gbCompMethods.Controls.Add(this.btnUpdate);
            this.gbCompMethods.Controls.Add(this.label1);
            this.gbCompMethods.Controls.Add(this.ddlCompMethods);
            this.gbCompMethods.Controls.Add(this.lblClaimNo);
            this.gbCompMethods.Controls.Add(this.lblClaimantNo);
            this.gbCompMethods.Location = new System.Drawing.Point(25, 26);
            this.gbCompMethods.Name = "gbCompMethods";
            this.gbCompMethods.Size = new System.Drawing.Size(838, 488);
            this.gbCompMethods.TabIndex = 0;
            this.gbCompMethods.TabStop = false;
            this.gbCompMethods.Text = "Change Compensation Method";
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(284, 380);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(143, 51);
            this.btnUpdate.TabIndex = 5;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(61, 262);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(448, 32);
            this.label1.TabIndex = 4;
            this.label1.Text = "Available Compensation Methods:";
            // 
            // ddlCompMethods
            // 
            this.ddlCompMethods.FormattingEnabled = true;
            this.ddlCompMethods.Location = new System.Drawing.Point(55, 314);
            this.ddlCompMethods.Name = "ddlCompMethods";
            this.ddlCompMethods.Size = new System.Drawing.Size(701, 39);
            this.ddlCompMethods.TabIndex = 3;
            // 
            // lblClaimNo
            // 
            this.lblClaimNo.AutoSize = true;
            this.lblClaimNo.Location = new System.Drawing.Point(161, 130);
            this.lblClaimNo.Name = "lblClaimNo";
            this.lblClaimNo.Size = new System.Drawing.Size(111, 32);
            this.lblClaimNo.TabIndex = 2;
            this.lblClaimNo.Text = "Claim #";
            // 
            // lblClaimantNo
            // 
            this.lblClaimantNo.AutoSize = true;
            this.lblClaimantNo.Location = new System.Drawing.Point(121, 69);
            this.lblClaimantNo.Name = "lblClaimantNo";
            this.lblClaimantNo.Size = new System.Drawing.Size(151, 32);
            this.lblClaimantNo.TabIndex = 1;
            this.lblClaimantNo.Text = "Claimant #";
            // 
            // lblCompMeth
            // 
            this.lblCompMeth.AutoSize = true;
            this.lblCompMeth.Location = new System.Drawing.Point(61, 193);
            this.lblCompMeth.Name = "lblCompMeth";
            this.lblCompMeth.Size = new System.Drawing.Size(211, 32);
            this.lblCompMeth.TabIndex = 6;
            this.lblCompMeth.Text = "Current Method";
            // 
            // ucCompensationMethod
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gbCompMethods);
            this.Name = "ucCompensationMethod";
            this.Size = new System.Drawing.Size(900, 1174);
            this.gbCompMethods.ResumeLayout(false);
            this.gbCompMethods.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbCompMethods;
        private System.Windows.Forms.Label lblClaimantNo;
        private System.Windows.Forms.Label lblClaimNo;
        private System.Windows.Forms.ComboBox ddlCompMethods;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Label lblCompMeth;
    }
}
