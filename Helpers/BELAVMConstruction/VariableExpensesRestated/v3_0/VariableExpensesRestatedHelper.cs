﻿
using DheccExcelAddIn2013.Models.BELAVMConstruction.VariableExpensesRestated.v3_0;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DheccExcelAddIn2013.Helpers.BELAVMConstruction.VariableExpensesRestated.v3_0
{
    public static class VariableExpensesRestatedHelper
    {
        public static VariableExpensesRestatedModel Get()
        {
            Worksheet ws = Globals.ThisAddIn.Application.Worksheets["Var Expense Analysis (Restated)"];
            ws.Activate();
            VariableExpensesRestatedModel verm = new VariableExpensesRestatedModel();

            OLEObject obj = (OLEObject)ws.OLEObjects("CheckBox7");
            verm.YearsProvided2007 = obj.Object.Value;

            obj = (OLEObject)ws.OLEObjects("CheckBox2");            
            verm.YearsProvided2008 = obj.Object.Value;

            obj = (OLEObject)ws.OLEObjects("CheckBox3");            
            verm.YearsProvided2009 = obj.Object.Value;

            obj = (OLEObject)ws.OLEObjects("CheckBox4");            
            verm.YearsProvided2010 = obj.Object.Value;

            obj = (OLEObject)ws.OLEObjects("CheckBox5");            
            verm.YearsProvided2011 = obj.Object.Value;

            verm.LineItemsFlagged.Add(ExcelPaster.GetCellValue(ws, "K9"));
            verm.LineItemsFlagged.Add(ExcelPaster.GetCellValue(ws, "K11"));
            verm.LineItemsFlagged.Add(ExcelPaster.GetCellValue(ws, "K13"));
            verm.LineItemsFlagged.Add(ExcelPaster.GetCellValue(ws, "K15"));
            verm.LineItemsFlagged.Add(ExcelPaster.GetCellValue(ws, "K17"));
            verm.LineItemsFlagged.Add(ExcelPaster.GetCellValue(ws, "K19"));
            verm.LineItemsFlagged.Add(ExcelPaster.GetCellValue(ws, "K21"));
            verm.LineItemsFlagged.Add(ExcelPaster.GetCellValue(ws, "K23"));

            verm.Comments.Add(ExcelPaster.GetCellValue(ws, "M9"));
            verm.Comments.Add(ExcelPaster.GetCellValue(ws, "M11"));
            verm.Comments.Add(ExcelPaster.GetCellValue(ws, "M13"));
            verm.Comments.Add(ExcelPaster.GetCellValue(ws, "M15"));
            verm.Comments.Add(ExcelPaster.GetCellValue(ws, "M17"));
            verm.Comments.Add(ExcelPaster.GetCellValue(ws, "M19"));
            verm.Comments.Add(ExcelPaster.GetCellValue(ws, "M21"));
            verm.Comments.Add(ExcelPaster.GetCellValue(ws, "M23"));

            return verm;
        }

        public static void Set(VariableExpensesRestatedModel verm)
        {
            Worksheet ws = Globals.ThisAddIn.Application.Worksheets["Declaration Analysis (Source)"];
            ws.Activate();
            OLEObject obj = (OLEObject)ws.OLEObjects("CheckBox1");
            obj.Object.Value= verm.YearsProvided2007;

            obj = (OLEObject)ws.OLEObjects("CheckBox2");            
            obj.Object.Value= verm.YearsProvided2008;

            obj = (OLEObject)ws.OLEObjects("CheckBox3");            
            obj.Object.Value= verm.YearsProvided2009;

            obj = (OLEObject)ws.OLEObjects("CheckBox4");            
            obj.Object.Value= verm.YearsProvided2010;

            obj = (OLEObject)ws.OLEObjects("CheckBox5");            
            obj.Object.Value= verm.YearsProvided2011;

            ExcelPaster.SetCellValue(ws, "K9", verm.LineItemsFlagged[0]);
            ExcelPaster.SetCellValue(ws, "K11", verm.LineItemsFlagged[1]);
            ExcelPaster.SetCellValue(ws, "K13", verm.LineItemsFlagged[2]);
            ExcelPaster.SetCellValue(ws, "K15", verm.LineItemsFlagged[3]);
            ExcelPaster.SetCellValue(ws, "K17", verm.LineItemsFlagged[4]);
            ExcelPaster.SetCellValue(ws, "K19", verm.LineItemsFlagged[5]);
            ExcelPaster.SetCellValue(ws, "K21", verm.LineItemsFlagged[6]);
            ExcelPaster.SetCellValue(ws, "K23", verm.LineItemsFlagged[7]);

            ExcelPaster.SetCellValue(ws, "M9", verm.Comments[0]);
            ExcelPaster.SetCellValue(ws, "M11", verm.Comments[1]);
            ExcelPaster.SetCellValue(ws, "M13", verm.Comments[2]);
            ExcelPaster.SetCellValue(ws, "M15", verm.Comments[3]);
            ExcelPaster.SetCellValue(ws, "M17", verm.Comments[4]);
            ExcelPaster.SetCellValue(ws, "M19", verm.Comments[5]);
            ExcelPaster.SetCellValue(ws, "M21", verm.Comments[6]);
            ExcelPaster.SetCellValue(ws, "M23", verm.Comments[7]);

        }
    }
}
