﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DheccExcelAddIn2013.Models.BELAVMConstruction.InputA1.v3_0
{
    public class InputA1Model
    {
        public bool MultiFacilityClaim { get; set; }
        public string OptimumBenchmarkPeriod { get; set; }
        public string Step1Period { get; set; }
        public string Step2Period { get; set; }
        public bool OnlyBenchmarkPeriod2009 {get;set;}
        public bool BenchMarkPeriod20082009 { get; set; }
        public bool BenchMarkPeriod20072008 { get; set; }
        public List<ReviewerHistoryModel> ReviewerHistory { get; set; }
        public string ModelType { get; set; }
        public bool SelfManagedProperty { get; set; }
        public string ClaimantPropertyOwnershipInterest { get; set; }
        public List<PreviousPaymentDetailsModel> PreviousPaymentDetails { get; set; }
        public List<GenericList> InteralReviewComments { get; set; }
        public bool ShowProrationCalculator { get; set; }
        public InputA1Model()
        {
            ReviewerHistory = new List<ReviewerHistoryModel>();
            PreviousPaymentDetails = new List<PreviousPaymentDetailsModel>();
            InteralReviewComments = new List<GenericList>();
        }
    }
}
