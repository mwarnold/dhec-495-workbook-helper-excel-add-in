﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Outlook;

namespace DheccExcelAddIn2013.Helpers
{
    public static class EmailHelper
    {
        public static void SendExceptionEmail(string exception)
        {
            try
            {
                var app = new Microsoft.Office.Interop.Outlook.Application();
                Microsoft.Office.Interop.Outlook.MailItem mail = app.CreateItem(0);
                mail.To = "marnold@dheclaims.com";
                mail.Subject = "Exception thrown from add in";
                mail.Body = exception;
                app.CreateItem(0);
                mail.Send();
            }
            catch { }
        }
    }
}
