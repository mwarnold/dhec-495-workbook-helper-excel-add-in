﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;

namespace DheccExcelAddIn2013.Helpers.BELAVMConstruction.DownloadBEL.v3_0
{
    public static class DownloadBELHelper
    {
        public static List<string> Get()
        {
            Worksheet ws = Globals.ThisAddIn.GetDownloadSheet();
            ws.Activate();
            Range rng = ws.Range["C2", "C268"];
            Array ar = (Array)rng.Value2;
            List<string> dbList = new List<string>();
            foreach (var rv in ar)
                dbList.Add(ExcelPaster.FormatRangeValueToString(rv));
            return dbList;
        }

       //public static void Set(List<string> dbList)
       // {
       //     Worksheet ws = Globals.ThisAddIn.GetDownloadSheet();
       //     ws.Activate();
       //     Range rng = ws.Range["C2", "C268"];
       //     ExcelPaster.PasteVerticleSingleDimension(dbList, rng);
       // }
    }
}
