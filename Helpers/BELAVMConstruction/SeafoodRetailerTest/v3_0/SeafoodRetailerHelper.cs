﻿using DheccExcelAddIn2013.Models.BELAVMConstruction.SeafoodRetailerTest.v3_0;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using DheccExcelAddIn2013.Models;

namespace DheccExcelAddIn2013.Helpers.BELAVMConstruction.SeafoodRetailerTest.v3_0
{
    public static class SeafoodRetailerTestHelper
    {
        public static SeafoodRetailerTestModel Get()
        {
            SeafoodRetailerTestModel srtm = new SeafoodRetailerTestModel();
            Worksheet ws = Globals.ThisAddIn.Application.Worksheets["Seafood Retailer Test"];
            ws.Activate();
            srtm.SeafoodRetailerTestNotes = ExcelPaster.GetCellValue(ws, "B22");
            Range rng = ws.get_Range("B28", "C32");
           
            foreach (Range r in rng.Rows)
            {
                GenericList gl = new GenericList();
                gl.Items.Add(r.Cells[1, 1].Value2);
                gl.Items.Add(r.Cells[1, 2].Value2);
                srtm.DocumentationReferenced.Add(gl);                
            }

            rng = ws.get_Range("C38", "I42");
           
            foreach (Range r in rng.Rows)
            {
                GenericList gl = new GenericList();
                gl.Items.Add(r.Cells[1, 1].Value2);
                gl.Items.Add(Convert.ToString(r.Cells[1, 2].Value2));
                gl.Items.Add(r.Cells[1, 3].Value2);
                gl.Items.Add(r.Cells[1, 4].Value2);
                gl.Items.Add(r.Cells[1, 5].Value2);
                gl.Items.Add(r.Cells[1, 6].Value2);
                gl.Items.Add(r.Cells[1, 7].Value2);
                srtm.SeafoodVendorMixTest.Add(gl);              
            }

            srtm.TotalFoodSales2009 = ExcelPaster.GetCellValue(ws, "D46");

            return srtm;
        }

        public static void Set(SeafoodRetailerTestModel srtm)
        {
            Worksheet ws = Globals.ThisAddIn.Application.Worksheets["Seafood Retailer Test"];
            ws.Activate();
            ExcelPaster.SetCellValue(ws, "B22",srtm.SeafoodRetailerTestNotes);
            Range rng = ws.get_Range("B28", "C32");
            ExcelPaster.PasteTwoDimensionalList(srtm.DocumentationReferenced, rng);

            rng = ws.get_Range("C38", "I42");
            ExcelPaster.PasteTwoDimensionalList(srtm.SeafoodVendorMixTest, rng);

            ExcelPaster.SetCellValue(ws, "D46", srtm.TotalFoodSales2009);
            
        }
    }
}
