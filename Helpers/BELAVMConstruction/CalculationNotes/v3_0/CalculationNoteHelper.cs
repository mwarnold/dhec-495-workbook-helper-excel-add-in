﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using DheccExcelAddIn2013.Models.BELAVMConstruction.CalculationNotes.v3_0;
using DheccExcelAddIn2013.Models.BELAVMConstruction.Workbook.v3_0;

namespace DheccExcelAddIn2013.Helpers.BELAVMConstruction.CalculationNotes.v3_0
{
    public static class CalculationNoteHelper
    {
        public static List<CalculationNoteModel> Get()
        {
            List<CalculationNoteModel> objList = new List<CalculationNoteModel>();
            Worksheet ws = Globals.ThisAddIn.Application.Worksheets["Calculation Notes"];
            ws.Activate();
            Range rng = Globals.ThisAddIn.Application.get_Range("B4", "D500");
            int i = 0;
            foreach (Range r in rng.Rows)
            {
                i++;
                if (r.Cells[1, 2].Value2 == null)
                    break;
                var val = r.Cells[1, 2].Value2.ToString();
                if (string.IsNullOrEmpty(val))
                    break;
                objList.Add(
                    new CalculationNoteModel()
                    {
                        Id = val,
                        AppliesTo = r.Cells[1, 2].Value2,
                        Description = r.Cells[1, 3].Value2
                    });
            }
            return objList;
        }

        public static void Set(List<CalculationNoteModel> cnm)
        {
            Worksheet ws = Globals.ThisAddIn.Application.Worksheets["Calculation Notes"];
            ws.Activate();
            Range rng = Globals.ThisAddIn.Application.get_Range("B4", "D500");
            int i = 1;
            foreach (Range r in rng.Rows)
            {
                int iIndex = i - 1;
                if (i == cnm.Count)
                    return;
                var cn = cnm[iIndex];
                r.Cells[i, 1] = cn.Id;
                r.Cells[i, 2].Value2 = cn.AppliesTo;
                r.Cells[i, 3].Value2 = cn.Description;
                i++;
            }
        }
    }
}
