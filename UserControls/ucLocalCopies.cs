﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DheccExcelAddIn2013.Helpers;
using DheccExcelAddIn2013.Models;
using System.IO;
using DheccExcelAddIn2013.Interfaces;

namespace DheccExcelAddIn2013.UserControls
{
    public partial class ucLocalCopies : UserControl
    {
        protected class LocalCopiesGrid
        {
            public string ClaimantName { get; set; }
            public int ClaimantNo { get; set; }
            public int ClaimNo { get; set; }
        }

        //private static List<UploadModel> _localCopies = new List<UploadModel>();
        //LocalCopiesHelper _lch = new LocalCopiesHelper();
        //private IDheccContract iContract;
        //public ucLocalCopies()
        //{
        //    InitializeComponent();
        //    this.iContract = new DheccContract();
        //}

        public void RefreshControl()
        {
            //aReset.Visible = false;
            //_localCopies = _lch.GetLocalCopies().FindAll(x => x.WorkingCopy == true);
            //LoadUploads();
        }

        private void LoadUploads()
        {
            try
            {
                //List<LocalCopiesGrid> ugList = new List<LocalCopiesGrid>();
                //foreach (var result in _localCopies)
                //{
                //    ugList.Add(new LocalCopiesGrid()
                //    {
                //        ClaimantName = result.ClaimantName == null ? "" : result.ClaimantName,
                //        ClaimantNo = result.ClaimantId,
                //        ClaimNo = result.ClaimId,
                //    });
                //}
                //int width = ((dgLocalCopies.Width / 3) - 15);

                //dgLocalCopies.DataSource = ugList;
                //dgLocalCopies.Columns[0].Width = width;
                //dgLocalCopies.Columns[1].Width = width;
                //dgLocalCopies.Columns[2].Width = width;
            }
            catch (Exception ex)
            {
                WebServiceResponseHelper.ValidateResponse(0, "An unhandled exception has occured. Our dev team has been notifiied. We apologize for the inconvenience");
                EmailHelper.SendExceptionEmail(ex.ToString());
            }
        }

      
        private void btnSearch_Click(object sender, EventArgs e)
        {
            //aReset.Visible = true;
            //string searchText = tbSearch.Text;
            //_localCopies = _localCopies.FindAll(x => x.ClaimId.ToString() == searchText || x.ClaimantId.ToString() == searchText
            //    || x.DownloadBel[0].ToLower().Contains(searchText.ToLower()));
            //LoadUploads();
            //aReset.Visible = true;
        }

        private void aReset_Click(object sender, EventArgs e)
        {
            //_localCopies = _lch.GetLocalCopies();
            //LoadUploads();
            //aReset.Visible = false;
            //tbSearch.Text = "";
        }

        private void dgLocalCopies_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                //if (e.RowIndex < 0)
                //    return;
                //Cursor.Current = Cursors.WaitCursor;
                //DataGridViewRow dr = dgLocalCopies.Rows[e.RowIndex];
                //int claimId = Convert.ToInt32(dr.Cells[2].Value);
                //int claimantId = Convert.ToInt32(dr.Cells[1].Value);
                //var toLoad = _lch.GetLocalCopies().SingleOrDefault(x => x.ClaimantId == claimantId && x.ClaimId == claimId && x.WorkingCopy == true);
                //WorkbookTemplate.OpenLocalWorkbook(toLoad);
                //AppVariables.SetCurrentWorkbookTypeId(toLoad.Workbook.WorkbookTypeId);
                //if (toLoad.CompensationMethods.Count > 0)
                //{
                //    iContract.CompensationMethodButton().Enabled = true;
                //}
                //iContract.SetUploadModel(toLoad);
                //iContract.WorkbookOpenedAction();
                //Cursor.Current = Cursors.Arrow;
            }
            catch (Exception ex)
            {
                WebServiceResponseHelper.ValidateResponse(0, "An unhandled exception has occured. Our dev team has been notifiied. We apologize for the inconvenience");
                EmailHelper.SendExceptionEmail(ex.ToString());
            }
        }

    }
}
