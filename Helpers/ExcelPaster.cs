﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using DheccExcelAddIn2013.Models;

namespace DheccExcelAddIn2013.Helpers
{
    public static class ExcelPaster
    {
        public static object[,] GetRangeValuesToInsert(List<GenericList> genList)
        {
            int colCount = genList.FirstOrDefault().Items.Count;
            object[,] valRange = new object[genList.Count, colCount];
            int rc = 0;
            int rowCount = 0;
            foreach (var gl in genList)
            {
                foreach (var item in gl.Items)
                {
                    valRange[rowCount, rc] = item;
                    rc++;
                }
                rowCount++;
            }
            return valRange;
        }

        public static void PasteTwoDimensionalList(List<GenericList> genList, Range excelRange)
        {
            int colCount = genList.FirstOrDefault().Items.Count;
            object[,] valRange = new object[genList.Count, colCount];
            int rc = 0;
            int rowCount = 0;
            foreach (var gl in genList)
            {
                foreach (var item in gl.Items)
                {
                    valRange[rowCount, rc] = item;
                    rc++;
                }
                rowCount++;
            }
            excelRange.Value = valRange;
        }

        public static void PasteVerticleSingleDimension(List<string> values, Range excelRange)
        {
            object[,] valRange = new object[values.Count, 1];
            int i = 0;
            foreach (var val in values)
            {
                valRange[i, 0] = val;
                i++;
            }
            excelRange.Value = valRange;
        }

        public static void PasteHorizontalSingleDimension(List<string> values, Range excelRange)
        {
            object[,] valRange = new object[1, values.Count];
            int i = 0;
            foreach (var val in values)
            {
                valRange[0, i] = val;
                i++;
            }
            excelRange.Value = valRange;
        }

        public static string GetCellValue(Worksheet ws, string cell)
        {
            Range rng = ws.Range[cell, cell];
            try
            {
                if (string.IsNullOrEmpty(rng.Value2.ToString()))
                    return "";
                else
                    return rng.Value2;
            }
            catch(Exception ex)
            {
                return Convert.ToString(rng.Value2);
            }
        }

        public static string GetClaimOrClaimantNumber(Worksheet ws, string cell)
        {
            Range rng = ws.Range[cell, cell];
            try
            {
                if (string.IsNullOrEmpty(rng.Value2.ToString()))
                    return "";
                else
                {
                    string retValue = "";
                    int temp = Math.Round(Convert.ToDecimal(rng.Value2), 0);
                    retValue = temp.ToString();
                    return retValue;
                }
            }
            catch (Exception ex)
            {
                return Convert.ToString(rng.Value2);
            }
        }
        public static void SetCellValue(Worksheet ws, string cell, string cellValue)
        {
            Range rng = ws.Range[cell, cell];
            rng.Value2 = cellValue;
        }

        public static string FormatRangeValueToString(dynamic rv)
        {
            if (rv == null)
                return "";
            if (string.IsNullOrEmpty(Convert.ToString(rv)))
                return "";
            try
            {
                return Convert.ToString(rv);
            }
            catch(Exception ex )
            {
                return "";
            }
        }
    }
}
