﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DheccExcelAddIn2013.Models
{
    public class WorkbookModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Version { get; set; }
        public int WorkbookTypeId { get; set; }
        public DateTime DateAdded { get; set; }
        public bool Active { get; set; }
        public string FileName { get; set; }
        public string LocalFilePath { get; set; }
        public List<FormulaModel> Formulas { get; set; }
        public List<MacroModel> Macros { get; set; }
        public WorkbookModel()
        {
            Formulas = new List<FormulaModel>();
            Macros = new List<MacroModel>();
        }
    }
}
