﻿using DheccExcelAddIn2013.Interfaces;
using DheccExcelAddIn2013.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DheccExcelAddIn2013.Helpers
{
    class WIPHelper
    {
        private const string _fileName = "WIP.txt";
        private IDheccContract iContract;
        public WIPHelper()
        {
            DoCheck();
            this.iContract = new DheccContract();
        }
        public List<WorkbookWrapper> GetWIP()
        {
            String json = "";
            try
            {
                string fp = AppVariables.LocalDataDirectory();
                using (StreamReader sr = new StreamReader(fp + _fileName))
                    json = sr.ReadToEnd();
            }
            catch { }
            if (string.IsNullOrEmpty(json))
                return new List<WorkbookWrapper>();
            var wipList = (List<WorkbookWrapper>)JsonConvert.DeserializeObject<List<WorkbookWrapper>>(json);
            return wipList;
        }

        public bool SaveWIP(WorkbookWrapper ww)
        {
            try
            {
                List<WorkbookWrapper> wipList = GetWIP();
                if (wipList == null)
                    wipList = new List<WorkbookWrapper>();
                try
                {
                    //clean up list of any previous copies
                    wipList.RemoveAll(x => x.ClaimInfo.ClaimId == ww.ClaimInfo.ClaimId && x.ClaimInfo.ClaimantId == ww.ClaimInfo.ClaimantId && x.CanEdit == ww.CanEdit && 
                        x.IsImportedClaim == ww.IsImportedClaim && x.IsRemoteCopy == ww.IsImportedClaim);
                }
                catch { } //nothing to delete
                wipList.Add(ww);
                string fp = AppVariables.LocalDataDirectory();
                using (StreamWriter sw = new StreamWriter(fp + _fileName))
                {
                    sw.Write(JsonConvert.SerializeObject(wipList));
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private void DoCheck()
        {
            if (!Directory.Exists(AppVariables.RootExcelAddInFileDirectory()))
                Directory.CreateDirectory(AppVariables.RootExcelAddInFileDirectory());
            if (!Directory.Exists(AppVariables.LocalDataRootDirectory()))
                Directory.CreateDirectory(AppVariables.LocalDataRootDirectory());
        }

        public bool RemoveWIP(WorkbookWrapper ww)
        {
            try
            {
                List<WorkbookWrapper> wipList = GetWIP();
                wipList.RemoveAll(x => x.ClaimInfo.ClaimantId == ww.ClaimInfo.ClaimantId &&
                   x.ClaimInfo.ClaimId == ww.ClaimInfo.ClaimId);
                using (StreamWriter sw = new StreamWriter(AppVariables.LocalDataDirectory() + _fileName))
                {
                    sw.Write(JsonConvert.SerializeObject(wipList));
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}