﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DheccExcelAddIn2013.Models
{
    public class CompensationMethod
    {
        public int WorkbookTypeId { get; set; }
        public string CompCalcMethodDesc { get; set; }
        public int CompCalcMethodId { get; set; }
        public int ClaimId { get; set; }
    }
}
