﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using DheccExcelAddIn2013.Models.BELAVMConstruction.VariableExpenses.v3_0;

namespace DheccExcelAddIn2013.Helpers.BELAVMConstruction.VariableExpenses.v3_0
{
    public static class VariableExpensesHelper
    {
        public static VariableExpensesModel Get()
        {
            Worksheet ws = Globals.ThisAddIn.Application.Worksheets["Var Expense Analysis (Source)"];
            ws.Activate();
            VariableExpensesModel vem = new VariableExpensesModel();

            OLEObject obj = (OLEObject)ws.OLEObjects("CheckBox7");
            vem.YearsProvided2007 = obj.Object.Value;

            obj = (OLEObject)ws.OLEObjects("CheckBox2");            
            vem.YearsProvided2008 = obj.Object.Value;

            obj = (OLEObject)ws.OLEObjects("CheckBox3");            
            vem.YearsProvided2009 = obj.Object.Value;

            obj = (OLEObject)ws.OLEObjects("CheckBox4");            
            vem.YearsProvided2010 = obj.Object.Value;

            obj = (OLEObject)ws.OLEObjects("CheckBox5");            
            vem.YearsProvided2011 = obj.Object.Value;

            vem.LineItemsFlagged.Add(ExcelPaster.GetCellValue(ws, "K9"));
            vem.LineItemsFlagged.Add(ExcelPaster.GetCellValue(ws, "K11"));
            vem.LineItemsFlagged.Add(ExcelPaster.GetCellValue(ws, "K13"));
            vem.LineItemsFlagged.Add(ExcelPaster.GetCellValue(ws, "K15"));
            vem.LineItemsFlagged.Add(ExcelPaster.GetCellValue(ws, "K17"));
            vem.LineItemsFlagged.Add(ExcelPaster.GetCellValue(ws, "K19"));
            vem.LineItemsFlagged.Add(ExcelPaster.GetCellValue(ws, "K21"));
            vem.LineItemsFlagged.Add(ExcelPaster.GetCellValue(ws, "K23"));

            vem.Comments.Add(ExcelPaster.GetCellValue(ws, "M9"));
            vem.Comments.Add(ExcelPaster.GetCellValue(ws, "M11"));
            vem.Comments.Add(ExcelPaster.GetCellValue(ws, "M13"));
            vem.Comments.Add(ExcelPaster.GetCellValue(ws, "M15"));
            vem.Comments.Add(ExcelPaster.GetCellValue(ws, "M17"));
            vem.Comments.Add(ExcelPaster.GetCellValue(ws, "M19"));
            vem.Comments.Add(ExcelPaster.GetCellValue(ws, "M21"));
            vem.Comments.Add(ExcelPaster.GetCellValue(ws, "M23"));

            return vem;
        }

        public static void Set(VariableExpensesModel vem)
        {
            Worksheet ws = Globals.ThisAddIn.Application.Worksheets["Declaration Analysis (Source)"];
            ws.Activate();
            OLEObject obj = (OLEObject)ws.OLEObjects("CheckBox1");
            obj.Object.Value= vem.YearsProvided2007;

            obj = (OLEObject)ws.OLEObjects("CheckBox2");            
            obj.Object.Value= vem.YearsProvided2008;

            obj = (OLEObject)ws.OLEObjects("CheckBox3");            
            obj.Object.Value= vem.YearsProvided2009;

            obj = (OLEObject)ws.OLEObjects("CheckBox4");            
            obj.Object.Value= vem.YearsProvided2010;

            obj = (OLEObject)ws.OLEObjects("CheckBox5");            
            obj.Object.Value= vem.YearsProvided2011;

            ExcelPaster.SetCellValue(ws, "K9", vem.LineItemsFlagged[0]);
            ExcelPaster.SetCellValue(ws, "K11", vem.LineItemsFlagged[1]);
            ExcelPaster.SetCellValue(ws, "K13", vem.LineItemsFlagged[2]);
            ExcelPaster.SetCellValue(ws, "K15", vem.LineItemsFlagged[3]);
            ExcelPaster.SetCellValue(ws, "K17", vem.LineItemsFlagged[4]);
            ExcelPaster.SetCellValue(ws, "K19", vem.LineItemsFlagged[5]);
            ExcelPaster.SetCellValue(ws, "K21", vem.LineItemsFlagged[6]);
            ExcelPaster.SetCellValue(ws, "K23", vem.LineItemsFlagged[7]);

            ExcelPaster.SetCellValue(ws, "M9", vem.Comments[0]);
            ExcelPaster.SetCellValue(ws, "M11", vem.Comments[1]);
            ExcelPaster.SetCellValue(ws, "M13", vem.Comments[2]);
            ExcelPaster.SetCellValue(ws, "M15", vem.Comments[3]);
            ExcelPaster.SetCellValue(ws, "M17", vem.Comments[4]);
            ExcelPaster.SetCellValue(ws, "M19", vem.Comments[5]);
            ExcelPaster.SetCellValue(ws, "M21", vem.Comments[6]);
            ExcelPaster.SetCellValue(ws, "M23", vem.Comments[7]);

        }
    }
}
