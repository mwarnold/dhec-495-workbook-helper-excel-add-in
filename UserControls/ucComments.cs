﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DheccExcelAddIn2013.Interfaces;
using DheccExcelAddIn2013.Models;
using DheccExcelAddIn2013.Helpers;

namespace DheccExcelAddIn2013.UserControls
{
    public partial class ucComments : UserControl
    {
        private IDheccContract iContract;
        public ucComments()
        {
            this.iContract = new DheccContract();
            InitializeComponent();
        }
        public void RefreshControl()
        {
            tbNewComment.Text = "";
            LoadComments(true);
            btnAddNewComment.Enabled = iContract.CurrentWorkbookWrapper().CanEdit;
        }

        private void btnAddNewComment_Click(object sender, EventArgs e)
        {
            try
            {
                iContract.CurrentWorkbookWrapper().Comments.Add(
                    new CommentModel()
                    {
                        Date = DateTime.Now,
                        Text = tbNewComment.Text,
                        ClaimId = iContract.CurrentClaimId(),
                        ClaimantId = iContract.CurrentClaimantId()
                    });
                tbNewComment.Text = "";
                try
                {
                    LoadComments(false);
                }
                catch { }
            }
            catch (Exception ex)
            {
                WebServiceResponseHelper.ValidateResponse(0, "An unhandled exception has occured. Our dev team has been notifiied. We apologize for the inconvenience");
                EmailHelper.SendExceptionEmail(ex.ToString());
            }
        }

        private void LoadComments(bool isLoad)
        {
            try
            {
                if (iContract.CurrentClaimId() == 0)
                {
                    tbComments.Text = "No claim information was detected.";
                    btnAddNewComment.Enabled = false;
                    return;
                }
                btnAddNewComment.Enabled = true;
                var comments = iContract.CurrentWorkbookWrapper().Comments;
                if (isLoad && AppVariables.IsLoggedIn)
                {
                    var client = AppVariables.BGWebService();
                    //get remote comments and add to comments
                    var result = client.GetAcctReviewerCommentsList(AppVariables.CurrentUserId, AppVariables.AuthToken, iContract.CurrentClaimantId());
                    foreach (var c in result.AcctReviewerCommentsList)
                    {
                        comments.Add(
                            new CommentModel()
                            {
                                ClaimantId = c.ClaimantID,
                                Date = c.ReviewerCmmts_DateEntered,
                                Text = "(Portal) " + c.Comments,
                                UserName = iContract.AppSetting().User.UserName,
                            });
                    }
                    WorkbookTemplate.UpdateActiveWorkbookList(result.WorkbookInfoList);
                }                
                StringBuilder sb = new StringBuilder();
                foreach (var comment in comments.OrderByDescending(x => x.Date))
                    sb.Append(string.Format("{0} - {1}: {2} " + Environment.NewLine, comment.Date.ToShortDateString() + " " + comment.Date.ToShortTimeString(), 
                        iContract.AppSetting().User.UserName, comment.Text));
                tbComments.Text = sb.ToString();
            }
            catch (Exception ex)
            {
                WebServiceResponseHelper.ValidateResponse(0, "An unhandled exception has occured. Our dev team has been notifiied. We apologize for the inconvenience");
                EmailHelper.SendExceptionEmail(ex.ToString());
            }
        }
    }
}
