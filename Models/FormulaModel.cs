﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DheccExcelAddIn2013.Models
{
    public class FormulaModel
    {
        public int Id { get; set; }
        public int WorkbookId { get; set; }
        public DateTime DateAdded { get; set; }
        public String Description { get; set; }
        public string SheetName { get; set; }
        public string CellRef { get; set; }
        public String Formula { get; set; }
    }
}
