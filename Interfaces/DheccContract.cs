﻿using DheccExcelAddIn2013.Helpers;
using DheccExcelAddIn2013.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DheccExcelAddIn2013.UserControls;
using Microsoft.Office.Core;
using Microsoft.Office.Tools.Ribbon;

namespace DheccExcelAddIn2013.Interfaces
{
    public class DheccContract : IDheccContract
    {

        #region Main Area

        private static BackgroundWorker _backgroundUploadWorker;
        private static string _currentUserControlTitle = "Login";

        public BackgroundWorker BackgroundUploadWorker() { return _backgroundUploadWorker; }
        public void SetBackgroudUploadWorker(BackgroundWorker bgw) { _backgroundUploadWorker = bgw; }

        #endregion

        #region MyAccount

        private static string _version;
        private static string _currentAvailableVersion;
        private static AppSetting _appSetting;
        private static AppSettingHelper _appSettingsHelper;

        public void SetAppSettingsHelper(AppSettingHelper appsetting) { _appSettingsHelper = appsetting; }

        public AppSettingHelper AppSettingHelper() { return _appSettingsHelper; }


        public string Version() { return _appSetting.UserAddIn.Version; }

        public string CurrentAvailableVersion() { return _currentAvailableVersion; }
        public void SetCurrentAvailableVersion(string currentVersion) { _currentAvailableVersion = currentVersion; }

        public AppSetting AppSetting() { return _appSetting; }
        public void SetAppSetting(AppSetting appSetting) { _appSetting = appSetting; }

        #endregion

        #region Wrapper
        private static WorkbookWrapper _workbookWrapper = new WorkbookWrapper();
        public void SetCurrentWorkbookWrapper(WorkbookWrapper ww) { _workbookWrapper = ww; }
        public WorkbookWrapper CurrentWorkbookWrapper() { return _workbookWrapper; }

        public int CurrentClaimId() { return _workbookWrapper.ClaimInfo.ClaimId; }
        public int CurrentClaimantId() { return _workbookWrapper.ClaimInfo.ClaimantId; }

        public void SetUsersClaims(List<int> clmIds)
        {
            var settings = _appSettingsHelper.GetSettings();
            settings.MyClaims = clmIds;
            _appSettingsHelper.SaveSettings(settings);
        }
        public List<int> UsersClaims() { return _appSettingsHelper.GetSettings().MyClaims; }
        #endregion

        #region Workbook Actions


        public void ShowHideButtons()
        {
            if (Globals.ThisAddIn.RibbonMain != null)
                Globals.ThisAddIn.RibbonMain.Invalidate();
        }





        #endregion

    }
}
