﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using DheccExcelAddIn2013.Models.BELAVMConstruction.DeclarationRestated.v3_0;

namespace DheccExcelAddIn2013.Helpers.BELAVMConstruction.DeclarationRestated.v3_0
{
    public static class DeclarationRestatedHelper
    {
        public static DeclarationRestatedModel Get()
        {
            Worksheet ws = Globals.ThisAddIn.Application.Worksheets["Declaration Analysis (Restated)"];
            ws.Activate();
            DeclarationRestatedModel drm = new DeclarationRestatedModel();

            OLEObject obj = (OLEObject)ws.OLEObjects("CheckBox1");
            drm.YearsProvided2007 = obj.Object.Value;

            obj = (OLEObject)ws.OLEObjects("CheckBox2");
            drm.YearsProvided2008 = obj.Object.Value;

            obj = (OLEObject)ws.OLEObjects("CheckBox3");
            drm.YearsProvided2009 = obj.Object.Value;

            obj = (OLEObject)ws.OLEObjects("CheckBox4");
            drm.YearsProvided2010 = obj.Object.Value;

            obj = (OLEObject)ws.OLEObjects("CheckBox5");
            drm.YearsProvided2011 = obj.Object.Value;

            drm.LineItemsFlagged.Add(ExcelPaster.GetCellValue(ws, "K9"));
            drm.LineItemsFlagged.Add(ExcelPaster.GetCellValue(ws, "K11"));
            drm.LineItemsFlagged.Add(ExcelPaster.GetCellValue(ws, "K13"));
            drm.LineItemsFlagged.Add(ExcelPaster.GetCellValue(ws, "K15"));
            drm.LineItemsFlagged.Add(ExcelPaster.GetCellValue(ws, "K17"));
            drm.LineItemsFlagged.Add(ExcelPaster.GetCellValue(ws, "K19"));
            drm.LineItemsFlagged.Add(ExcelPaster.GetCellValue(ws, "K21"));
            drm.LineItemsFlagged.Add(ExcelPaster.GetCellValue(ws, "K23"));

            drm.Comments.Add(ExcelPaster.GetCellValue(ws, "M9"));
            drm.Comments.Add(ExcelPaster.GetCellValue(ws, "M11"));
            drm.Comments.Add(ExcelPaster.GetCellValue(ws, "M13"));
            drm.Comments.Add(ExcelPaster.GetCellValue(ws, "M15"));
            drm.Comments.Add(ExcelPaster.GetCellValue(ws, "M17"));
            drm.Comments.Add(ExcelPaster.GetCellValue(ws, "M19"));
            drm.Comments.Add(ExcelPaster.GetCellValue(ws, "M21"));
            drm.Comments.Add(ExcelPaster.GetCellValue(ws, "M23"));

            return drm;
        }
        public static void Set(DeclarationRestatedModel drm)
        {
            Worksheet ws = Globals.ThisAddIn.Application.Worksheets["Declaration Analysis (Source)"];
            ws.Activate();
            OLEObject obj = (OLEObject)ws.OLEObjects("CheckBox1");
            obj.Object.Value = drm.YearsProvided2007;

            obj = (OLEObject)ws.OLEObjects("CheckBox2");
            obj.Object.Value = drm.YearsProvided2008;

            obj = (OLEObject)ws.OLEObjects("CheckBox3");
            obj.Object.Value = drm.YearsProvided2009;

            obj = (OLEObject)ws.OLEObjects("CheckBox4");
            obj.Object.Value = drm.YearsProvided2010;

            obj = (OLEObject)ws.OLEObjects("CheckBox5");
            obj.Object.Value = drm.YearsProvided2011;

            ExcelPaster.SetCellValue(ws, "K9", drm.LineItemsFlagged[0]);
            ExcelPaster.SetCellValue(ws, "K11", drm.LineItemsFlagged[1]);
            ExcelPaster.SetCellValue(ws, "K13", drm.LineItemsFlagged[2]);
            ExcelPaster.SetCellValue(ws, "K15", drm.LineItemsFlagged[3]);
            ExcelPaster.SetCellValue(ws, "K17", drm.LineItemsFlagged[4]);
            ExcelPaster.SetCellValue(ws, "K19", drm.LineItemsFlagged[5]);
            ExcelPaster.SetCellValue(ws, "K21", drm.LineItemsFlagged[6]);
            ExcelPaster.SetCellValue(ws, "K23", drm.LineItemsFlagged[7]);

            ExcelPaster.SetCellValue(ws, "M9", drm.Comments[0]);
            ExcelPaster.SetCellValue(ws, "M11", drm.Comments[1]);
            ExcelPaster.SetCellValue(ws, "M13", drm.Comments[2]);
            ExcelPaster.SetCellValue(ws, "M15", drm.Comments[3]);
            ExcelPaster.SetCellValue(ws, "M17", drm.Comments[4]);
            ExcelPaster.SetCellValue(ws, "M19", drm.Comments[5]);
            ExcelPaster.SetCellValue(ws, "M21", drm.Comments[6]);
            ExcelPaster.SetCellValue(ws, "M23", drm.Comments[7]);

        }
    }
}
