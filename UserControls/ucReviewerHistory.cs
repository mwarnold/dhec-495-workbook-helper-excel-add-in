﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DheccExcelAddIn2013.Interfaces;
using DheccExcelAddIn2013.Helpers;
using Newtonsoft.Json.Linq;
using DheccExcelAddIn2013.Models;
using Newtonsoft.Json;
using System.IO;

namespace DheccExcelAddIn2013.UserControls
{
    public partial class ucReviewerHistory : UserControl
    {
        private IDheccContract iContract;
        public ucReviewerHistory()
        {
            this.iContract = new DheccContract();
            InitializeComponent();
        }

        public void RefreshControl()
        {
            this.tbReviewerHistory.Text = "";
            LoadHistory();
        }

        private void LoadHistory()
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                foreach (var rh in iContract.CurrentWorkbookWrapper().ReviewerHistory.OrderByDescending(x => x.Date))
                {
                    sb.Append(string.Format("{0}: {1} by {2}", rh.Date.ToShortDateString() + " " + rh.Date.ToShortTimeString(),  rh.Description,
                        rh.ReviewedBy) + Environment.NewLine + Environment.NewLine);
                };
                tbReviewerHistory.Text = sb.ToString();
            }
            catch (Exception ex)
            {
                WebServiceResponseHelper.ValidateResponse(0, "An unhandled exception has occured. Our dev team has been notifiied. We apologize for the inconvenience");
                EmailHelper.SendExceptionEmail(ex.ToString());
            }
        }


    }
}
