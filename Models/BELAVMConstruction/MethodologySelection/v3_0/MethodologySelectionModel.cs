﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DheccExcelAddIn2013.Models.BELAVMConstruction.MethodologySelection.v3_0
{
    public class MethodologySelectionModel
    {
        public string ProceedWith { get; set; }
        public List<string> CellsA17_21  { get; set; }
        public MethodologySelectionModel()
        {
            CellsA17_21 = new List<string>();
        }
    }
}
