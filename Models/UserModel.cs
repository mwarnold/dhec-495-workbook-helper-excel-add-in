﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DheccExcelAddIn2013.Models
{
    public class UserModel
    {
        public string UserName { get; set; }
        public List<RoleModel> Roles { get; set; }

        public UserModel()
        {
            Roles = new List<RoleModel>();
        }
    }
}
