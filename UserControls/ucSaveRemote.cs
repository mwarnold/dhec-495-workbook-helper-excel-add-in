﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DheccExcelAddIn2013.Interfaces;
using DheccExcelAddIn2013.Models;
using DheccExcelAddIn2013.Helpers;
using Newtonsoft.Json;
using System.Net;
using Newtonsoft.Json.Linq;
using System.IO;

namespace DheccExcelAddIn2013.UserControls
{
    public partial class ucSaveRemote : UserControl
    {
        protected class SaveStatus
        {
            public string Status { get; set; }
        }

        private static bool _isLoading = false;
        private IDheccContract iContract;
        private List<string> statusList = new List<string>();
        public ucSaveRemote()
        {
            InitializeComponent();
        }
        public void RefreshControl()
        {
            iContract = new DheccContract();
            var _currentWorkbookWrapper = iContract.CurrentWorkbookWrapper();
            Globals.ThisAddIn.SetActiveWorkbook();
        }

        private void btnUpload_Click(object sender, EventArgs e)
        {
            pbUpload.Value = 0;
            var _currentWorkbookWrapper = iContract.CurrentWorkbookWrapper();
            //Globals.ThisAddIn.SetActiveWorkbook();
            try
            {
                pbUpload.PerformStep();
                pbUpload.PerformStep();
                pbUpload.PerformStep();
                if (_currentWorkbookWrapper != null)
                {
                    _currentWorkbookWrapper.ReviewerHistory.Add
                   (new ReviewerHistoryModel()
                   {
                       Date = DateTime.Now,
                       Description = "Check In",
                       ReviewedBy = iContract.AppSetting().User.UserName,
                   });
                    pbUpload.PerformStep();
                    pbUpload.PerformStep();

                    try
                    {
                        var client = AppVariables.BGWebService();
                        com.browngreer.www3.clsSaveWorkbookInfo wbSave = new com.browngreer.www3.clsSaveWorkbookInfo();
                        var interimInfo = new com.browngreer.www3.clsInterimInfo();
                        var addInInfo = new com.browngreer.www3.clsAddIn();
                        addInInfo.Active = iContract.AppSetting().UserAddIn.Active;
                        addInInfo.AddInID = iContract.AppSetting().UserAddIn.Id;
                        addInInfo.Description = iContract.AppSetting().UserAddIn.Description;
                        addInInfo.Version = iContract.AppSetting().UserAddIn.Version;
                        interimInfo.AddIn = addInInfo;
                        interimInfo.AuthToken = AppVariables.AuthToken;
                        interimInfo.ClaimantID = _currentWorkbookWrapper.ClaimInfo.ClaimantId;
                        interimInfo.ClaimID = _currentWorkbookWrapper.ClaimInfo.ClaimId;
                        interimInfo.ReviewQueueTypeID = Convert.ToInt16(_currentWorkbookWrapper.ReviewQueueTypeId);
                        string commitMessage = DateTime.Now.ToShortDateString() + DateTime.Now.ToShortTimeString() + " - " + tbMessage.Text;
                        interimInfo.CommitMessage = string.Format("Uploaded by: {0} {1} Claimant Name: {2} {3} Claimant# {4} {5} Claim# {6}",
                          iContract.AppSetting().User.UserName, Environment.NewLine, _currentWorkbookWrapper.ClaimInfo.ClaimantName + " #" +
                          _currentWorkbookWrapper.ClaimInfo.ClaimantId, Environment.NewLine + Environment.NewLine, _currentWorkbookWrapper.ClaimInfo.ClaimId,
                          Environment.NewLine + Environment.NewLine, commitMessage);
                        interimInfo.CompMethodID = Convert.ToInt16(_currentWorkbookWrapper.CompensationMethodId);
                        interimInfo.Date = DateTime.Now;
                        interimInfo.IsFinal = false;
                        interimInfo.UserID = AppVariables.CurrentUserId;
                        interimInfo.WBInfoID = _currentWorkbookWrapper.Workbook.Id;
                        wbSave.InterimInfo = interimInfo;

                        wbSave.FileStream_Excel = WorkbookTemplate.SaveWorkbookForUploadAndGetStream();
                        wbSave.FileName_Excel = Path.GetFileName(_currentWorkbookWrapper.Workbook.LocalFilePath);
                        wbSave.FileExtension_Excel = Path.GetExtension(_currentWorkbookWrapper.Workbook.LocalFilePath);

                        var result = client.SaveWorkbookInfo(wbSave);

                        //now upload the response id                        
                        _currentWorkbookWrapper.SaveHistId = result.SaveHistID;
                        _currentWorkbookWrapper.LastSaveDate = DateTime.Now;
                        interimInfo.ClaimData = JsonConvert.SerializeObject(_currentWorkbookWrapper);
                        wbSave.InterimInfo = interimInfo;
                        wbSave.FileExtension_Excel = null;
                        wbSave.FileName_Excel = null;
                        wbSave.FileStream_Excel = null;
                        var result2 = client.SaveWorkbookInfo(wbSave);

                        pbUpload.PerformStep();
                        pbUpload.PerformStep();
                        pbUpload.PerformStep();
                        pbUpload.PerformStep();
                        pbUpload.PerformStep();
                        pbUpload.PerformStep();

                        var wh = new WIPHelper();
                        //now save local copy
                        _currentWorkbookWrapper.LastSaveDate = DateTime.Now;
                        wh.SaveWIP(_currentWorkbookWrapper);

                        if (result != null && result2 != null)
                        {
                            if (!WebServiceResponseHelper.ValidateResponse(result.Result.ResultID, result.Result.ResultMessage))
                                return;
                            if (!WebServiceResponseHelper.ValidateResponse(result2.Result.ResultID, result2.Result.ResultMessage))
                                return;
                            WorkbookTemplate.UpdateActiveWorkbookList(result.WorkbookInfoList);
                            tbMessage.Text = "";
                            MessageBox.Show("Successfully Checked In.");
                        }
                        else
                            MessageBox.Show("Check In Failed. A message has been sent to our development team.");
                    }
                    catch (Exception ex)
                    {
                        pbUpload.Value = 0;
                        MessageBox.Show("Error: " + ex.ToString());
                        return;
                    }
                    WorkbookTemplate.DeleteTempSaveFile();
                }
            }
            catch (Exception ex)
            {
                WebServiceResponseHelper.ValidateResponse(0, "An unhandled exception has occured. Our dev team has been notifiied. We apologize for the inconvenience");
                EmailHelper.SendExceptionEmail(ex.ToString());
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Globals.ThisAddIn.Application.Run("TestMacro");
        }

    }
}
