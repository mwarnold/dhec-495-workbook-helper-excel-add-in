﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DheccExcelAddIn2013.Models.BELAVMConstruction.TaxReconciliation.v3_0
{
    public class TaxReconciliationModel
    {
        public List<GenericList> AdjustmentsForReconciliation { get; set; }
        public List<string> TaxVarianceComments { get; set; }
        public List<string> TaxReconComments { get; set; }

        public TaxReconciliationModel()
        {
            TaxVarianceComments = new List<string>();
            TaxReconComments = new List<string>();
            AdjustmentsForReconciliation = new List<GenericList>();
        }
    }
}
