﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DheccExcelAddIn2013.Models.BELAVMConstruction.TaxReturn.v3_0
{
    public class TaxReturnSchEModel
    {
        public List<string> PhysicalAddress { get; set; }
        public List<string> UsedByFamily { get; set; }
        public List<string> RentsReceived { get; set; }
        public List<string> RoyaltiesReceived { get; set; }
        public List<string> Advertising { get; set; }
        public List<string> AutoAndTravel { get; set; }
        public List<string> CleaningAndMaintenance { get; set; }
        public List<string> Commissions { get; set; }
        public List<string> Insurance { get; set; }
        public List<string> LegalAndOtherProfessionalFees { get; set; }
        public List<string> ManagementFees { get; set; }
        public List<string> MortgageInterestPaidToBanksEtc { get; set; }
        public List<string> OtherInterest { get; set; }
        public List<string> Repairs { get; set; }
        public List<string> Supplies { get; set; }
        public List<string> Taxes { get; set; }
        public List<string> Utilities { get; set; }
        public List<GenericList> Other { get; set; }
        public List<string> DepreciationExpensesOrDepletion { get; set; }
        public List<string> DeductibleRentalRealEstateLoss { get; set; }

        public TaxReturnSchEModel()
        {
            PhysicalAddress = new List<string>();
            UsedByFamily = new List<string>();
            RentsReceived = new List<string>();
            RoyaltiesReceived = new List<string>();
            Advertising = new List<string>();
            AutoAndTravel = new List<string>();
            CleaningAndMaintenance = new List<string>();
            Commissions = new List<string>();
            Insurance = new List<string>();
            LegalAndOtherProfessionalFees = new List<string>();
            ManagementFees = new List<string>();
            MortgageInterestPaidToBanksEtc = new List<string>();
            Repairs = new List<string>();
            Supplies = new List<string>();
            OtherInterest = new List<string>();
            Taxes = new List<string>();
            Utilities = new List<string>();
            Other = new List<GenericList>();
            DepreciationExpensesOrDepletion = new List<string>();
            DeductibleRentalRealEstateLoss = new List<string>();
        }
    }
}
