﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace DheccExcelAddIn2013.Encryption
{
    public static class MAesHelper
    {
        public static bool EncryptFile(byte[] toEncrypt, string outName, string pw, string salt)
        {
            try
            {
                FileStream fout = new FileStream(outName, FileMode.OpenOrCreate, FileAccess.Write);
                fout.SetLength(0);
                byte[] saltBytes = System.Text.Encoding.UTF8.GetBytes(salt);
                var keyDerivationFunction = new Rfc2898DeriveBytes(pw, saltBytes);
                var keyBytes = keyDerivationFunction.GetBytes(32);
                var ivBytes = keyDerivationFunction.GetBytes(16);
                string keyString = System.Text.Encoding.UTF8.GetString(keyBytes);
                string ivString = System.Text.Encoding.UTF8.GetString(ivBytes);

                SymmetricAlgorithm rijn = SymmetricAlgorithm.Create();
                CryptoStream cryptostreameEncr = new CryptoStream(fout, rijn.CreateEncryptor(keyBytes, ivBytes), CryptoStreamMode.Write);

                cryptostreameEncr.Write(toEncrypt, 0, toEncrypt.Length);
                cryptostreameEncr.Flush();
                cryptostreameEncr.Close();
                return true;
            }
            catch(Exception ex)
            {
                DheccExcelAddIn2013.Helpers.EmailHelper.SendExceptionEmail(ex.ToString());
                return false;
            }
        }

        public static DecryptionModel DecryptFile(string inName, string pw, string salt)
        {
            FileStream fin = new FileStream(inName, FileMode.Open, FileAccess.Read);
            DecryptionModel dm = new DecryptionModel();
            using (var md5 = MD5.Create())
            {
                dm.Md5Value = System.BitConverter.ToString(md5.ComputeHash(fin)).Replace("-", ""); ;
            }
            fin.Flush();
            fin.Close();
            fin = new FileStream(inName, FileMode.Open, FileAccess.Read);
            byte[] saltBytes = System.Text.Encoding.UTF8.GetBytes(salt);

            var keyDerivationFunction = new Rfc2898DeriveBytes(pw, saltBytes);
            var keyBytes = keyDerivationFunction.GetBytes(32);
            var ivBytes = keyDerivationFunction.GetBytes(16);
            SymmetricAlgorithm rijn = SymmetricAlgorithm.Create();
            CryptoStream cryptostreamDecr = new CryptoStream(fin, rijn.CreateDecryptor(keyBytes, ivBytes), CryptoStreamMode.Read);
            byte[] decryBytes;
            using (BinaryReader brDecrypt = new BinaryReader(cryptostreamDecr))
            {
                decryBytes = ReadAllBytes(brDecrypt);
            }
            dm.DecryptedBytes = decryBytes;
            return dm;
        }

        public static byte[] ReadAllBytes(this BinaryReader reader)
        {
            const int bufferSize = 4096;
            using (var ms = new MemoryStream())
            {
                byte[] buffer = new byte[bufferSize];
                int count;
                while ((count = reader.Read(buffer, 0, buffer.Length)) != 0)
                    ms.Write(buffer, 0, count);
                return ms.ToArray();
            }

        }
    }
}