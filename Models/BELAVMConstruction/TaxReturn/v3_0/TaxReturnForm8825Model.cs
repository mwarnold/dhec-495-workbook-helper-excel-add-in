﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DheccExcelAddIn2013.Models.BELAVMConstruction.TaxReturn.v3_0
{
    public class TaxReturnForm8825Model
    {
        public List<string> PhysicalAddress { get; set; }
        public List<string> GrossRents { get; set; }
        public List<string> Advertising { get; set; }
        public List<string> AutoAndTravel { get; set; }
        public List<string> CleaningAndMaintenance { get; set; }
        public List<string> Commissions { get; set; }
        public List<string> Insurance { get; set; }
        public List<string> LegalAndOtherProfessionalFees { get; set; }
        public List<string> Interest { get; set; }
        public List<string> Repairs { get; set; }
        public List<string> Taxes { get; set; }
        public List<string> Utilities { get; set; }
        public List<string> WagesAndSalaries { get; set; }
        public List<string> Depreciation { get; set; }
        public List<GenericList> Other { get; set; }

        public TaxReturnForm8825Model()
        {
            PhysicalAddress = new List<string>();
            GrossRents = new List<string>();
            Advertising = new List<string>();
            AutoAndTravel = new List<string>();
            CleaningAndMaintenance = new List<string>();
            Commissions = new List<string>();
            Insurance = new List<string>();
            LegalAndOtherProfessionalFees = new List<string>();
            Interest = new List<string>();
            Repairs = new List<string>();
            Utilities = new List<string>();
            WagesAndSalaries = new List<string>();
            Depreciation = new List<string>();
            Other = new List<GenericList>();
            Taxes = new List<string>();
        }
    }
}
