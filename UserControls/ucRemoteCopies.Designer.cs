﻿namespace DheccExcelAddIn2013.UserControls
{
    partial class ucRemoteCopies
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlSearch = new System.Windows.Forms.Panel();
            this.gbSearch = new System.Windows.Forms.GroupBox();
            this.cbMyClaims = new System.Windows.Forms.CheckBox();
            this.gbUploadDates = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpEndDate = new System.Windows.Forms.DateTimePicker();
            this.lblUploadDatestart = new System.Windows.Forms.Label();
            this.dtpStartDate = new System.Windows.Forms.DateTimePicker();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.tbClaimant = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbClaim = new System.Windows.Forms.TextBox();
            this.lblSearchText1 = new System.Windows.Forms.Label();
            this.dgRemoteCopies = new System.Windows.Forms.DataGridView();
            this.pnlGrid = new System.Windows.Forms.Panel();
            this.pnlUploadInfo = new System.Windows.Forms.Panel();
            this.gbUploadDetails = new System.Windows.Forms.GroupBox();
            this.btnLoadClaim = new System.Windows.Forms.Button();
            this.tbCommitMessage = new System.Windows.Forms.TextBox();
            this.lblDateValue = new System.Windows.Forms.Label();
            this.lblDateText = new System.Windows.Forms.Label();
            this.pnlSearch.SuspendLayout();
            this.gbSearch.SuspendLayout();
            this.gbUploadDates.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgRemoteCopies)).BeginInit();
            this.pnlGrid.SuspendLayout();
            this.pnlUploadInfo.SuspendLayout();
            this.gbUploadDetails.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlSearch
            // 
            this.pnlSearch.Controls.Add(this.gbSearch);
            this.pnlSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlSearch.Location = new System.Drawing.Point(0, 0);
            this.pnlSearch.Name = "pnlSearch";
            this.pnlSearch.Size = new System.Drawing.Size(900, 258);
            this.pnlSearch.TabIndex = 34;
            // 
            // gbSearch
            // 
            this.gbSearch.Controls.Add(this.cbMyClaims);
            this.gbSearch.Controls.Add(this.gbUploadDates);
            this.gbSearch.Controls.Add(this.btnReset);
            this.gbSearch.Controls.Add(this.btnSearch);
            this.gbSearch.Controls.Add(this.tbClaimant);
            this.gbSearch.Controls.Add(this.label1);
            this.gbSearch.Controls.Add(this.tbClaim);
            this.gbSearch.Controls.Add(this.lblSearchText1);
            this.gbSearch.Location = new System.Drawing.Point(47, 8);
            this.gbSearch.Name = "gbSearch";
            this.gbSearch.Size = new System.Drawing.Size(806, 243);
            this.gbSearch.TabIndex = 35;
            this.gbSearch.TabStop = false;
            this.gbSearch.Text = "Search Claims";
            // 
            // cbMyClaims
            // 
            this.cbMyClaims.AutoSize = true;
            this.cbMyClaims.Checked = true;
            this.cbMyClaims.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbMyClaims.Location = new System.Drawing.Point(449, 201);
            this.cbMyClaims.Name = "cbMyClaims";
            this.cbMyClaims.Size = new System.Drawing.Size(316, 36);
            this.cbMyClaims.TabIndex = 45;
            this.cbMyClaims.Text = "Show only my claims";
            this.cbMyClaims.UseVisualStyleBackColor = true;
            // 
            // gbUploadDates
            // 
            this.gbUploadDates.Controls.Add(this.label2);
            this.gbUploadDates.Controls.Add(this.dtpEndDate);
            this.gbUploadDates.Controls.Add(this.lblUploadDatestart);
            this.gbUploadDates.Controls.Add(this.dtpStartDate);
            this.gbUploadDates.Location = new System.Drawing.Point(419, 24);
            this.gbUploadDates.Name = "gbUploadDates";
            this.gbUploadDates.Size = new System.Drawing.Size(334, 162);
            this.gbUploadDates.TabIndex = 44;
            this.gbUploadDates.TabStop = false;
            this.gbUploadDates.Text = "Date Uploaded";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(56, 106);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 32);
            this.label2.TabIndex = 51;
            this.label2.Tag = "";
            this.label2.Text = "To";
            // 
            // dtpEndDate
            // 
            this.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpEndDate.Location = new System.Drawing.Point(110, 106);
            this.dtpEndDate.Name = "dtpEndDate";
            this.dtpEndDate.Size = new System.Drawing.Size(200, 38);
            this.dtpEndDate.TabIndex = 50;
            // 
            // lblUploadDatestart
            // 
            this.lblUploadDatestart.AutoSize = true;
            this.lblUploadDatestart.Location = new System.Drawing.Point(24, 50);
            this.lblUploadDatestart.Name = "lblUploadDatestart";
            this.lblUploadDatestart.Size = new System.Drawing.Size(80, 32);
            this.lblUploadDatestart.TabIndex = 49;
            this.lblUploadDatestart.Tag = "";
            this.lblUploadDatestart.Text = "From";
            // 
            // dtpStartDate
            // 
            this.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpStartDate.Location = new System.Drawing.Point(110, 48);
            this.dtpStartDate.Name = "dtpStartDate";
            this.dtpStartDate.Size = new System.Drawing.Size(200, 38);
            this.dtpStartDate.TabIndex = 48;
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(241, 171);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(107, 44);
            this.btnReset.TabIndex = 43;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(60, 171);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(135, 44);
            this.btnSearch.TabIndex = 39;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // tbClaimant
            // 
            this.tbClaimant.Location = new System.Drawing.Point(186, 111);
            this.tbClaimant.Name = "tbClaimant";
            this.tbClaimant.Size = new System.Drawing.Size(190, 38);
            this.tbClaimant.TabIndex = 38;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 114);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(151, 32);
            this.label1.TabIndex = 42;
            this.label1.Tag = "";
            this.label1.Text = "Claimant #";
            // 
            // tbClaim
            // 
            this.tbClaim.Location = new System.Drawing.Point(186, 57);
            this.tbClaim.Name = "tbClaim";
            this.tbClaim.Size = new System.Drawing.Size(190, 38);
            this.tbClaim.TabIndex = 37;
            // 
            // lblSearchText1
            // 
            this.lblSearchText1.AutoSize = true;
            this.lblSearchText1.Location = new System.Drawing.Point(54, 57);
            this.lblSearchText1.Name = "lblSearchText1";
            this.lblSearchText1.Size = new System.Drawing.Size(111, 32);
            this.lblSearchText1.TabIndex = 41;
            this.lblSearchText1.Tag = "";
            this.lblSearchText1.Text = "Claim #";
            // 
            // dgRemoteCopies
            // 
            this.dgRemoteCopies.AllowUserToAddRows = false;
            this.dgRemoteCopies.AllowUserToDeleteRows = false;
            this.dgRemoteCopies.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgRemoteCopies.Location = new System.Drawing.Point(47, 22);
            this.dgRemoteCopies.Name = "dgRemoteCopies";
            this.dgRemoteCopies.ReadOnly = true;
            this.dgRemoteCopies.RowTemplate.Height = 40;
            this.dgRemoteCopies.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgRemoteCopies.Size = new System.Drawing.Size(806, 414);
            this.dgRemoteCopies.TabIndex = 33;
            this.dgRemoteCopies.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgRemoteCopies_CellContentClick);
            this.dgRemoteCopies.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgRemoteCopies_DataBindingComplete);
            // 
            // pnlGrid
            // 
            this.pnlGrid.AutoScroll = true;
            this.pnlGrid.Controls.Add(this.pnlUploadInfo);
            this.pnlGrid.Controls.Add(this.dgRemoteCopies);
            this.pnlGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlGrid.Location = new System.Drawing.Point(0, 258);
            this.pnlGrid.Name = "pnlGrid";
            this.pnlGrid.Size = new System.Drawing.Size(900, 916);
            this.pnlGrid.TabIndex = 36;
            // 
            // pnlUploadInfo
            // 
            this.pnlUploadInfo.Controls.Add(this.gbUploadDetails);
            this.pnlUploadInfo.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlUploadInfo.Location = new System.Drawing.Point(0, 461);
            this.pnlUploadInfo.Name = "pnlUploadInfo";
            this.pnlUploadInfo.Size = new System.Drawing.Size(900, 455);
            this.pnlUploadInfo.TabIndex = 36;
            // 
            // gbUploadDetails
            // 
            this.gbUploadDetails.Controls.Add(this.btnLoadClaim);
            this.gbUploadDetails.Controls.Add(this.tbCommitMessage);
            this.gbUploadDetails.Controls.Add(this.lblDateValue);
            this.gbUploadDetails.Controls.Add(this.lblDateText);
            this.gbUploadDetails.Location = new System.Drawing.Point(47, 25);
            this.gbUploadDetails.Name = "gbUploadDetails";
            this.gbUploadDetails.Size = new System.Drawing.Size(806, 404);
            this.gbUploadDetails.TabIndex = 34;
            this.gbUploadDetails.TabStop = false;
            this.gbUploadDetails.Text = "Upload Details";
            // 
            // btnLoadClaim
            // 
            this.btnLoadClaim.Location = new System.Drawing.Point(226, 336);
            this.btnLoadClaim.Name = "btnLoadClaim";
            this.btnLoadClaim.Size = new System.Drawing.Size(342, 51);
            this.btnLoadClaim.TabIndex = 7;
            this.btnLoadClaim.Text = "Load Selected Claim";
            this.btnLoadClaim.UseVisualStyleBackColor = true;
            this.btnLoadClaim.Click += new System.EventHandler(this.btnLoadClaim_Click_1);
            // 
            // tbCommitMessage
            // 
            this.tbCommitMessage.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.tbCommitMessage.Location = new System.Drawing.Point(39, 102);
            this.tbCommitMessage.Multiline = true;
            this.tbCommitMessage.Name = "tbCommitMessage";
            this.tbCommitMessage.ReadOnly = true;
            this.tbCommitMessage.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbCommitMessage.Size = new System.Drawing.Size(714, 218);
            this.tbCommitMessage.TabIndex = 50;
            // 
            // lblDateValue
            // 
            this.lblDateValue.AutoSize = true;
            this.lblDateValue.Location = new System.Drawing.Point(220, 46);
            this.lblDateValue.Name = "lblDateValue";
            this.lblDateValue.Size = new System.Drawing.Size(301, 32);
            this.lblDateValue.TabIndex = 50;
            this.lblDateValue.Text = "This is the upload date";
            // 
            // lblDateText
            // 
            this.lblDateText.AutoSize = true;
            this.lblDateText.Location = new System.Drawing.Point(39, 43);
            this.lblDateText.Name = "lblDateText";
            this.lblDateText.Size = new System.Drawing.Size(181, 32);
            this.lblDateText.TabIndex = 50;
            this.lblDateText.Text = "Upload Date:";
            // 
            // ucRemoteCopies
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Controls.Add(this.pnlGrid);
            this.Controls.Add(this.pnlSearch);
            this.Name = "ucRemoteCopies";
            this.Size = new System.Drawing.Size(900, 1174);
            this.Load += new System.EventHandler(this.ucRemoteCopies_Load);
            this.pnlSearch.ResumeLayout(false);
            this.gbSearch.ResumeLayout(false);
            this.gbSearch.PerformLayout();
            this.gbUploadDates.ResumeLayout(false);
            this.gbUploadDates.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgRemoteCopies)).EndInit();
            this.pnlGrid.ResumeLayout(false);
            this.pnlUploadInfo.ResumeLayout(false);
            this.gbUploadDetails.ResumeLayout(false);
            this.gbUploadDetails.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlSearch;
        private System.Windows.Forms.GroupBox gbSearch;
        private System.Windows.Forms.GroupBox gbUploadDates;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpEndDate;
        private System.Windows.Forms.Label lblUploadDatestart;
        private System.Windows.Forms.DateTimePicker dtpStartDate;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TextBox tbClaimant;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbClaim;
        private System.Windows.Forms.Label lblSearchText1;
        private System.Windows.Forms.DataGridView dgRemoteCopies;
        private System.Windows.Forms.Panel pnlGrid;
        private System.Windows.Forms.Panel pnlUploadInfo;
        private System.Windows.Forms.GroupBox gbUploadDetails;
        private System.Windows.Forms.Button btnLoadClaim;
        private System.Windows.Forms.TextBox tbCommitMessage;
        private System.Windows.Forms.Label lblDateValue;
        private System.Windows.Forms.Label lblDateText;
        private System.Windows.Forms.CheckBox cbMyClaims;
    }
}
