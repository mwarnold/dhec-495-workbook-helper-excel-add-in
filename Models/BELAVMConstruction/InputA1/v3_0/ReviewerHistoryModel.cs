﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DheccExcelAddIn2013.Models.BELAVMConstruction.InputA1.v3_0
{
    public class ReviewerHistoryModel
    {
        public string ReviewerName { get; set; }
        public string DateReviewed { get; set; }
        public string ReviewType { get; set; }
    }
}
