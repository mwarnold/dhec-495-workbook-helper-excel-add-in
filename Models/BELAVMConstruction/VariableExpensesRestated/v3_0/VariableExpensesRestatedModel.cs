﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DheccExcelAddIn2013.Models.BELAVMConstruction.VariableExpensesRestated.v3_0
{
    public class VariableExpensesRestatedModel
    {
        public bool YearsProvided2007 { get; set; }
        public bool YearsProvided2008 { get; set; }
        public bool YearsProvided2009 { get; set; }
        public bool YearsProvided2010 { get; set; }
        public bool YearsProvided2011 { get; set; }
        public List<string> LineItemsFlagged { get; set; }
        public List<string> Comments { get; set; }

        public VariableExpensesRestatedModel()
        {
            LineItemsFlagged = new List<string>();
            Comments = new List<string>();
        }
    }
}
