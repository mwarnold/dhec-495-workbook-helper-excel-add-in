﻿namespace DheccExcelAddIn2013.UserControls
{
    partial class ucReviewerHistory
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Date = new System.Windows.Forms.DataGridViewLinkColumn();
            this.ReviewedBy = new System.Windows.Forms.DataGridViewLinkColumn();
            this.btnLoad = new System.Windows.Forms.DataGridViewButtonColumn();
            this.ClaimantId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClaimId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tbReviewerHistory = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // Date
            // 
            this.Date.HeaderText = "Date";
            this.Date.Name = "Date";
            this.Date.ReadOnly = true;
            this.Date.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Date.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // ReviewedBy
            // 
            this.ReviewedBy.HeaderText = "Reviewer";
            this.ReviewedBy.Name = "ReviewedBy";
            this.ReviewedBy.ReadOnly = true;
            this.ReviewedBy.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ReviewedBy.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // btnLoad
            // 
            this.btnLoad.HeaderText = "Open";
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.ReadOnly = true;
            // 
            // ClaimantId
            // 
            this.ClaimantId.HeaderText = "ClaimantId";
            this.ClaimantId.Name = "ClaimantId";
            this.ClaimantId.ReadOnly = true;
            this.ClaimantId.Visible = false;
            // 
            // ClaimId
            // 
            this.ClaimId.HeaderText = "ClaimId";
            this.ClaimId.Name = "ClaimId";
            this.ClaimId.ReadOnly = true;
            this.ClaimId.Visible = false;
            // 
            // tbReviewerHistory
            // 
            this.tbReviewerHistory.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.tbReviewerHistory.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbReviewerHistory.Location = new System.Drawing.Point(37, 26);
            this.tbReviewerHistory.Multiline = true;
            this.tbReviewerHistory.Name = "tbReviewerHistory";
            this.tbReviewerHistory.ReadOnly = true;
            this.tbReviewerHistory.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbReviewerHistory.Size = new System.Drawing.Size(823, 1109);
            this.tbReviewerHistory.TabIndex = 1;
            // 
            // ucReviewerHistory
            // 
            this.AccessibleRole = System.Windows.Forms.AccessibleRole.StaticText;
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Controls.Add(this.tbReviewerHistory);
            this.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.Name = "ucReviewerHistory";
            this.Size = new System.Drawing.Size(900, 1174);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridViewLinkColumn Date;
        private System.Windows.Forms.DataGridViewLinkColumn ReviewedBy;
        private System.Windows.Forms.DataGridViewButtonColumn btnLoad;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClaimantId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClaimId;
        private System.Windows.Forms.TextBox tbReviewerHistory;
    }
}
