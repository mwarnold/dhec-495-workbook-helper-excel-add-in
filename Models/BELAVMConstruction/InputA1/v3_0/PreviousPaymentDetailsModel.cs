﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DheccExcelAddIn2013.Models.BELAVMConstruction.InputA1.v3_0
{
    public class PreviousPaymentDetailsModel
    {
        public string PaymentCategory { get; set; }
        public string PaymentDescription{ get; set; }
        public string Amount { get; set; }
        public string DatePaid { get; set; }
    }
}
