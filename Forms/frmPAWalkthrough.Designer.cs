﻿namespace DheccExcelAddIn2013.Forms
{
    partial class frmPAWalkthrough
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label label8;
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tcPAWalkthrough = new System.Windows.Forms.TabControl();
            this.tpStep1 = new System.Windows.Forms.TabPage();
            this.imgPAStep1 = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnS2 = new System.Windows.Forms.Button();
            this.tpPAStep2 = new System.Windows.Forms.TabPage();
            this.label4 = new System.Windows.Forms.Label();
            this.imgPAStep2 = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.tpPAStep3 = new System.Windows.Forms.TabPage();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button2 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.tpPAStep4 = new System.Windows.Forms.TabPage();
            this.label7 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.button3 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.tpPAStep5 = new System.Windows.Forms.TabPage();
            label8 = new System.Windows.Forms.Label();
            this.tcPAWalkthrough.SuspendLayout();
            this.tpStep1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgPAStep1)).BeginInit();
            this.tpPAStep2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgPAStep2)).BeginInit();
            this.tpPAStep3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tpPAStep4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.tpPAStep5.SuspendLayout();
            this.SuspendLayout();
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label8.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            label8.Location = new System.Drawing.Point(20, 37);
            label8.Name = "label8";
            label8.Size = new System.Drawing.Size(1143, 48);
            label8.TabIndex = 0;
            label8.Text = "That\'s it!! Please restart Excel for the changes to take effect. ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(140, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(1059, 32);
            this.label1.TabIndex = 0;
            this.label1.Text = "DWH 495 Helper Add In requires programmatic access to the VBA Module of Excel.";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(120, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(1099, 32);
            this.label2.TabIndex = 1;
            this.label2.Text = "Below is a tutorial on how to set this up in Excel. This should only take a minut" +
    "e or two.";
            // 
            // tcPAWalkthrough
            // 
            this.tcPAWalkthrough.Controls.Add(this.tpStep1);
            this.tcPAWalkthrough.Controls.Add(this.tpPAStep2);
            this.tcPAWalkthrough.Controls.Add(this.tpPAStep3);
            this.tcPAWalkthrough.Controls.Add(this.tpPAStep4);
            this.tcPAWalkthrough.Controls.Add(this.tpPAStep5);
            this.tcPAWalkthrough.Location = new System.Drawing.Point(27, 153);
            this.tcPAWalkthrough.Name = "tcPAWalkthrough";
            this.tcPAWalkthrough.SelectedIndex = 0;
            this.tcPAWalkthrough.Size = new System.Drawing.Size(1272, 794);
            this.tcPAWalkthrough.TabIndex = 2;
            // 
            // tpStep1
            // 
            this.tpStep1.Controls.Add(this.imgPAStep1);
            this.tpStep1.Controls.Add(this.label3);
            this.tpStep1.Controls.Add(this.btnS2);
            this.tpStep1.Location = new System.Drawing.Point(4, 40);
            this.tpStep1.Name = "tpStep1";
            this.tpStep1.Padding = new System.Windows.Forms.Padding(3);
            this.tpStep1.Size = new System.Drawing.Size(1264, 750);
            this.tpStep1.TabIndex = 0;
            this.tpStep1.Text = "Step 1";
            this.tpStep1.UseVisualStyleBackColor = true;
            // 
            // imgPAStep1
            // 
            this.imgPAStep1.Image = global::DheccExcelAddIn2013.Properties.Resources.paWalkthroughStep1;
            this.imgPAStep1.Location = new System.Drawing.Point(43, 101);
            this.imgPAStep1.Name = "imgPAStep1";
            this.imgPAStep1.Size = new System.Drawing.Size(465, 586);
            this.imgPAStep1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgPAStep1.TabIndex = 2;
            this.imgPAStep1.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(37, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(620, 32);
            this.label3.TabIndex = 1;
            this.label3.Text = "In Excel, click File then Options. (Circled in Red)";
            // 
            // btnS2
            // 
            this.btnS2.Location = new System.Drawing.Point(1085, 673);
            this.btnS2.Name = "btnS2";
            this.btnS2.Size = new System.Drawing.Size(141, 50);
            this.btnS2.TabIndex = 0;
            this.btnS2.Text = "Next";
            this.btnS2.UseVisualStyleBackColor = true;
            this.btnS2.Click += new System.EventHandler(this.btnS2_Click);
            // 
            // tpPAStep2
            // 
            this.tpPAStep2.Controls.Add(this.label4);
            this.tpPAStep2.Controls.Add(this.imgPAStep2);
            this.tpPAStep2.Controls.Add(this.button1);
            this.tpPAStep2.Location = new System.Drawing.Point(4, 40);
            this.tpPAStep2.Name = "tpPAStep2";
            this.tpPAStep2.Padding = new System.Windows.Forms.Padding(3);
            this.tpPAStep2.Size = new System.Drawing.Size(1264, 750);
            this.tpPAStep2.TabIndex = 1;
            this.tpPAStep2.Text = "Step 2";
            this.tpPAStep2.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(25, 37);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(1184, 32);
            this.label4.TabIndex = 3;
            this.label4.Text = "Click the Trust Center link in the left navigation payne as shown in the image. (" +
    "Circled in Red)";
            // 
            // imgPAStep2
            // 
            this.imgPAStep2.Image = global::DheccExcelAddIn2013.Properties.Resources.paWalkthroughStep2;
            this.imgPAStep2.Location = new System.Drawing.Point(43, 101);
            this.imgPAStep2.Name = "imgPAStep2";
            this.imgPAStep2.Size = new System.Drawing.Size(542, 594);
            this.imgPAStep2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgPAStep2.TabIndex = 2;
            this.imgPAStep2.TabStop = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1085, 673);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(141, 50);
            this.button1.TabIndex = 1;
            this.button1.Text = "Next";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tpPAStep3
            // 
            this.tpPAStep3.Controls.Add(this.pictureBox1);
            this.tpPAStep3.Controls.Add(this.button2);
            this.tpPAStep3.Controls.Add(this.label5);
            this.tpPAStep3.Location = new System.Drawing.Point(4, 40);
            this.tpPAStep3.Name = "tpPAStep3";
            this.tpPAStep3.Padding = new System.Windows.Forms.Padding(3);
            this.tpPAStep3.Size = new System.Drawing.Size(1264, 750);
            this.tpPAStep3.TabIndex = 2;
            this.tpPAStep3.Text = "Step 3";
            this.tpPAStep3.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::DheccExcelAddIn2013.Properties.Resources.paWalkthroughStep3;
            this.pictureBox1.Location = new System.Drawing.Point(43, 101);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1028, 594);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(1085, 673);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(141, 50);
            this.button2.TabIndex = 5;
            this.button2.Text = "Next";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(37, 37);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(984, 32);
            this.label5.TabIndex = 4;
            this.label5.Text = "Click the Trust Center Settings button as shown in the image. (Circled in Red)";
            // 
            // tpPAStep4
            // 
            this.tpPAStep4.Controls.Add(this.label7);
            this.tpPAStep4.Controls.Add(this.pictureBox2);
            this.tpPAStep4.Controls.Add(this.button3);
            this.tpPAStep4.Controls.Add(this.label6);
            this.tpPAStep4.Location = new System.Drawing.Point(4, 40);
            this.tpPAStep4.Name = "tpPAStep4";
            this.tpPAStep4.Padding = new System.Windows.Forms.Padding(3);
            this.tpPAStep4.Size = new System.Drawing.Size(1264, 750);
            this.tpPAStep4.TabIndex = 3;
            this.tpPAStep4.Text = "Step 4";
            this.tpPAStep4.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(38, 52);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(419, 32);
            this.label7.TabIndex = 10;
            this.label7.Text = "project object model is checked.";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::DheccExcelAddIn2013.Properties.Resources.paWalkthroughStep4;
            this.pictureBox2.Location = new System.Drawing.Point(44, 96);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(742, 594);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 9;
            this.pictureBox2.TabStop = false;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(1086, 668);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(141, 50);
            this.button3.TabIndex = 8;
            this.button3.Text = "Next";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(38, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(1142, 32);
            this.label6.TabIndex = 7;
            this.label6.Text = "Click the Macro Settings link in the left payne. Make sure that the Trust access " +
    "to the VBA ";
            // 
            // tpPAStep5
            // 
            this.tpPAStep5.Controls.Add(label8);
            this.tpPAStep5.Location = new System.Drawing.Point(4, 40);
            this.tpPAStep5.Name = "tpPAStep5";
            this.tpPAStep5.Padding = new System.Windows.Forms.Padding(3);
            this.tpPAStep5.Size = new System.Drawing.Size(1264, 750);
            this.tpPAStep5.TabIndex = 4;
            this.tpPAStep5.Text = "Step 5";
            this.tpPAStep5.UseVisualStyleBackColor = true;
            // 
            // frmPAWalkthrough
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1326, 974);
            this.Controls.Add(this.tcPAWalkthrough);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "frmPAWalkthrough";
            this.Text = "Allowing Programmatic Access Walkthrough";
            this.tcPAWalkthrough.ResumeLayout(false);
            this.tpStep1.ResumeLayout(false);
            this.tpStep1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgPAStep1)).EndInit();
            this.tpPAStep2.ResumeLayout(false);
            this.tpPAStep2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgPAStep2)).EndInit();
            this.tpPAStep3.ResumeLayout(false);
            this.tpPAStep3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tpPAStep4.ResumeLayout(false);
            this.tpPAStep4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.tpPAStep5.ResumeLayout(false);
            this.tpPAStep5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabControl tcPAWalkthrough;
        private System.Windows.Forms.TabPage tpStep1;
        private System.Windows.Forms.TabPage tpPAStep2;
        private System.Windows.Forms.Button btnS2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox imgPAStep1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox imgPAStep2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabPage tpPAStep3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TabPage tpPAStep4;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TabPage tpPAStep5;
    }
}