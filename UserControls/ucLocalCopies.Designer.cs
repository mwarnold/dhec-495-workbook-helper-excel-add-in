﻿namespace DheccExcelAddIn2013.UserControls
{
    partial class ucLocalCopies
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblSearchText1 = new System.Windows.Forms.Label();
            this.tbSearch = new System.Windows.Forms.TextBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.aReset = new System.Windows.Forms.Label();
            this.dgLocalCopies = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgLocalCopies)).BeginInit();
            this.SuspendLayout();
            // 
            // lblSearchText1
            // 
            this.lblSearchText1.AutoSize = true;
            this.lblSearchText1.Location = new System.Drawing.Point(53, 72);
            this.lblSearchText1.Name = "lblSearchText1";
            this.lblSearchText1.Size = new System.Drawing.Size(105, 32);
            this.lblSearchText1.TabIndex = 10;
            this.lblSearchText1.Text = "Search";
            // 
            // tbSearch
            // 
            this.tbSearch.Location = new System.Drawing.Point(181, 69);
            this.tbSearch.Name = "tbSearch";
            this.tbSearch.Size = new System.Drawing.Size(390, 38);
            this.tbSearch.TabIndex = 11;
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(617, 67);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(92, 44);
            this.btnSearch.TabIndex = 17;
            this.btnSearch.Text = "Go";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // aReset
            // 
            this.aReset.AutoSize = true;
            this.aReset.Location = new System.Drawing.Point(744, 72);
            this.aReset.Name = "aReset";
            this.aReset.Size = new System.Drawing.Size(89, 32);
            this.aReset.TabIndex = 22;
            this.aReset.Text = "Reset";
            this.aReset.Click += new System.EventHandler(this.aReset_Click);
            // 
            // dgLocalCopies
            // 
            this.dgLocalCopies.AllowUserToAddRows = false;
            this.dgLocalCopies.AllowUserToDeleteRows = false;
            this.dgLocalCopies.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgLocalCopies.Location = new System.Drawing.Point(52, 166);
            this.dgLocalCopies.Name = "dgLocalCopies";
            this.dgLocalCopies.ReadOnly = true;
            this.dgLocalCopies.RowTemplate.Height = 40;
            this.dgLocalCopies.Size = new System.Drawing.Size(794, 954);
            this.dgLocalCopies.TabIndex = 23;
            this.dgLocalCopies.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgLocalCopies_CellMouseDoubleClick);
            // 
            // ucLocalCopies
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Controls.Add(this.dgLocalCopies);
            this.Controls.Add(this.aReset);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.tbSearch);
            this.Controls.Add(this.lblSearchText1);
            this.Name = "ucLocalCopies";
            this.Size = new System.Drawing.Size(900, 1174);
            ((System.ComponentModel.ISupportInitialize)(this.dgLocalCopies)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblSearchText1;
        private System.Windows.Forms.TextBox tbSearch;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label aReset;
        private System.Windows.Forms.DataGridView dgLocalCopies;



    }
}
