﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DheccExcelAddIn2013.Models
{
    public class AppSetting
    {
        public string WebServiceURL { get; set; }
        public string WebServiceEnvironment { get; set; }
        public AddInModel UserAddIn { get; set; }
        public List<WorkbookModel> DownloadedWorkbooks { get; set; }
        public List<WorkbookModel> ActiveWorkbooks { get; set; }
        public List<int> MyClaims { get; set; }
        public List<MeasureModel> Measures { get; set; }
        public UserModel User { get; set; }
        public List<int> StartedClaims { get; set; }
        public bool ProgrammaticAccessTutorialShown { get; set; }
        public AppSetting()
        {
            DownloadedWorkbooks = new List<WorkbookModel>();
            MyClaims = new List<int>();
            Measures = new List<MeasureModel>();
            User = new UserModel();
            ActiveWorkbooks = new List<WorkbookModel>();
            StartedClaims = new List<int>();
        }
    }
}
