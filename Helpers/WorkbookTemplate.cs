﻿using DheccExcelAddIn2013.Forms;
using DheccExcelAddIn2013.Interfaces;
using DheccExcelAddIn2013.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VBIDE = Microsoft.Vbe.Interop;

namespace DheccExcelAddIn2013.Helpers
{
    public static class WorkbookTemplate
    {
        private static BackgroundWorker _bgWorker = new BackgroundWorker();
        private static IDheccContract iContract = new DheccContract();

        public static string DownloadWorkbookTemplate(WorkbookModel wb)
        {
            bool failedOnce = false;
            string ext = Path.GetExtension(wb.FileName);
            string filePath = AppVariables.LocalExcelFileDirectory() + string.Format("{0}_{1}_{2}{3}",
               iContract.CurrentClaimId(), iContract.CurrentClaimantId(),
               iContract.CurrentWorkbookWrapper().Workbook.Id, ext);
            string tempFile = AppVariables.TemplateDirectory() + wb.FileName;
            try{
                if (File.Exists(filePath))
                    File.Delete(filePath);
            }
            catch{}
            try
            {
                //User doesn't have the most recent version of the workbook
                if (iContract.AppSetting().DownloadedWorkbooks.FindAll(x => x.Id == wb.Id).Count == 0)
                {
                    var client = AppVariables.BGWebService();
                    int userId = AppVariables.CurrentUserId;
                    string authToken = AppVariables.AuthToken;
                    int workbookType = wb.WorkbookTypeId;
                    var wbStream = client.GetWorkbook(AppVariables.CurrentUserId, AppVariables.AuthToken, wb.Id);
                    if (!Directory.Exists(AppVariables.TemplateRootDirectory()))
                        Directory.CreateDirectory(AppVariables.TemplateRootDirectory());
                    File.WriteAllBytes(tempFile, wbStream.WorkbookStream);

                    File.Copy(tempFile, filePath);
                    iContract.AppSetting().DownloadedWorkbooks.Add(wb);
                    iContract.AppSettingHelper().SaveSettings(iContract.AppSetting());
                }
                else // copy the prev downloaded copy and attach
                {
                    File.Copy(tempFile, filePath);
                }
                iContract.CurrentWorkbookWrapper().Workbook.LocalFilePath = filePath;
                new WIPHelper().SaveWIP(iContract.CurrentWorkbookWrapper());
                return filePath;
            }
            catch (Exception ex)
            {
                if (!failedOnce)
                {
                    failedOnce = true;
                    DownloadWorkbookTemplate(wb);
                    return filePath;
                }
                else
                {
                    EmailHelper.SendExceptionEmail(ex.ToString());
                    AppVariables.SetCurrentException(ex.ToString());
                    return "";
                }
            }
        }
        public static bool OpenWorkInProgress(int claimId)
        {
            try
            {
                AppVariables.SetTemplateIsLoading(true);
                var wip = new WIPHelper().GetWIP().SingleOrDefault(x => x.ClaimInfo.ClaimId == claimId && x.CanEdit == true);
               // wip = CheckForPendingMeasureUpload(wip);
                iContract.SetCurrentWorkbookWrapper(wip);
                new WIPHelper().SaveWIP(wip);
                Globals.ThisAddIn.Application.Workbooks.Add(wip.Workbook.LocalFilePath);
                CheckWorkbookForUpdates(true);
                AppVariables.SetTemplateIsLoading(false);
                AppVariables.SetDWH495WorkbookOpened(true);

                //if logged in, check to see if zone or any other BEL data has changed and alert to user with options
                if (AppVariables.IsLoggedIn)
                {
                    var client = AppVariables.BGWebService();
                    com.browngreer.www3.clsDownloadExcelInfo belData = client.GetDownloadExcelInfo(AppVariables.CurrentUserId,
                        iContract.CurrentWorkbookWrapper().ClaimInfo.ClaimantId, iContract.CurrentWorkbookWrapper().ClaimInfo.ClaimId, AppVariables.AuthToken);

                    List<string>currentDBList = iContract.CurrentWorkbookWrapper().ClaimInfo.DownloadBel;
                    List<string> updatedDbList = new List<string>();
                    foreach (var bv in belData.DownloadExcelAttrInfoList)
                        updatedDbList.Add(string.IsNullOrEmpty(bv.AttributeValue) ? "" : bv.AttributeValue);
                    var diff = currentDBList.Except(updatedDbList);
                    if (diff.Count() > 0)
                    {
                        frmDownloadChanged frmDC = new frmDownloadChanged(currentDBList, updatedDbList);
                        frmDC.Show();
                    }                
                }

                return true;
            }
            catch { return false; }
        }

        public static void CheckForPendingMeasureUpload()
        {
            try
            {
                var wipHelper = new WIPHelper();
                 var allWb = wipHelper.GetWIP();
                 foreach (var ww in allWb)
                 {
                     var pendingMeasures = ww.Measures.FindAll(x => x.PendingUpload == true);
                     foreach (var asm in pendingMeasures)
                     {
                         var client = AppVariables.BGWebService();
                         var cResult = client.clsAddAcctMeasure(AppVariables.CurrentUserId, AppVariables.AuthToken, asm.MeasureId,
                                  iContract.CurrentClaimId(), iContract.CurrentClaimantId(),
                                  asm.Started, asm.Started, asm.Checked);
                         if (WebServiceResponseHelper.ValidateResponse(cResult.Result.ResultID, cResult.Result.ResultMessage))
                             asm.PendingUpload = false;
                     }
                     wipHelper.SaveWIP(ww);
                 }
            }
            catch { }
        }
        public static bool OpenNewWorkbook(WorkbookWrapper ww)
        {
            try
            {
                WIPHelper wipHelper = new WIPHelper();
                AppVariables.SetTemplateIsLoading(true);
                new WIPHelper().SaveWIP(ww);
                iContract.SetCurrentWorkbookWrapper(ww);
                string filePath = DownloadWorkbookTemplate(ww.Workbook);
                Globals.ThisAddIn.Application.Workbooks.Add(iContract.CurrentWorkbookWrapper().Workbook.LocalFilePath);
                //save as macro enabled
                SaveWorkbook(false);
                //new workbook must reset list of macros and formulas
                ww.Workbook.Macros = new List<MacroModel>();
                ww.Workbook.Formulas = new List<FormulaModel>();
                wipHelper.SaveWIP(ww);

                //now check to see what macros need to be applied to this workbook
                CheckWorkbookForUpdates(false);
                wipHelper.SaveWIP(ww);
                //delete .xls version of workbook
                File.Delete(filePath);
                AppVariables.SetTemplateIsLoading(false);
                AppVariables.SetDWH495WorkbookOpened(true);
                return true;
            }
            catch { return false; }

        }

        public static bool OpenRemoteWorkbook(WorkbookWrapper ww, int saveHistId)
        {
            try
            {
                //ww = CheckForPendingMeasureUpload(ww);
                AppVariables.SetTemplateIsLoading(true);
                var client = AppVariables.BGWebService();
                var wbInfo = client.GetInterimWorkbookInfo(AppVariables.CurrentUserId, AppVariables.AuthToken, saveHistId);
                string filePath = "";
                string fileName = string.Format("{0}_{1}_{2}_RemoteCopy.xlsm", ww.ClaimInfo.ClaimId, ww.ClaimInfo.ClaimantId, ww.Workbook.Id);
                filePath = AppVariables.DownloadedExcelFileDirectory() + fileName;

                if (!Directory.Exists(AppVariables.DownloadedExcelFileDirectory()))
                    Directory.CreateDirectory(AppVariables.DownloadedExcelFileDirectory());
                ww.Workbook.LocalFilePath = filePath;
                File.WriteAllBytes(filePath, wbInfo.WorkbookStream);
                var wipHelper = new WIPHelper();
                var prevCopies = wipHelper.GetWIP().FindAll(x => x.IsRemoteCopy == true && x.ClaimInfo.ClaimId == ww.ClaimInfo.ClaimId);
                foreach (var pc in prevCopies)
                    wipHelper.RemoveWIP(pc);
                new WIPHelper().SaveWIP(ww);
                iContract.SetCurrentWorkbookWrapper(ww);
                Globals.ThisAddIn.Application.Workbooks.Add(filePath);
                CheckWorkbookForUpdates(true);
                AppVariables.SetTemplateIsLoading(false);
                AppVariables.SetDWH495WorkbookOpened(true);
                return true;
            }
            catch { return false; }
        }

        public static string GetWorkbookFileName(int workbookId)
        {
            try
            {
                return iContract.AppSetting().ActiveWorkbooks.SingleOrDefault(x => x.Id == workbookId).FileName;
            }
            catch { return ""; }
        }

        public static void SaveWorkbook(bool showSavedMessage = true)
        {
            try
            {
                if (Globals.ThisAddIn.Application.Workbooks.Count == 0)
                {
                    MessageBox.Show("No workbook to save.");
                    return;
                }
                string filePathToSave = "";
                Microsoft.Office.Interop.Excel.Worksheet ws = Globals.ThisAddIn.GetDownloadSheet();
                ExcelPaster.SetCellValue(ws, "Z1", "");
                string filePath = AppVariables.LocalExcelFileDirectory();
                filePathToSave = filePath + string.Format("{0}_{1}_{2}.xlsm",
                    iContract.CurrentClaimId(), iContract.CurrentClaimantId(),
                    iContract.CurrentWorkbookWrapper().Workbook.Id);
                Globals.ThisAddIn.Application.DisplayAlerts = false;
                Globals.ThisAddIn.Application.ActiveWorkbook.SaveAs(filePathToSave,
                    Microsoft.Office.Interop.Excel.XlFileFormat.xlOpenXMLWorkbookMacroEnabled, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange);
                Globals.ThisAddIn.Application.DisplayAlerts = true;
                iContract.CurrentWorkbookWrapper().Workbook.LocalFilePath = filePathToSave;
                iContract.CurrentWorkbookWrapper().CanEdit = true;
                iContract.CurrentWorkbookWrapper().LastSaveDate = DateTime.Now;
                new WIPHelper().SaveWIP(iContract.CurrentWorkbookWrapper());
                if (showSavedMessage)
                    MessageBox.Show("Workbook Saved");
            }
            catch (Exception ex)
            {
                if (!ex.ToString().Contains("Cannot access"))
                    MessageBox.Show("An error occured while saving to local. Details: " + ex.ToString());
            }
        }

        public static byte[] SaveWorkbookForUploadAndGetStream()
        {
            try
            {
                string filePath = AppVariables.LocalExcelFileDirectory();
                string fullPath = filePath +
                    string.Format("{0}_{1}_{2}_save.xlsm", iContract.CurrentClaimId(),
                    iContract.CurrentClaimantId(), iContract.CurrentWorkbookWrapper().Workbook.Id);
                Globals.ThisAddIn.Application.ActiveWorkbook.SaveCopyAs(fullPath);
                return File.ReadAllBytes(fullPath);
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occured while saving to local. Details: " + ex.ToString());
                return null;
            }
        }

        public static void DeleteTempSaveFile()
        {
            try
            {
                string filePath = AppVariables.LocalExcelFileDirectory();
                string fullPath = filePath +
                    string.Format("{0}_{1}_{2}_save.xlsm", iContract.CurrentClaimId(), iContract.CurrentClaimantId(),
                    iContract.CurrentWorkbookWrapper().Workbook.Id);
                Globals.ThisAddIn.Application.ActiveWorkbook.SaveCopyAs(fullPath);
                File.Delete(fullPath);
            }
            catch { }
        }

        public static ClaimInfoModel GetActiveWorkbookClaimInfo()
        {
            Microsoft.Office.Interop.Excel.Worksheet ws = Globals.ThisAddIn.GetDownloadSheet();
            int claimant = Convert.ToInt32(ExcelPaster.GetClaimOrClaimantNumber(ws, "C3"));
            int claim = Convert.ToInt32(ExcelPaster.GetClaimOrClaimantNumber(ws, "C4"));
            string clmtName = ExcelPaster.GetCellValue(ws, "C2");
            string tempVal = ExcelPaster.GetCellValue(ws, "Z1");
            return new ClaimInfoModel()
            {
                ClaimantId = claimant,
                ClaimantName = clmtName,
                ClaimId = claim,
                TempVal = tempVal
            };
        }
        private static bool _initialLoadComplete = true;
        public static bool InitialWbLoadComplete { get { return _initialLoadComplete; } set { _initialLoadComplete = value; } }
        public static void UpdateActiveWorkbookList(com.browngreer.www3.clsWorkbookInfo[] wbList)
        {
            List<WorkbookModel> workbookList = new List<WorkbookModel>();
            foreach (var wb in wbList)
            {
                var wbToAdd =
                     new WorkbookModel()
                     {
                         Active = wb.Active,
                         DateAdded = wb.DateAdded,
                         Id = wb.ID,
                         Name = wb.Name,
                         Version = wb.Version,
                         WorkbookTypeId = wb.WorkbookType.WorkbookTypeID,
                         FileName = Path.GetFileName(wb.FilePath)
                     };
                foreach (var m in wb.MacroInfoList)
                {
                    wbToAdd.Macros.Add(
                        new MacroModel()
                        {
                            Active = m.Active,
                            Id = m.MacroID,
                            DateAdded = m.DateAdded,
                            Description = m.Description,
                            FileType = m.FileType,
                            CodeName = m.CodeName,
                            ModuleName = m.ModuleName,
                            WorkbookId = m.WorkbookID
                        });
                }
                if (wb.FormulasInfoList != null)
                {
                    foreach (var f in wb.FormulasInfoList)
                    {
                        wbToAdd.Formulas.Add(
                            new FormulaModel()
                            {
                                CellRef = f.CellRef,
                                DateAdded = f.DateAdded,
                                Description = f.Description,
                                Formula = f.FormulaValue,
                                Id = f.ID,
                                SheetName = f.SheetName,
                                WorkbookId = f.WorkbookID
                            });
                    }
                }
                workbookList.Add(wbToAdd);
            }
            iContract.AppSetting().ActiveWorkbooks = workbookList;
            iContract.AppSettingHelper().SaveSettings(iContract.AppSetting());
            if (!_bgWorker.IsBusy)
            {
                _bgWorker = new BackgroundWorker();
                iContract.SetBackgroudUploadWorker(_bgWorker);
                _bgWorker.DoWork += new DoWorkEventHandler(GetMacroFilesForActiveWorkbooks);
                _bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(GetMacroFilesForActiveWorkbooksProcessComplete);
                _bgWorker.RunWorkerAsync();
            }           
        }

        private static void GetMacroFilesForActiveWorkbooksProcessComplete(object sender, RunWorkerCompletedEventArgs e)
        {
            if (_initialLoadComplete)
                return;
            if (e.Error != null)
                MessageBox.Show("An Error Occured: " + e.Error.Message);
            else
                MessageBox.Show("The Add In finished gathering the neecissary information to load your queue.");
            iContract.ShowHideButtons();
            _initialLoadComplete = true;
        }

        public static void GetMacroFilesForActiveWorkbooks(object sender, DoWorkEventArgs e)
        {
            if (AppVariables.IsLoggedIn)
            {
                try
                {
                    var activeWorkbooks = iContract.AppSetting().ActiveWorkbooks;
                    foreach (var wb in activeWorkbooks)
                    {
                        foreach (var macro in wb.Macros)
                        {
                            string macroWbRootPath = AppVariables.LocalMacroDirectory() + string.Format(@"{0}\{1}", wb.Id, macro.Id);
                            string macroWbPath = AppVariables.LocalMacroDirectory() + string.Format(@"{0}\{1}\", wb.Id, macro.Id);
                            string encryptedMacroPath = macroWbPath + macro.ModuleName + ".txt";
                            if (!File.Exists(encryptedMacroPath))
                            {
                                if (!Directory.Exists(macroWbRootPath))
                                    Directory.CreateDirectory(macroWbRootPath);

                                //call web service and update
                                var client = AppVariables.BGWebService();
                                var cResult = client.GetWorkbookMacro(AppVariables.CurrentUserId, AppVariables.AuthToken, macro.Id);
                                bool success = Encryption.MAesHelper.EncryptFile(cResult.MacroDetails.FileStream_Macros, encryptedMacroPath, AppVariables.PasswordValue, AppVariables.SaltKey);
                            }
                        }
                    }
                }
                catch(Exception ex)
                {
                    EmailHelper.SendExceptionEmail(ex.ToString() + Environment.NewLine + iContract.AppSetting().User.UserName +
                           " - Background Process Failed");
                }
            }
        }

        public static void CheckWorkbookForUpdates(bool askUserToUpdate)
        {
            Cursor.Current = Cursors.WaitCursor;
            var ww = iContract.CurrentWorkbookWrapper();

            //not versioning imported workbooks not originated from Add In
            if (ww.IsImportedClaim)
                return;
            var activeWorkbook = iContract.AppSetting().ActiveWorkbooks.SingleOrDefault(x => x.Id == ww.Workbook.Id);

            //Now validate that the workbook being used is still active. If not alert the user and cancel update.
            if (activeWorkbook == null)
            {
                MessageBox.Show("This workbook is not an active workbook. Please contact your department head for instructions on how to " +
                    "migrate to the active workbook.");
                return;
            }

            //macros
            var currentMacros = ww.Workbook.Macros;
            var currentMacroIds = ww.Workbook.Macros.Select(x => x.Id).ToList();
            var activeMacros = activeWorkbook.Macros;
            var activeMacroIds = activeWorkbook.Macros.Select(x => x.Id).ToList();
            var macrosToUpdate = activeMacros.FindAll(x => !currentMacroIds.Contains(x.Id));

            //formulas
            var currentFormulas = ww.Workbook.Formulas;
            var currentFormulaIds = currentFormulas.Select(x => x.Id);
            var activeFormulas = activeWorkbook.Formulas;
            var formulasToUpdate = activeFormulas.FindAll(x => !currentFormulaIds.Contains(x.Id));

            bool isError = false;
            List<int> failedMacroIds = new List<int>();
            if (macrosToUpdate.Count() > 0 || formulasToUpdate.Count() > 0)
            {
                DialogResult dialogResult = DialogResult.Yes;
                if (askUserToUpdate)
                    dialogResult = MessageBox.Show("Newer version of this workbook is available. Would to like to run update process now? ", "Workbook Update", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes || !askUserToUpdate)
                {
                    Microsoft.Office.Interop.Excel.Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
                    try
                    {
                        //loop through all the macros to be updated
                        foreach (VBIDE.VBComponent comp in Globals.ThisAddIn.Application.ActiveWorkbook.VBProject.VBComponents)
                        {
                            //check to see if there is an update for this module
                            var macro = macrosToUpdate.SingleOrDefault(x => x.CodeName.Trim() == comp.Name);
                            if (macro == null)
                                continue;

                            string macroWbPath = AppVariables.LocalMacroDirectory() + string.Format(@"{0}\{1}\", activeWorkbook.Id, macro.Id);
                            string encryptedMacroPath = macroWbPath + macro.ModuleName + ".txt";
                            var dec = Encryption.MAesHelper.DecryptFile(encryptedMacroPath, AppVariables.PasswordValue, AppVariables.SaltKey);
                            string decryptedMacroPath = macroWbPath + string.Format("{0}{1}", macro.ModuleName, macro.FileType);
                            //temporarilty decrypt and save                           
                            File.WriteAllBytes(decryptedMacroPath, dec.DecryptedBytes);

                            if (comp.Type == VBIDE.vbext_ComponentType.vbext_ct_Document)
                            {
                                //remove any prev code associated with same code module
                                int iLen = comp.CodeModule.CountOfLines;
                                if (iLen > 0)
                                    comp.CodeModule.DeleteLines(1, iLen);
                                try
                                {
                                    comp.CodeModule.AddFromFile(decryptedMacroPath);
                                    File.Delete(decryptedMacroPath);
                                    ww.Workbook.Macros.Add(macro);
                                }
                                catch (Exception ex)
                                {
                                    File.Delete(decryptedMacroPath);
                                    failedMacroIds.Add(macro.Id);
                                    isError = true;
                                    EmailHelper.SendExceptionEmail(ex.ToString() + Environment.NewLine + iContract.AppSetting().User.UserName +
                             " - WB: " + ww.Workbook.Id + " - Macro:" + macro.Id + " - " + macro.ModuleName);
                                }
                            }
                            else if (comp.Type == VBIDE.vbext_ComponentType.vbext_ct_StdModule)
                            {
                                try
                                {
                                    //remove any prev code associated with same code module
                                    Globals.ThisAddIn.Application.ActiveWorkbook.VBProject.VBComponents.Remove(comp);
                                    Globals.ThisAddIn.Application.ActiveWorkbook.VBProject.VBComponents.Import(decryptedMacroPath);
                                    File.Delete(decryptedMacroPath);
                                    ww.Workbook.Macros.Add(macro);
                                }
                                catch (Exception ex)
                                {
                                    File.Delete(decryptedMacroPath);
                                    failedMacroIds.Add(macro.Id);
                                    isError = true;
                                    EmailHelper.SendExceptionEmail(ex.ToString() + Environment.NewLine + iContract.AppSetting().User.UserName +
                             " - WB: " + ww.Workbook.Id + " - Macro:" + macro.Id + " - " + macro.ModuleName);
                                }
                            }
                        }

                        //if this is the first time opening the workbook, need to add the code modules
                        if (!askUserToUpdate)
                        {

                            var basMacros = activeMacros.FindAll(x => x.FileType.ToLower() == ".bas");
                            foreach (var macro in basMacros)
                            {
                                string macroWbPath = AppVariables.LocalMacroDirectory() + string.Format(@"{0}\{1}\", activeWorkbook.Id, macro.Id);
                                string encryptedMacroPath = macroWbPath + macro.ModuleName + ".txt";
                                var dec = Encryption.MAesHelper.DecryptFile(encryptedMacroPath, AppVariables.PasswordValue, AppVariables.SaltKey);
                                string decryptedMacroPath = macroWbPath + string.Format("{0}{1}", macro.ModuleName, macro.FileType);
                                try
                                {
                                    //temporarilty decrypt and save                           
                                    File.WriteAllBytes(decryptedMacroPath, dec.DecryptedBytes);
                                    VBIDE.VBComponent oModule = Globals.ThisAddIn.Application.ActiveWorkbook.VBProject
                                        .VBComponents.Add(VBIDE.vbext_ComponentType.vbext_ct_StdModule);
                                    oModule.Name = macro.ModuleName.Trim();
                                    oModule.CodeModule.AddFromFile(decryptedMacroPath);
                                    File.Delete(decryptedMacroPath);
                                    ww.Workbook.Macros.Add(macro);
                                }
                                catch (Exception ex)
                                {
                                    File.Delete(decryptedMacroPath);
                                    failedMacroIds.Add(macro.Id);
                                    isError = true;
                                    EmailHelper.SendExceptionEmail(ex.ToString() + Environment.NewLine + iContract.AppSetting().User.UserName +
                              " - WB: " + ww.Workbook.Id + " - Macro:" + macro.Id + " - " + macro.ModuleName);
                                    continue;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        isError = true;
                    }
                    try
                    {
                        wb.VBProject.VBComponents.VBE.MainWindow.Close();
                    }
                    catch { }                   

                    //update formulas
                    foreach (var formula in formulasToUpdate)
                    {
                        continue;
                        try
                        {
                            Microsoft.Office.Interop.Excel.Worksheet ws = wb.Worksheets[formula.SheetName];
                            ws.Activate();
                            Microsoft.Office.Interop.Excel.Range rng = ws.get_Range(formula.CellRef);
                            rng.Formula = formula.Formula;
                            ww.Workbook.Formulas.Add(formula);
                        }
                        catch (Exception ex)
                        {
                            isError = true;
                            EmailHelper.SendExceptionEmail(ex.ToString() + Environment.NewLine + iContract.AppSetting().User.UserName +
                                " - WB: " + ww.Workbook.Id + " - Formula:" + formula.Id);
                            continue;
                        }
                    }
                    var toSetInactive = activeMacros.FindAll(x => !failedMacroIds.Contains(x.Id));
                    foreach (var m in toSetInactive)
                        m.Active = false;
                    if (!isError)
                    {
                        SaveWorkbook(false);
                        if (askUserToUpdate)
                            MessageBox.Show("Workbook Successfully Updated");
                    }
                    else
                        MessageBox.Show("Not all updates were successfully applied to the workbook.");
                }
            }
            Cursor.Current = Cursors.Arrow;
        }


    }
}
