﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DheccExcelAddIn2013.Models
{
    public class WorkbookWrapper
    {
        public AddInModel AddIn { get; set; }
        public ClaimInfoModel ClaimInfo { get; set; }
        public List<CommentModel> Comments { get; set; }
        public List<ReviewerHistoryModel> ReviewerHistory { get; set; }
        public WorkbookModel Workbook { get; set; }
        public int CompensationMethodId { get; set; }
        public int ReviewQueueTypeId { get; set; }
        public bool CanEditCompensationMethod { get; set; }
        public List<AcctStepModel> Measures { get; set; }
        public bool CanEdit { get; set; }
        public bool IsImportedClaim { get; set; }
        public bool IsRemoteCopy { get; set; }
        public int SaveHistId { get; set; }
        public DateTime LastSaveDate { get; set; }
      
        public WorkbookWrapper()
        {
            Comments = new List<CommentModel>();
            Workbook = new WorkbookModel();
            ReviewerHistory = new List<ReviewerHistoryModel>();          
            Measures = new List<AcctStepModel>();
            AddIn = new AddInModel();
            ClaimInfo = new ClaimInfoModel();
        }
    }
}
