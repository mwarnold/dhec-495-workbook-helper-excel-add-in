﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using DheccExcelAddIn2013.Models.BELAVMConstruction.MethodologySelection.v3_0;

namespace DheccExcelAddIn2013.Helpers.BELAVMConstruction.MethodologySelection.v3_0
{
    public static class MethodologySelectionHelper
    {
        public static MethodologySelectionModel Get()
        {
            Worksheet ws = Globals.ThisAddIn.Application.Worksheets["Methodology Selection"];
            ws.Activate();
            MethodologySelectionModel msm = new MethodologySelectionModel();
            msm.ProceedWith = ExcelPaster.GetCellValue(ws, "C4");
            List<string> rList = new List<string>();
            rList.Add(ExcelPaster.GetCellValue(ws, "A17"));
            rList.Add(ExcelPaster.GetCellValue(ws, "A18"));
            rList.Add(ExcelPaster.GetCellValue(ws, "A19"));
            rList.Add(ExcelPaster.GetCellValue(ws, "A20"));
            rList.Add(ExcelPaster.GetCellValue(ws, "A21"));
            msm.CellsA17_21 = rList;
            return msm;
        }

        public static void Set(MethodologySelectionModel msm)
        {
            Worksheet ws = Globals.ThisAddIn.Application.Worksheets["Methodology Selection"];
            ws.Activate();
            ExcelPaster.SetCellValue(ws, "C4", msm.ProceedWith);
            Range rng = ws.get_Range("A17", "A21");
            ExcelPaster.PasteHorizontalSingleDimension(msm.CellsA17_21, rng);
        }
    }
}
