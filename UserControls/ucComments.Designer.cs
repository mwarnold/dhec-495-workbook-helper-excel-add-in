﻿namespace DheccExcelAddIn2013.UserControls
{
    partial class ucComments
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbNewComment = new System.Windows.Forms.TextBox();
            this.btnAddNewComment = new System.Windows.Forms.Button();
            this.tbComments = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // tbNewComment
            // 
            this.tbNewComment.Location = new System.Drawing.Point(37, 33);
            this.tbNewComment.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tbNewComment.Multiline = true;
            this.tbNewComment.Name = "tbNewComment";
            this.tbNewComment.Size = new System.Drawing.Size(829, 264);
            this.tbNewComment.TabIndex = 0;
            // 
            // btnAddNewComment
            // 
            this.btnAddNewComment.Location = new System.Drawing.Point(558, 311);
            this.btnAddNewComment.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.btnAddNewComment.Name = "btnAddNewComment";
            this.btnAddNewComment.Size = new System.Drawing.Size(308, 55);
            this.btnAddNewComment.TabIndex = 1;
            this.btnAddNewComment.Text = "Add Comment";
            this.btnAddNewComment.UseVisualStyleBackColor = true;
            this.btnAddNewComment.Click += new System.EventHandler(this.btnAddNewComment_Click);
            // 
            // tbComments
            // 
            this.tbComments.BackColor = System.Drawing.SystemColors.Window;
            this.tbComments.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbComments.Enabled = false;
            this.tbComments.Location = new System.Drawing.Point(37, 418);
            this.tbComments.Multiline = true;
            this.tbComments.Name = "tbComments";
            this.tbComments.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbComments.Size = new System.Drawing.Size(829, 722);
            this.tbComments.TabIndex = 2;
            // 
            // ucComments
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Controls.Add(this.tbComments);
            this.Controls.Add(this.btnAddNewComment);
            this.Controls.Add(this.tbNewComment);
            this.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.Name = "ucComments";
            this.Size = new System.Drawing.Size(900, 1174);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbNewComment;
        private System.Windows.Forms.Button btnAddNewComment;
        private System.Windows.Forms.TextBox tbComments;
    }
}
