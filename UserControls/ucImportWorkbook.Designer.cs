﻿namespace DheccExcelAddIn2013.UserControls
{
    partial class ucImportWorkbook
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbImport = new System.Windows.Forms.GroupBox();
            this.btnImport = new System.Windows.Forms.Button();
            this.ddlReviewType = new System.Windows.Forms.ComboBox();
            this.lblSelectReviewType = new System.Windows.Forms.Label();
            this.lblTextSelW = new System.Windows.Forms.Label();
            this.btnSelectWorkbook = new System.Windows.Forms.Button();
            this.tbFilePath = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.gbImport.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbImport
            // 
            this.gbImport.Controls.Add(this.btnImport);
            this.gbImport.Controls.Add(this.ddlReviewType);
            this.gbImport.Controls.Add(this.lblSelectReviewType);
            this.gbImport.Controls.Add(this.lblTextSelW);
            this.gbImport.Controls.Add(this.btnSelectWorkbook);
            this.gbImport.Controls.Add(this.tbFilePath);
            this.gbImport.Location = new System.Drawing.Point(35, 170);
            this.gbImport.Name = "gbImport";
            this.gbImport.Size = new System.Drawing.Size(836, 412);
            this.gbImport.TabIndex = 0;
            this.gbImport.TabStop = false;
            this.gbImport.Text = "Import Information";
            // 
            // btnImport
            // 
            this.btnImport.Enabled = false;
            this.btnImport.Location = new System.Drawing.Point(638, 328);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(168, 55);
            this.btnImport.TabIndex = 11;
            this.btnImport.Text = "Import";
            this.btnImport.UseVisualStyleBackColor = true;
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // ddlReviewType
            // 
            this.ddlReviewType.FormattingEnabled = true;
            this.ddlReviewType.Items.AddRange(new object[] {
            "Initial Review",
            "Q/A",
            "Consistency"});
            this.ddlReviewType.Location = new System.Drawing.Point(325, 215);
            this.ddlReviewType.Name = "ddlReviewType";
            this.ddlReviewType.Size = new System.Drawing.Size(275, 39);
            this.ddlReviewType.TabIndex = 10;
            this.ddlReviewType.Text = "--Review Types--";
            this.ddlReviewType.SelectedIndexChanged += new System.EventHandler(this.ddlReviewType_SelectedIndexChanged);
            // 
            // lblSelectReviewType
            // 
            this.lblSelectReviewType.AutoSize = true;
            this.lblSelectReviewType.Location = new System.Drawing.Point(25, 217);
            this.lblSelectReviewType.Name = "lblSelectReviewType";
            this.lblSelectReviewType.Size = new System.Drawing.Size(273, 32);
            this.lblSelectReviewType.TabIndex = 9;
            this.lblSelectReviewType.Text = "Select Review Type:";
            // 
            // lblTextSelW
            // 
            this.lblTextSelW.AutoSize = true;
            this.lblTextSelW.Location = new System.Drawing.Point(37, 58);
            this.lblTextSelW.Name = "lblTextSelW";
            this.lblTextSelW.Size = new System.Drawing.Size(371, 32);
            this.lblTextSelW.TabIndex = 8;
            this.lblTextSelW.Text = "Select a workbook to import.";
            // 
            // btnSelectWorkbook
            // 
            this.btnSelectWorkbook.Location = new System.Drawing.Point(721, 110);
            this.btnSelectWorkbook.Name = "btnSelectWorkbook";
            this.btnSelectWorkbook.Size = new System.Drawing.Size(75, 55);
            this.btnSelectWorkbook.TabIndex = 7;
            this.btnSelectWorkbook.Text = "...";
            this.btnSelectWorkbook.UseVisualStyleBackColor = true;
            this.btnSelectWorkbook.Click += new System.EventHandler(this.btnSelectWorkbook_Click);
            // 
            // tbFilePath
            // 
            this.tbFilePath.Location = new System.Drawing.Point(31, 116);
            this.tbFilePath.Name = "tbFilePath";
            this.tbFilePath.Size = new System.Drawing.Size(670, 38);
            this.tbFilePath.TabIndex = 6;
            this.tbFilePath.TextChanged += new System.EventHandler(this.tbFilePath_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(43, 78);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(798, 32);
            this.label1.TabIndex = 1;
            this.label1.Text = "After importing a workbook, it will show up in your local copies.";
            // 
            // ucImportWorkbook
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.gbImport);
            this.Name = "ucImportWorkbook";
            this.Size = new System.Drawing.Size(900, 1174);
            this.gbImport.ResumeLayout(false);
            this.gbImport.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbImport;
        private System.Windows.Forms.Button btnImport;
        private System.Windows.Forms.ComboBox ddlReviewType;
        private System.Windows.Forms.Label lblSelectReviewType;
        private System.Windows.Forms.Label lblTextSelW;
        private System.Windows.Forms.Button btnSelectWorkbook;
        private System.Windows.Forms.TextBox tbFilePath;
        private System.Windows.Forms.Label label1;

    }
}
