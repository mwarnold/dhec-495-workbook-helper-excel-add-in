﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DheccExcelAddIn2013.Models.BELAVMConstruction.Documentation.v3_0;
using Microsoft.Office.Interop.Excel;
using DheccExcelAddIn2013.Models;

namespace DheccExcelAddIn2013.Helpers.BELAVMConstruction.Documentation.v3_0
{
    public static class DocumentationHelper
    {
        public static DocumentationChecklistModel Get()
        {
            DocumentationChecklistModel dcm = new DocumentationChecklistModel();
            Worksheet ws = Globals.ThisAddIn.Application.Worksheets["Documentation"];
            ws.Activate();
            GenericList gl = new GenericList();
            gl.Items.Add(ExcelPaster.GetCellValue(ws, "R6"));
            gl.Items.Add(ExcelPaster.GetCellValue(ws, "S6"));
            dcm.ClaimsPreparation = gl;

            gl = new GenericList();
            gl.Items.Add(ExcelPaster.GetCellValue(ws, "R7"));
            gl.Items.Add(ExcelPaster.GetCellValue(ws, "S7"));
            dcm.Contracts = gl;

            gl = new GenericList();
            gl.Items.Add(ExcelPaster.GetCellValue(ws, "R8"));
            gl.Items.Add(ExcelPaster.GetCellValue(ws, "S8"));
            dcm.FinancialStatements2006 = gl;

            gl = new GenericList();
            gl.Items.Add(ExcelPaster.GetCellValue(ws, "R9"));
            gl.Items.Add(ExcelPaster.GetCellValue(ws, "S9"));
            dcm.FinancialStatements2007 = gl;

            gl = new GenericList();
            gl.Items.Add(ExcelPaster.GetCellValue(ws, "R10"));
            gl.Items.Add(ExcelPaster.GetCellValue(ws, "S10"));
            dcm.FinancialStatements2008 = gl;

            gl = new GenericList();
            gl.Items.Add(ExcelPaster.GetCellValue(ws, "R11"));
            gl.Items.Add(ExcelPaster.GetCellValue(ws, "S11"));
            dcm.FinancialStatements2009 = gl;

            gl = new GenericList();
            gl.Items.Add(ExcelPaster.GetCellValue(ws, "R12"));
            gl.Items.Add(ExcelPaster.GetCellValue(ws, "S12"));
            dcm.FinancialStatements2010 = gl;

            gl = new GenericList();
            gl.Items.Add(ExcelPaster.GetCellValue(ws, "R13"));
            gl.Items.Add(ExcelPaster.GetCellValue(ws, "S13"));
            dcm.FinancialStatements2011 = gl;

            gl = new GenericList();
            gl.Items.Add(ExcelPaster.GetCellValue(ws, "R14"));
            gl.Items.Add(ExcelPaster.GetCellValue(ws, "S14"));
            dcm.FinancialStatements2012 = gl;

            gl = new GenericList();
            gl.Items.Add(ExcelPaster.GetCellValue(ws, "R15"));
            gl.Items.Add(ExcelPaster.GetCellValue(ws, "S15"));
            dcm.FinancialStatementsOther = gl;

            gl = new GenericList();
            gl.Items.Add(ExcelPaster.GetCellValue(ws, "R16"));
            gl.Items.Add(ExcelPaster.GetCellValue(ws, "S16"));
            dcm.RentalProperty = gl;

            gl = new GenericList();
            gl.Items.Add(ExcelPaster.GetCellValue(ws, "R17"));
            gl.Items.Add(ExcelPaster.GetCellValue(ws, "S17"));
            dcm.SupplementalFinancialDocuments = gl;

            gl = new GenericList();
            gl.Items.Add(ExcelPaster.GetCellValue(ws, "R18"));
            gl.Items.Add(ExcelPaster.GetCellValue(ws, "S18"));
            dcm.TaxReturn2006 = gl;

            gl = new GenericList();
            gl.Items.Add(ExcelPaster.GetCellValue(ws, "R19"));
            gl.Items.Add(ExcelPaster.GetCellValue(ws, "S19"));
            dcm.TaxReturn2007 = gl;

            gl = new GenericList();
            gl.Items.Add(ExcelPaster.GetCellValue(ws, "R20"));
            gl.Items.Add(ExcelPaster.GetCellValue(ws, "S20"));
            dcm.TaxReturn2008 = gl;

            gl = new GenericList();
            gl.Items.Add(ExcelPaster.GetCellValue(ws, "R21"));
            gl.Items.Add(ExcelPaster.GetCellValue(ws, "S21"));
            dcm.TaxReturn2009 = gl;

            gl = new GenericList();
            gl.Items.Add(ExcelPaster.GetCellValue(ws, "R22"));
            gl.Items.Add(ExcelPaster.GetCellValue(ws, "S22"));
            dcm.TaxReturn2010 = gl;

            gl = new GenericList();
            gl.Items.Add(ExcelPaster.GetCellValue(ws, "R23"));
            gl.Items.Add(ExcelPaster.GetCellValue(ws, "S23"));
            dcm.TaxReturn2011 = gl;

            gl = new GenericList();
            gl.Items.Add(ExcelPaster.GetCellValue(ws, "R24"));
            gl.Items.Add(ExcelPaster.GetCellValue(ws, "S24"));
            dcm.TaxReturn2012 = gl;

            gl = new GenericList();
            gl.Items.Add(ExcelPaster.GetCellValue(ws, "R25"));
            gl.Items.Add(ExcelPaster.GetCellValue(ws, "S25"));
            dcm.TaxReturnOther = gl;

            return dcm;
        }

        public static void Set(DocumentationChecklistModel dcm)
        {
            Worksheet ws = Globals.ThisAddIn.Application.Worksheets["Documentation"];
            ws.Activate();
            ExcelPaster.SetCellValue(ws,"R6", dcm.ClaimsPreparation.Items[0]);
            ExcelPaster.SetCellValue(ws,"S6",dcm.ClaimsPreparation.Items[1]);

            ExcelPaster.SetCellValue(ws,"R7",dcm.Contracts.Items[0]);
            ExcelPaster.SetCellValue(ws,"S7",dcm.Contracts.Items[1]);

            ExcelPaster.SetCellValue(ws,"R8",dcm.FinancialStatements2006.Items[0]);
            ExcelPaster.SetCellValue(ws,"S8",dcm.FinancialStatements2006.Items[1]);

            ExcelPaster.SetCellValue(ws,"R9",dcm.FinancialStatements2007.Items[0]);
            ExcelPaster.SetCellValue(ws,"S9",dcm.FinancialStatements2007.Items[1]);

            ExcelPaster.SetCellValue(ws,"R10",dcm.FinancialStatements2008.Items[0]);
            ExcelPaster.SetCellValue(ws,"S10",dcm.FinancialStatements2008.Items[1]);

            ExcelPaster.SetCellValue(ws,"R11",dcm.FinancialStatements2009.Items[0]);
            ExcelPaster.SetCellValue(ws,"S11",dcm.FinancialStatements2009.Items[1]);

            ExcelPaster.SetCellValue(ws,"R12",dcm.FinancialStatements2010.Items[0]);
            ExcelPaster.SetCellValue(ws,"S12",dcm.FinancialStatements2010.Items[1]);

            ExcelPaster.SetCellValue(ws,"R13",dcm.FinancialStatements2011.Items[0]);
            ExcelPaster.SetCellValue(ws,"S13",dcm.FinancialStatements2011.Items[1]);

            ExcelPaster.SetCellValue(ws,"R14",dcm.FinancialStatements2012.Items[0]);
            ExcelPaster.SetCellValue(ws,"S14",dcm.FinancialStatements2012.Items[1]);

            ExcelPaster.SetCellValue(ws,"R15",dcm.FinancialStatementsOther.Items[0]);
            ExcelPaster.SetCellValue(ws,"S15",dcm.FinancialStatementsOther.Items[1]);

            ExcelPaster.SetCellValue(ws,"R16",dcm.RentalProperty.Items[0]);
            ExcelPaster.SetCellValue(ws,"S16",dcm.RentalProperty.Items[1]);

            ExcelPaster.SetCellValue(ws,"R17",dcm.SupplementalFinancialDocuments.Items[0]);
            ExcelPaster.SetCellValue(ws,"S17",dcm.SupplementalFinancialDocuments.Items[1]);

            ExcelPaster.SetCellValue(ws,"R18",dcm.TaxReturn2006.Items[0]);
            ExcelPaster.SetCellValue(ws,"S18",dcm.TaxReturn2006.Items[1]);

            ExcelPaster.SetCellValue(ws,"R19",dcm.TaxReturn2007.Items[0]);
            ExcelPaster.SetCellValue(ws,"S19",dcm.TaxReturn2007.Items[1]);

            ExcelPaster.SetCellValue(ws,"R20",dcm.TaxReturn2008.Items[0]);
            ExcelPaster.SetCellValue(ws,"S20",dcm.TaxReturn2008.Items[1]);

            ExcelPaster.SetCellValue(ws,"R21",dcm.TaxReturn2009.Items[0]);
            ExcelPaster.SetCellValue(ws,"S21",dcm.TaxReturn2009.Items[1]);

            ExcelPaster.SetCellValue(ws,"R22",dcm.TaxReturn2010.Items[0]);
            ExcelPaster.SetCellValue(ws,"S22",dcm.TaxReturn2010.Items[1]);

            ExcelPaster.SetCellValue(ws,"R23",dcm.TaxReturn2011.Items[0]);
            ExcelPaster.SetCellValue(ws,"S23",dcm.TaxReturn2011.Items[1]);

            ExcelPaster.SetCellValue(ws,"R24",dcm.TaxReturn2012.Items[0]);
            ExcelPaster.SetCellValue(ws,"S24",dcm.TaxReturn2012.Items[1]);

            ExcelPaster.SetCellValue(ws,"R25",dcm.TaxReturnOther.Items[0]);
            ExcelPaster.SetCellValue(ws,"S25",dcm.TaxReturnOther.Items[1]);

        }
    }
}
