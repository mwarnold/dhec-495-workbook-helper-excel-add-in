﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using DheccExcelAddIn2013.Interfaces;
using DheccExcelAddIn2013.Helpers;
using DheccExcelAddIn2013.Models;
using Newtonsoft.Json;

namespace DheccExcelAddIn2013.UserControls
{
    public partial class ucUploadClaim : UserControl
    {
        OpenFileDialog ofd;
        private static string PDFFilePath = "";
        private static string PNLFilePath = "";
        private IDheccContract iContract;
        public ucUploadClaim()
        {
            InitializeComponent();
            iContract = new DheccContract();
        }

        public void RefreshControl()
        {
            btnUpload.Enabled = false;
            lblPDFFile.Text = "No file selected.";
            lblPNLSummary.Text = "No file selected.";
            PNLFilePath = "";
            PDFFilePath = "";
            Validate();
            lblWorkbookStatus.Text = "";
            lblSummaryStatus.Text = "";
            lblPDFStatus.Text = "";
            ShowHideGB();
        }

        private void ShowHideGB()
        {
            if (iContract.AppSetting().User.Roles.Select(x=> x.Id).ToList().Contains(AppVariables.QARoleId))
            {
                gbInitialReview.Visible = false;
                gbQAUpload.Location = new Point() { X = 14, Y = 14 };
                gbQAUpload.Visible = true;
            }
            else
            {
                gbQAUpload.Visible = false;
                gbInitialReview.Location = new Point() { X = 14, Y = 14 };
                gbInitialReview.Visible = true;
            }
        }

        private void btnAttachPDF_Click(object sender, EventArgs e)
        {
            ofd = new OpenFileDialog();
            ofd.Filter = "Pdf Files|*.pdf";
            ofd.Title = "Please select the PDF for this workbook.";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                PDFFilePath = ofd.FileName;
                lblPDFFile.Text = PDFFilePath;
                Validate();
            }
        }

        private void btnPNLSummary_Click(object sender, EventArgs e)
        {
            ofd = new OpenFileDialog();
            ofd.Filter = "Excel Worksheets|*.xlsx;*.xls";
            ofd.Title = "Please select the Excel workbook.";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                PNLFilePath = ofd.FileName;
                lblPNLSummary.Text = PNLFilePath;
                Validate();
            }
        }

        private void btnUpload_Click_1(object sender, EventArgs e)
        {
            //This is for the Q/A user only
            Globals.ThisAddIn.SetActiveWorkbook();
            if (!iContract.UsersClaims().Any(x => x == iContract.CurrentClaimId()))
            {
                MessageBox.Show("Claim and/or claimant is not valid for: " + iContract.AppSetting().User.UserName);
                return;
            }
            int _claimantId = iContract.CurrentClaimantId();
            int _claimId = iContract.CurrentClaimId();
            var upload = iContract.CurrentWorkbookWrapper();
            try
            {
                pbUpload.Value = 0;
                pbUpload.PerformStep();
                try
                {
                    String json = JsonConvert.SerializeObject(new { UploadModel = upload });
                    var client = AppVariables.BGWebService();

                    com.browngreer.www3.clsSaveWorkbookInfo wbSave = new com.browngreer.www3.clsSaveWorkbookInfo();
                    var stream = WorkbookTemplate.SaveWorkbookForUploadAndGetStream();
                    pbInitialReviewer.PerformStep();
                    pbInitialReviewer.PerformStep();
                    string filePath = AppVariables.LocalExcelFileDirectory();
                    string fullPath = filePath +
                        string.Format("{0}_{1}_{2}_save.xlsm", iContract.CurrentClaimId(),
                        iContract.CurrentClaimantId(), iContract.CurrentWorkbookWrapper().Workbook.Id);
                    wbSave.FileExtension_Excel = Path.GetExtension(fullPath);
                    wbSave.FileName_Excel = Path.GetFileName(fullPath);
                    wbSave.FileStream_Excel = stream;

                    var interimInfo = new com.browngreer.www3.clsInterimInfo();
                    var addInInfo = new com.browngreer.www3.clsAddIn();
                    addInInfo.Active = iContract.AppSetting().UserAddIn.Active;
                    addInInfo.AddInID = iContract.AppSetting().UserAddIn.Id;
                    addInInfo.Description = iContract.AppSetting().UserAddIn.Description;
                    addInInfo.Version = iContract.AppSetting().UserAddIn.Version;
                    interimInfo.AddIn = addInInfo;
                    interimInfo.AuthToken = AppVariables.AuthToken;
                    interimInfo.ClaimantID = _claimantId;
                    interimInfo.ClaimID = _claimId;
                    upload.LastSaveDate = DateTime.Now;
                    interimInfo.ClaimData = JsonConvert.SerializeObject(upload);
                    interimInfo.ReviewQueueTypeID = Convert.ToInt16(upload.ReviewQueueTypeId);
                    interimInfo.CommitMessage = iContract.AppSetting().User.UserName + " uploaded claim for approval";
                    interimInfo.CompMethodID = Convert.ToInt16(upload.CompensationMethodId);
                    interimInfo.Date = DateTime.Now;
                    interimInfo.IsFinal = true;
                    interimInfo.UserID = AppVariables.CurrentUserId;
                    interimInfo.WBInfoID = upload.Workbook.Id;
                    wbSave.InterimInfo = interimInfo;
                    var result = client.SaveWorkbookInfo(wbSave);

                    pbUpload.PerformStep();
                   
                    if (result != null)
                    {
                        if (result.Result.ResultMessage == "Success")
                        {
                            try
                            {
                                WorkbookTemplate.DeleteTempSaveFile();
                            }
                            catch { }
                            pbUpload.PerformStep();
                            WorkbookTemplate.UpdateActiveWorkbookList(result.WorkbookInfoList);
                            lblWorkbookStatus.Text = "Full workbook upload process complete.";
                            //Now upload workbook summary
                            com.browngreer.www3.clsResult summaryResult = client.SaveFinancialDataWorkbook(AppVariables.CurrentUserId, _claimantId,
                                Path.GetFileName(PNLFilePath), Path.GetExtension(PNLFilePath), File.ReadAllBytes(PNLFilePath));
                            if (summaryResult.ResultMessage == "Success")
                            {
                                pbUpload.PerformStep();
                                pbUpload.PerformStep();
                                WorkbookTemplate.UpdateActiveWorkbookList(result.WorkbookInfoList);
                                lblSummaryStatus.Text = "Client P&L Summary upload complete.";
                                //Now upload pdf 
                                com.browngreer.www3.clsResult pdfResult = client.SaveWorkbookPDF(AppVariables.CurrentUserId, _claimantId,
                                    Path.GetFileName(PDFFilePath), File.ReadAllBytes(PDFFilePath));
                                if (pdfResult.ResultMessage == "Success")
                                {
                                    pbUpload.PerformStep();
                                    pbUpload.PerformStep();
                                    WorkbookTemplate.UpdateActiveWorkbookList(result.WorkbookInfoList);
                                    lblPDFStatus.Text = "PDF upload complete.";
                                   
                                    iContract.CurrentWorkbookWrapper().ReviewerHistory.Add
                                        (new ReviewerHistoryModel()
                                        {
                                            Date = DateTime.Now,
                                            Description = "Final Save Complete",
                                            ReviewedBy = iContract.AppSetting().User.UserName,
                                        });
                                    upload.SaveHistId = result.SaveHistID;
                                    upload.LastSaveDate = DateTime.Now;
                                    new WIPHelper().SaveWIP(iContract.CurrentWorkbookWrapper());
                                    MessageBox.Show("Successfully uploaded to portal. Please go to the portal and complete step 6.");
                                }
                                else
                                {
                                    MessageBox.Show("PDF Message: " + pdfResult.ResultMessage);
                                }
                            }
                            else
                            {
                                MessageBox.Show("P&L Summary Message: " + summaryResult.ResultMessage);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Workbook Message: " + result.Result.ResultMessage);
                        }
                    }
                    else
                    {
                        EmailHelper.SendExceptionEmail("Final upload failed.");
                        MessageBox.Show("Save Failed. A message has been sent to our development team.");
                    }
                }
                catch (Exception ex)
                {
                    pbUpload.Value = 0;
                    MessageBox.Show("Error: " + ex.ToString());
                    EmailHelper.SendExceptionEmail(ex.ToString());
                    return;
                }
            }
            catch (Exception ex)
            {
                WebServiceResponseHelper.ValidateResponse(0, "An unhandled exception has occured. Our dev team has been notifiied. We apologize for the inconvenience");
                EmailHelper.SendExceptionEmail(ex.ToString());
            }
        }

        private bool Validate()
        {
            bool isValid = false;
            if (string.IsNullOrEmpty(PDFFilePath))
                imgPDF.Image = DheccExcelAddIn2013.Properties.Resources.FailIcon;
            else
            {
                isValid = true;
                imgPDF.Image = DheccExcelAddIn2013.Properties.Resources.SuccessIcon;
            }

            if (string.IsNullOrEmpty(PNLFilePath))
                imgPNL.Image = DheccExcelAddIn2013.Properties.Resources.FailIcon;
            else
            {
                isValid = !isValid ? isValid : true;
                imgPNL.Image = DheccExcelAddIn2013.Properties.Resources.SuccessIcon;
            }

            btnUpload.Enabled = isValid;
            return isValid;
        }

        private void btnInitialReviewerUpload_Click(object sender, EventArgs e)
        {
            //This is for the initial reviewer user only
            if (!iContract.UsersClaims().Any(x => x == iContract.CurrentClaimId()))
            {
                MessageBox.Show("Claim and/or claimant is not valid for: " + iContract.AppSetting().User.UserName);
                return;
            }
            int _claimantId = iContract.CurrentClaimantId();
            int _claimId = iContract.CurrentClaimId();
            var upload = iContract.CurrentWorkbookWrapper();
            try
            {
                pbInitialReviewer.Value = 0;
                pbInitialReviewer.PerformStep();
                try
                {
                    String json = JsonConvert.SerializeObject(new { UploadModel = upload });
                    var client = AppVariables.BGWebService();
                    var stream = WorkbookTemplate.SaveWorkbookForUploadAndGetStream();
                    pbInitialReviewer.PerformStep();
                    pbInitialReviewer.PerformStep();
                    com.browngreer.www3.clsSaveWorkbookInfo wbSave = new com.browngreer.www3.clsSaveWorkbookInfo();
                    string filePath = AppVariables.LocalExcelFileDirectory();
                    string fullPath = filePath +
                        string.Format("{0}_{1}_{2}_save.xlsm", iContract.CurrentClaimId(),
                        iContract.CurrentClaimantId(), iContract.CurrentWorkbookWrapper().Workbook.Id);
                    wbSave.FileExtension_Excel = Path.GetExtension(fullPath);
                    wbSave.FileName_Excel = Path.GetFileName(fullPath);
                    wbSave.FileStream_Excel = stream;
                    pbInitialReviewer.PerformStep();
                    var interimInfo = new com.browngreer.www3.clsInterimInfo();
                    var addInInfo = new com.browngreer.www3.clsAddIn();
                    addInInfo.Active = iContract.AppSetting().UserAddIn.Active;
                    addInInfo.AddInID = iContract.AppSetting().UserAddIn.Id;
                    addInInfo.Description = iContract.AppSetting().UserAddIn.Description;
                    addInInfo.Version = iContract.AppSetting().UserAddIn.Version;
                    interimInfo.AddIn = addInInfo;
                    interimInfo.AuthToken = AppVariables.AuthToken;
                    interimInfo.ClaimantID = _claimantId;
                    interimInfo.ClaimID = _claimId;
                    pbInitialReviewer.PerformStep();
                    upload.LastSaveDate = DateTime.Now;
                    interimInfo.ClaimData = JsonConvert.SerializeObject(upload);
                    interimInfo.ReviewQueueTypeID = Convert.ToInt16(upload.ReviewQueueTypeId);
                    interimInfo.CommitMessage = iContract.AppSetting().User.UserName + " uploaded claim for approval";
                    interimInfo.CompMethodID = Convert.ToInt16(upload.CompensationMethodId);
                    interimInfo.Date = DateTime.Now;
                    interimInfo.IsFinal = true;
                    interimInfo.UserID = AppVariables.CurrentUserId;
                    interimInfo.WBInfoID = upload.Workbook.Id;
                    wbSave.InterimInfo = interimInfo;
                    var result = client.SaveWorkbookInfo(wbSave);

                    pbInitialReviewer.PerformStep();
                    pbInitialReviewer.PerformStep();
                    pbInitialReviewer.PerformStep();
                    pbInitialReviewer.PerformStep();
                    pbInitialReviewer.PerformStep();
                    pbInitialReviewer.PerformStep();
                    if (result != null)
                    {
                        if (result.Result.ResultMessage == "Success")
                        {
                            iContract.CurrentWorkbookWrapper().ReviewerHistory.Add
                                        (new ReviewerHistoryModel()
                                        {
                                            Date = DateTime.Now,
                                            Description = "Final Save Complete",
                                            ReviewedBy = iContract.AppSetting().User.UserName,
                                        });
                            upload.SaveHistId = result.SaveHistID;
                            upload.LastSaveDate = DateTime.Now;
                            new WIPHelper().SaveWIP(iContract.CurrentWorkbookWrapper());
                            MessageBox.Show("Successfully uploaded to portal. Please go to the portal and complete step 6.");
                            Cursor.Current = Cursors.Arrow;
                            try
                            {
                                WorkbookTemplate.DeleteTempSaveFile();
                            }
                            catch { }
                        }
                        else
                        {
                            MessageBox.Show("Workbook Message: " + result.Result.ResultMessage);
                        }
                    }
                    else
                    {
                        EmailHelper.SendExceptionEmail("Final upload failed.");
                        MessageBox.Show("Save Failed. A message has been sent to our development team.");
                    }
                }
                catch (Exception ex)
                {
                    pbInitialReviewer.Value = 0;
                    Cursor.Current = Cursors.Arrow;
                    MessageBox.Show("Error: " + ex.ToString());
                    EmailHelper.SendExceptionEmail(ex.ToString());
                    return;
                }
            }
            catch (Exception ex)
            {
                WebServiceResponseHelper.ValidateResponse(0, "An unhandled exception has occured. Our dev team has been notifiied. We apologize for the inconvenience");
                EmailHelper.SendExceptionEmail(ex.ToString());
            }
        }
    }
}
